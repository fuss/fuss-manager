fuss-manager
============

Dependencies
------------

* ``python3 >= 3.4``
* ``python3-setuptools``
* ``python3-tornado >= 4.4``
* ``python3-ruamel.yaml >= 0.15``
* ``python3-aiodns``
* ``python3-dateutil``
* ``inotify-tools >= 3.14``
* ``arpwatch >= 2.1a15``
* ``libjs-bootstrap4``
* ``libjs-popper.js``
* ``libjs-jquery``
* ``libjs-jquery-datatables``
* ``libjs-mustache``
* ``libjs-moment``
* ``fonts-fork-awesome``

To run tests and qa:

* ``python3-nose2``
* ``flake8``

To also run the tests that require a known network:

* ``libvirt-clients``
* ``libvirt-daemon``
* ``virtinst``
* ``libvirt-daemon-system``
* ``qemu``
* ``qemu-kvm``

and the current user should be able to run libvirt/qemu virtual machines.

Running tests
-------------

The scripts ``./run_tests`` and ``./run_coverage`` will run all available
tests (printing coverage results in the second case.

Some of the tests require an environment with a few known (virtual)
machines; if they are not available those tests are auto-skipped.
To configure those machines, there is a script ``fuss-test-network`` which
can create (and destroy) them using libvirt and qemu/kvm.

To setup such a system run::

    ./fuss-test-network --start

to download a base image (if needed), create, configure and run two virtual
machines (with their own network; you can then run the tests as above, and
you will no longer get skipped ones.

Afterwards, by running::

    ./fuss-test-network --stop

the machines will be stopped and destroyed, together with the network,
keeping only the base image that has been downloaded and preconfigured
originally.

See ``./fuss-test-network --help`` for further options to customize e.g.
what storage or what network configuration to use.

License
-------

Copyright (C) 2019 Provincia Autonoma di Bolzano

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
