***************
 Configuration
***************

``fuss-manager runserver`` will read its configuration from a yaml file 
in ``/etc/fuss-manager/fuss-manager.yaml`` (or any location specified
with the option ``--config``).

Most configuration have sensible defaults to fall back to in case they
are not defined in the configuration file.

Configuration options
=====================

LDAP
----

LDAP configuration variables don't have a default: if they are not
present in the configuration login via LDAP is disabled.

``ldap_uri``
   URI of the ldap server, e.g. ``"ldap://localhost:port"``
``ldap_user_search_base``
   e.g. ``ou=Users,dc=fuss,dc=lan``
``ldap_group_search_base``
   e.g. ``ou=Groups,dc=fuss,dc=lan``

Ansible and inventory files
---------------------------

``path_ansible_inventory``
   Path to the ansible inventory managed by fuss-manager.

   default: ``"/etc/fuss-manager/hosts"``
``path_stats_cache``
   Path to the stats file.

   default: ``"/etc/fuss-manager/machine_stats"``
``path_playbooks``
   Path to the ansible playbooks available to fuss-manager.

   default: ``"/etc/fuss-manager/playbooks"``

Web service configuration
-------------------------

``web_address``
   Address of the fuss-manager.

   default: ``"localhost"``
``web_port``
   Port to listen to.

   default: ``1232``

Miscellaneous
-------------

``arpwatch_datafile``
   default: ``'/var/lib/arpwatch/arp.dat'``
``dhcp_log_file``
   default: ``'/var/log/syslog'``
``debug``
   default: ``False``
``operation_log_file``
   default: ``'/var/log/fuss-manager/operations.log'``
``playbook_log_dir``
   default: ``'/var/log/fuss-manager/playbooks.log'``
``disabled_sources``
   default: ``()``


