.. fuss-manager documentation master file, created by
   sphinx-quickstart on Tue Aug 18 13:35:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fuss-manager's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   configuration
   web_interface
   rest_api

   reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
