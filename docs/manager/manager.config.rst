manager.config module
=====================

.. automodule:: manager.config
   :members:
   :undoc-members:
   :show-inheritance:
