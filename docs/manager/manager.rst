manager package
===============

.. automodule:: manager
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::

   manager.sources
   manager.users
   manager.web

Submodules
----------

.. toctree::

   manager.compat
   manager.config
   manager.events
   manager.manager
   manager.ops
   manager.perms
   manager.playbook
   manager.signing
   manager.stores
