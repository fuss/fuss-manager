manager.signing module
======================

.. automodule:: manager.signing
   :members:
   :undoc-members:
   :show-inheritance:
