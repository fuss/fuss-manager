manager.users package
=====================

.. automodule:: manager.users
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::

   manager.users.db
   manager.users.ldap
   manager.users.local
   manager.users.master
   manager.users.mock
