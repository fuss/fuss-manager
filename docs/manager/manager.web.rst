manager.web package
===================

.. automodule:: manager.web
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::

   manager.web.server
   manager.web.session
   manager.web.static
   manager.web.views
   manager.web.webapi
