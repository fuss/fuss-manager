manager.web.session module
==========================

.. automodule:: manager.web.session
   :members:
   :undoc-members:
   :show-inheritance:
