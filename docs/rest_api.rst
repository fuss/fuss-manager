***********************
 fuss-manager REST API
***********************

General description
===================

The main API entry point is at ``/api/1.0``. All methods return ``JSON``
results, and the result is always an Object whether the request is
successful or not.

All successful requests return objects with the ``time`` key set to the
server time as a Unix timestamp.

All failed requests return objects with three keys:

-  ``error``: ``true``
-  ``code``: the HTTP error code
-  ``message``: a string describing the error

``GET`` requests can take extra arguments in a query string.

``POST`` requests take their input from the ``POST`` body encoded as
``JSON``.

API methods
===========

``ping``
--------

Takes no arguments.

Returns:

-  ``pong``: ``true``

``async_ping``
--------------

Takes no arguments.

Returns:

-  ``pong``: ``true``

``machines``
------------

Takes no arguments.

Returns:

-  ``machines``: list of machine objects.

Each machine object contains the following keys:

-  ``mac``: MAC address, as a string
-  ``ip``: IP address, as a string
-  ``name``: machine name
-  ``first_seen``: time the machine was first seen, as a Unix timestamp
-  ``last_seen``: time the machine was last seen, as a Unix timestamp

You can use the response ``time`` timestamp to compute how long ago a
machine was first or last seen without introducing errors due to the
clock difference between the browser and the server.
