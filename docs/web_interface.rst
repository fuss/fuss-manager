Fuss Manager web interface specs
================================

Browser compatibility
---------------------

The minimum browser versions supported are the versions of Chrome and
Firefox as packaged in Debian 9:

-  ``chromium`` ``>=`` ``73.0.3683.75-1~deb9u1`` (i386, amd64)
-  ``firefox-esr`` ``>=`` ``68.10.0esr-1~deb9u1`` (i386, amd64)

Javascript frameworks used
--------------------------

For JavaScript libraries, Fuss Manager uses only what is packaged from
Debian Buster onwards.

A custom file handler (see ``manager/web/static.py``) gives priority to
packaged versions, falling back on the fuss-manager static file
directory when assets are not found there.

The idea is to target Debian Bullseye and Debian Buster+backports for
official packaging, and make the package still installable, even if not
fully policy compliant, for earlier versions of Debian.
