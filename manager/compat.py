"""
Miscellaneous wrappers for compatibility with old versions of
depenencies.
"""

import asyncio
import logging
import datetime

import ruamel.yaml

# Tornado 4.5 moved URLSpec from tornado.web to tornado.routing
# This is to be used elsewhere as ``from compat import URLSpec``.

try:
    from tornado.routing import URLSpec  # noqa: F401
except ImportError:
    from tornado.web import URLSpec  # noqa: F401


# creating Futures has changed in python 3.5 with the availability of
# the method loop.create_future()


def create_future(loop):
    f = getattr(loop, 'create_future', None)
    if f is not None:
        return f()
    else:
        return asyncio.Future()


# Starting from python 3.7 the preferred way to create new Tasks is to
# use asyncio.create_task()


def create_task(coro):
    try:
        return asyncio.create_task(coro)
    except AttributeError:
        # this is awkward, but required to prevent a SyntaxError in
        # python versions where async is a keyword
        return getattr(asyncio, 'async')(coro)


try:
    from dateutil.parser import isoparse
except ImportError:
    from pytz import utc

    def isoparse(s):
        res = datetime.datetime.strptime(s)
        return res.replace(tzinfo=utc)
