import logging

import ruamel.yaml

log = logging.getLogger(__name__)

DEFAULTS = {
    "path_ansible_inventory": "/etc/fuss-manager/hosts",
    "path_stats_cache": "/etc/fuss-manager/machine_stats",
    "path_playbooks": "/etc/fuss-manager/playbooks",
    "web_address": "localhost",
    "web_port": 1232,
    "arpwatch_datadir": "/var/lib/arpwatch/",
    "dhcp_log_file": "/var/log/syslog",
    "dhcp_leases_file": "/var/lib/dhcp/dhcpd.leases",
    "debug": False,
    "operation_log_file": "/var/log/fuss-manager/operations.log",
    "event_log_file": None,
    "playbook_log_dir": "/var/log/fuss-manager/playbooks.log",
    "disabled_sources": (),
    "permission_file": "/etc/fuss-manager/perms.yaml",
    "master_pass_file": "/etc/fuss-server/fuss-server.yaml",
    "ldap_uri": False,
    "ldap_user_search_base": False,
    "ldap_group_search_base": False,
    "web_session_dir": None,
    "auth_forward_secret": None,
}


MOCK_VALUES = {
    "path_ansible_inventory": "./hosts",
    "path_stats_cache": "./machine_stats",
    "path_playbooks": "./tests/data/playbooks",
    "dhcp_log_file": "/var/log/syslog",
    "debug": True,
    "operation_log_file": "./operations.log",
    "playbook_log_dir": "./playbooks.log",
    "permission_file": "tests/data/perms.yaml",
    "master_pass_file": False,
    "auth_forward_secret": "mock-auth-forward-secret",
}


class Config:
    def __init__(self, config_fname=None):
        if config_fname:
            conf = self._load_file(config_fname)
        else:
            conf = {}
        for k, v in DEFAULTS.items():
            setattr(self, k, conf.get(k, v))
        self._load_perms()

    def _load_file(self, fname):
        yaml = ruamel.yaml.YAML()
        with open(fname) as fp:
            return yaml.load(fp) or {}

    def _load_perms(self):
        yaml = ruamel.yaml.YAML()
        try:
            with open(self.permission_file) as fp:
                p = yaml.load(fp) or {}
        except IOError as e:
            log.warning("Could not load permission file: %s", str(e))
            p = {}

        self.permissions = {
            "users": p.get("users", {}),
            "groups": p.get("groups", {}),
        }


class MockConfig(Config):
    def __init__(self):
        super().__init__()
        for k, v in MOCK_VALUES.items():
            setattr(self, k, v)
        # reload permissions now that the file has probably changed
        self._load_perms()
