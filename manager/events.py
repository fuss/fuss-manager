from typing import Any, Type, Sequence, Callable, Optional
import asyncio
import time
from collections import defaultdict


class Event:
    """
    Root class hierarchy for events
    """

    __slots__ = ("timestamp", "source")

    def __init__(self, source: Any, timestamp=None):
        if timestamp is None:
            timestamp = time.time()
        self.timestamp = timestamp
        self.source = source

    @property
    def description(self):
        return "generic event"

    def to_jsonable(self):
        """
        Return this event as a dict of values that can be serialized to JSON
        """
        return {
            "type": self.__class__.__name__,
            "source": self.source.__class__.__name__,
            "ts": self.timestamp,
        }


class HostEvent(Event):
    __slots__ = ()


class HostSeenEvent(HostEvent):
    __slots__ = ("name", "ip", "mac", "registered")

    def __init__(
        self,
        source: Any,
        timestamp=None,
        mac=None,
        ip=None,
        name=None,
        registered=None,
    ):
        super().__init__(source, timestamp=timestamp)
        self.name = name
        self.ip = ip
        self.mac = mac
        self.registered = registered

    @property
    def description(self):
        data = []
        for field in "name", "ip", "mac", "registered":
            value = getattr(self, field, None)
            if value is None:
                continue
            data.append("{}: {}".format(field, value))
        return "host seen: {}".format(", ".join(data))

    def to_jsonable(self):
        res = super().to_jsonable()
        res["name"] = self.name
        res["ip"] = self.ip
        res["mac"] = self.mac
        res["registered"] = self.registered
        return res

    def __str__(self):
        return "HostSeenEvent(source={}, name={}, ip={}, mac={}, registered={})".format(  # noqa: E501
            self.source.__class__.__name__,
            repr(self.name),
            repr(self.ip),
            repr(self.mac),
            repr(self.registered),
        )

    def __repr__(self):
        return self.__str__()


class HostNewEvent(HostEvent):
    __slots__ = ("name", "ip", "mac", "registered")

    def __init__(
        self,
        source: Any,
        timestamp=None,
        mac=None,
        ip=None,
        name=None,
        registered=None,
    ):
        super().__init__(source, timestamp)
        self.mac = mac
        self.ip = ip
        self.name = name
        self.registered = registered

    @property
    def description(self):
        data = []
        for field in "name", "ip", "mac", "registered":
            value = getattr(self, field, None)
            if value is None:
                continue
            data.append("{}: {}".format(field, repr(value)))
        return "new host: {}".format(", ".join(data))

    def to_jsonable(self):
        res = super().to_jsonable()
        res["name"] = self.name
        res["ip"] = self.ip
        res["mac"] = self.mac
        res["registered"] = self.registered
        return res


class HostChangedEvent(HostEvent):
    __slots__ = ("mac", "changes")

    def __init__(self, source: Any, timestamp=None, mac=None, changes=None):
        super().__init__(source, timestamp)
        self.mac = mac
        self.changes = changes

    @property
    def description(self):
        data = []
        for name, (old, new) in sorted(self.changes.items()):
            data.append("{}: {} -> {}".format(name, old, new))
        return "host {} changed: {}".format(self.mac, ", ".join(data))

    def to_jsonable(self):
        res = super().to_jsonable()
        res["mac"] = self.mac
        res["changes"] = self.changes
        return res


class HostFactsEvent(HostEvent):
    __slots__ = ("name", "facts")

    def __init__(self, source: Any, timestamp=None, name=None, facts=None):
        super().__init__(source, timestamp)
        self.name = name
        self.facts = facts

    def to_jsonable(self):
        res = super().to_jsonable()
        res["name"] = self.name
        res["facts"] = self.facts
        return res


class HostFactsFailed(HostEvent):
    __slots__ = ("name", "result", "details")

    def __init__(
        self, source: Any, timestamp=None, name=None, result=None, details=None
    ):
        super().__init__(source, timestamp)
        self.name = name
        self.result = result
        self.details = details

    def to_jsonable(self):
        res = super().to_jsonable()
        res["name"] = self.name
        res["result"] = self.result
        res["details"] = self.details
        return res


class HostPlaybookEvent(HostEvent):
    __slots__ = ("name", "playbook", "playbook_id")

    def __init__(
        self,
        source: Any,
        timestamp=None,
        name=None,
        playbook=None,
        playbook_id=None,
    ):
        super().__init__(source, timestamp)
        self.name = name
        self.playbook = playbook
        self.playbook_id = playbook_id

    def to_jsonable(self):
        res = super().to_jsonable()
        res["name"] = self.name
        res["playbook"] = self.playbook
        res["playbook_id"] = self.playbook_id
        return res


class DataSourceEvent(Event):
    __slots__ = ()
    pass


class DataSourceStartEvent(Event):
    __slots__ = ()

    @property
    def description(self):
        return "data source started"


class DataSourceStopEvent(Event):
    __slots__ = ()

    @property
    def description(self):
        return "data source stopped"


class AnsibleEvent(Event):
    __slots__ = ()


class AnsiblePlaybookEvent(AnsibleEvent):
    __slots__ = ("playbook", "playbook_id", "results")

    def __init__(
        self,
        source: Any,
        timestamp=None,
        playbook=None,
        playbook_id=None,
        results=None,
    ):
        super().__init__(source, timestamp)
        self.playbook = playbook
        self.playbook_id = playbook_id
        self.results = results

    def to_jsonable(self):
        res = super().to_jsonable()
        res["playbook"] = self.playbook
        res["playbook_id"] = self.playbook_id
        res["results"] = self.results
        return res


class Hub:
    """
    Collect events and dispatch them to subscribers
    """

    def __init__(self, event_log=None):
        self.subscriptions = defaultdict(set)
        self.event_log = event_log

    async def publish(self, event: Event):
        """
        Publish an event, calling the registered coroutines that handle it
        """

        if self.event_log:
            self.event_log.log(event)

        # Find the handlers interested in this event
        handlers = set()
        for cls, subscribers in self.subscriptions.items():
            if isinstance(event, cls):
                handlers.update(subscribers)

        # Create the coroutine handlers
        coros = []
        for handler in handlers:
            coros.append(handler(event))

        # Run the coroutine handlers
        await asyncio.gather(*coros)

    def subscribe(
        self,
        coro: Callable[[Event], None],
        classes: Optional[Sequence[Type]] = None,
    ):
        """
        Request to call the given function whenever an event is received that
        is an instance of one of the given classes.

        If classes is missing, [Event] is assumed.

        The function is assumed to be an awaitable/coroutine
        """
        if classes is None:
            classes = [Event]

        for c in classes:
            self.subscriptions[c].add(coro)

    def unsubscribe(
        self,
        coro: Callable[[Event], None],
        classes: Optional[Sequence[Type]] = None,
    ):
        """
        Stop calling the given coroutine on events.

        If classes is None, the coroutine will be unsubscribed from all
        event types.

        coro must match what was passed to subscribe()
        """
        if classes is None:
            for handlers in self.subscriptions.values():
                handlers.discard(coro)
        else:
            for c in classes:
                self.subscriptions[c].discard(coro)
