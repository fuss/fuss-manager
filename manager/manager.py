import asyncio
import json
import logging
import os
import time

from . import sources, stores, events, playbook


class JsonableLog:
    """
    Logger for jsonable objects

    It can be used for operations executed by the manager, as well as
    for events, or anything else with a method ``.to_json()``.
    """

    def __init__(self, pathname):
        self.pathname = pathname
        os.makedirs(os.path.dirname(self.pathname), exist_ok=True)
        try:
            self.fd = open(self.pathname, "at")
        except FileNotFoundError:
            self.fd = None

    def close(self):
        if self.fd is None:
            return
        self.fd.close()

    def log(self, op):
        if self.fd is None:
            return
        op_data = op.to_jsonable()
        op_data["ts"] = time.time()
        json.dump(op_data, self.fd)
        self.fd.write("\n")


class Manager:
    def __init__(self, config):
        self.log = logging.getLogger(self.__class__.__name__)
        self.config = config
        if self.config.event_log_file:
            event_log = JsonableLog(self.config.event_log_file)
        else:
            event_log = None
        self.event_hub = events.Hub(event_log)
        self.sources = []
        self.load_data_sources()
        self.store = stores.MachineStore(self.event_hub, config=self.config)
        self.operation_log = JsonableLog(self.config.operation_log_file)
        self.ansible = playbook.Ansible(self.event_hub, self.config)
        self.users = None

    def load_data_sources(self):
        """
        Try to load all known data sources.

        Check for viability using .is_viable() to prevend loading data
        sources that are missing dependencies.
        """
        # sources that require no params
        for source in [
            sources.DhcpdDataSource,
            sources.ChaosDataSource,
        ]:
            if not source.is_viable(self.config):
                self.log.warn(
                    "%s: data source will not be started", source.__name__
                )
                continue
            self.log.info("%s: data source started", source.__name__)
            self.sources.append(
                source(event_hub=self.event_hub, config=self.config)
            )
        # arpwatch data source requires a filename
        # we only try to load the arpwatch datasource if the directory
        # is readable
        if os.path.isdir(self.config.arpwatch_datadir) and os.access(
            self.config.arpwatch_datadir, os.R_OK
        ):
            for fname in os.listdir(self.config.arpwatch_datadir):
                if fname.endswith('.dat'):
                    source = sources.ArpwatchDataSource
                    dat_fname = os.path.join(
                        self.config.arpwatch_datadir, fname
                    )
                    if not source.is_viable(self.config, dat_fname):
                        self.log.warn(
                            "ArpwatchDataSource: data source will not be started on %s",  # noqa: E501
                            dat_fname,
                        )
                        continue
                    self.log.info(
                        "ArpwatchDataSource: data source started on %s",
                        dat_fname,
                    )
                    self.sources.append(
                        source(
                            event_hub=self.event_hub,
                            config=self.config,
                            datafile=dat_fname,
                        )
                    )

    async def load_user_database(self):
        """
        Try to load a user database
        """
        from .users import ldap
        from .users import local
        from .users import master
        from .users import mock

        # sources that require no params
        for db in [
            ldap.LDAP,
            local.Local,
            master.Master,
            mock.Mock,
        ]:
            if not await db.is_viable(self.config):
                self.log.warn(
                    "%s: user database will not be used", db.__name__
                )
                continue
            return db(self.config)
        raise RuntimeError("Cannot instantiate a valid user database")

    async def init(self):
        self.users = await self.load_user_database()
        await self.store.load()

    async def run(self):
        coros = []
        for ds in self.sources:
            coros.append(ds.run())
        await asyncio.gather(*coros)

    async def shutdown(self):
        self.log.info("shutdown requested")
        coros = []
        for ds in self.sources:
            coros.append(ds.shutdown())
        await asyncio.gather(*coros)
        self.operation_log.close()

    async def execute(self, op):
        """
        Execute an operation
        """
        self.operation_log.log(op)
        await op.execute(self)
