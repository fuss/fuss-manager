from functools import wraps
from typing import Dict, Any, Optional, List
from .manager import Manager
from . import perms


registry = {}


def permission_required(perm):
    """
    Decorator to be used on the execute method of an operation to check
    that the current user has permissions to run that operation.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(op, manager):
            if perms.has_permissions(
                op.user, perm, manager.config.permissions
            ):
                return func(op, manager)
            else:
                raise OpPermissionDenied()

        return wrapper

    return decorator


class OpPermissionDenied(Exception):
    """
    Raised when the user does not have permissions to run an operation.
    """


class OperationMetaClass(type):
    def __new__(cls, name, bases, dct):
        res = super().__new__(cls, name, bases, dct)
        registry[name] = res
        return res


class Operation(metaclass=OperationMetaClass):
    def __init__(self, source: Optional[str] = None, user=None, **kw):
        self.source = source
        self.user = user

    @classmethod
    def from_json(cls, decoded, user):
        name = decoded.pop("name")
        decoded['user'] = user
        Op = registry[name]
        return Op(**decoded)

    def get_name(self) -> str:
        """
        Get the string describing the operation
        """
        return self.__class__.__name__

    def to_jsonable(self) -> Dict[str, Any]:
        return {
            "name": self.get_name(),
            "source": self.source,
        }

    async def execute(self, manager: Manager):
        raise NotImplementedError()


class Noop(Operation):
    """
    Does nothing
    """

    async def execute(self, manager: Manager):
        pass


class AddGroup(Operation):
    """
    Associate a machine to a group
    """

    def __init__(self, mac: str, group: str, **kw):
        super().__init__(**kw)
        self.mac = mac
        self.group = group

    def to_jsonable(self) -> Dict[str, Any]:
        res = super().to_jsonable()
        res["mac"] = self.mac
        res["group"] = self.group
        return res

    @permission_required(perms.MachineGroupEdit)
    async def execute(self, manager: Manager):
        await manager.store.add_machine_to_group(self.mac, self.group)


class DelGroup(Operation):
    """
    Deassociate a machine from a group
    """

    def __init__(self, mac: str, group: str, **kw):
        super().__init__(**kw)
        self.mac = mac
        self.group = group

    def to_jsonable(self) -> Dict[str, Any]:
        res = super().to_jsonable()
        res["mac"] = self.mac
        res["group"] = self.group
        return res

    @permission_required(perms.MachineGroupEdit)
    async def execute(self, manager: Manager):
        await manager.store.remove_machine_from_group(self.mac, self.group)


class RunPlaybook(Operation):
    """
    Run a playbook
    """

    def __init__(self, hosts: str, playbook: str, **kw):
        super().__init__(**kw)
        self.hosts = hosts
        self.playbook = playbook

    def to_jsonable(self) -> Dict[str, Any]:
        res = super().to_jsonable()
        res["hosts"] = self.hosts
        res["playbook"] = self.playbook
        return res

    @permission_required(perms.RunPlaybooks)
    async def execute(self, manager: Manager):
        await manager.ansible.run_playbook(self.playbook, self.hosts)


class RefreshFacts(Operation):
    """
    Refresh facts for a machine or group of machines
    """

    def __init__(self, hosts: str, **kw):
        super().__init__(**kw)
        self.hosts = hosts

    def to_jsonable(self) -> Dict[str, Any]:
        res = super().to_jsonable()
        res["hosts"] = self.hosts
        return res

    @permission_required(perms.RunPlaybooks)
    async def execute(self, manager: Manager):
        await manager.ansible.gather_facts(self.hosts)


class WakeOnLan(Operation):
    """
    Run a playbook
    """

    def __init__(self, macs: List[str], **kw):
        super().__init__(**kw)
        self.macs = macs

    def to_jsonable(self) -> Dict[str, Any]:
        res = super().to_jsonable()
        res["macs"] = self.macs
        return res

    @permission_required(perms.MachinePowerOn)
    async def execute(self, manager: Manager):
        await manager.ansible.wake_on_lan(self.macs)
