from manager.users.db import User


# When adding new permissions remember to add them to the example in
# doc/perms.yaml.example


class Permission:
    """
    Root class for permissions.

    Permissions do not need to be instantiated: the classes can be used
    directly.
    """
    @classmethod
    def allows(cls, perm):
        """
        Check if this permission is or contains the given permission
        """
        return issubclass(cls, perm)


class ReadonlyBaseAccess(Permission):
    """
    User has read-only access to machine information, user information, and
    events
    """
    pass


class MachineGroupEdit(Permission):
    """
    User can add/remove entries from machine groups
    """
    pass


class ShowPlaybookLogs(Permission):
    """
    User can read playbook logs
    """
    pass


class RunPlaybooks(Permission):
    """
    User can run any playbook
    """
    pass


class InventoryVarsEdit(Permission):
    """
    User can modify host and group variables in the machine inventory
    """
    pass


class MachinePowerOn(Permission):
    """
    User can send power-on wake-on-lan packets
    """
    pass


# TODO: run di playbook (per tag).


def has_permissions(user: User, perm: Permission, perm_lists: dict):
    user_perms = perm_lists['users'].get(getattr(user, 'name', None), [])
    if perm.__name__ in user_perms:
        return True
    for g in getattr(user, 'groups', []):
        # user may be None, and in that case it wouldn't have any
        # group, so we use a getaddr.
        group_perms = perm_lists['groups'].get(g.name, [])
        if perm.__name__ in group_perms:
            return True
    return False
