import asyncio
import logging
import json
import os
import re
import datetime
import hashlib
import time

import ruamel.yaml

from . import events
from .compat import isoparse


# Base ansible subprocess kwargs
env = dict(os.environ)
env['PYTHONUNBUFFERED'] = '1'
env['ANSIBLE_STDOUT_CALLBACK'] = 'json'
ANSIBLE_BASE_KW = {
    'stdout': asyncio.subprocess.PIPE,
    'stderr': asyncio.subprocess.PIPE,
    'env': env,
}

re_head = re.compile(r"^(?P<host>\S+) \| (?P<result>\S+) => {")


class PlaybookLog:
    """
    Logger for results of Playbook runs
    """

    def __init__(self, root):
        self.root = root

    def log(self, playbook, results):
        """
        Log playbook results
        """
        first_play = results["plays"][0]["play"]
        try:
            start = isoparse(first_play["duration"]["start"])
            end = isoparse(first_play["duration"]["end"])
        except KeyError:
            # Failed logs don't have a duration field, so we fallback on
            # the current timestamp.
            start = datetime.datetime.now()
            end = start
        playbook_id = first_play["id"]

        has_ok = False
        has_changed = False
        has_failures = False
        has_unreachable = False
        for s in results["stats"].values():
            if s["ok"] > 0:
                has_ok = True
            if s["changed"] > 0:
                has_changed = True
            if s["failures"] > 0:
                has_failures = True
            if s["unreachable"] > 0:
                has_unreachable = True

        pb_relpath = start.strftime("%Y-%m")
        os.makedirs(os.path.join(self.root, pb_relpath), exist_ok=True)

        id_relpath = os.path.join(
            "id", hashlib.md5(playbook_id.encode()).hexdigest()[:2]
        )
        os.makedirs(os.path.join(self.root, id_relpath), exist_ok=True)

        pb_name = "{:%d-%H%M%S-%f}-{}.json".format(
            start, playbook.replace("/", "-")
        )

        pb_relpathname = os.path.join(pb_relpath, pb_name)

        pb_abspath = os.path.join(self.root, pb_relpathname)
        with open(pb_abspath, "wt") as fd:
            json.dump(
                {"playbook": playbook, "results": results}, fd, indent=1
            )

        with open(pb_abspath + ".summary", "wt") as fd:
            json.dump(
                {
                    "playbook": playbook,
                    "start": start.timestamp(),
                    "end": end.timestamp(),
                    "id": playbook_id,
                    "title": first_play["name"],
                    "has_ok": has_ok,
                    "has_changed": has_changed,
                    "has_failures": has_failures,
                    "has_unreachable": has_unreachable,
                },
                fd,
                indent=1,
            )

        os.symlink(
            os.path.join("..", "..", pb_relpathname),
            os.path.join(self.root, id_relpath, playbook_id),
        )

    def load(self, playbook_id):
        """
        Load playbook data by ID
        """
        id_relpath = hashlib.md5(playbook_id.encode()).hexdigest()[:2]
        try:
            with open(
                os.path.join(self.root, "id", id_relpath, playbook_id), "rt"
            ) as fd:
                return json.load(fd)
        except (OSError, json.JSONDecodeError):
            return None

    def get_first_month(self):
        re_month = re.compile(r"^(\d{4})-(\d{2})$")
        minval = None
        if not os.path.exists(self.root):
            return None
        for fn in os.listdir(self.root):
            mo = re_month.match(fn)
            if not mo:
                continue
            cur = (int(mo.group(1)), int(mo.group(2)))
            if minval is None or cur < minval:
                minval = cur
        if minval is None:
            return None
        else:
            return datetime.date(minval[0], minval[1], 1)

    def list_month(self, year, month):
        """
        Return a list of all playbook summaries in the given month
        """
        month_dir = os.path.join(
            self.root, "{:04d}-{:02d}".format(year, month)
        )
        if not os.path.exists(month_dir):
            return
        for fn in sorted(os.listdir(month_dir)):
            if not fn.endswith(".json.summary"):
                continue
            with open(os.path.join(month_dir, fn), "rt") as fd:
                yield json.load(fd)


class Ansible:
    def __init__(self, event_hub, config):
        self.log = logging.getLogger(self.__class__.__name__)
        self.event_hub = event_hub
        self.config = config
        self.playbook_log = PlaybookLog(config.playbook_log_dir)

    def list_playbooks(self):
        res = []
        base = self.config.path_playbooks
        yaml = ruamel.yaml.YAML()
        for root, dirs, fnames in os.walk(base):
            for fn in fnames:
                if fn.endswith(".yml") or fn.endswith(".yaml"):
                    abspath = os.path.join(root, fn)

                    info = {
                        "name": os.path.relpath(abspath, base),
                    }

                    with open(abspath, "rt") as fp:
                        try:
                            playbook = yaml.load(fp)
                        except Exception as e:
                            self.log.warning(
                                "Cannot load playbook %s: %s", abspath, e
                            )
                            playbook = None

                    try:
                        if playbook is not None:
                            info["title"] = playbook[0].get("name")
                    except Exception as e:
                        self.log.warning(
                            "Cannot extract information from playbook %s: %s",
                            abspath,
                            e,
                        )
                        playbook = None

                    res.append(info)

        res.sort(key=lambda x: x["name"])
        return res

    async def run_playbook(self, playbook_name, hosts="all"):
        playbook = os.path.join(self.config.path_playbooks, playbook_name)
        runner = PlaybookRunner(self, playbook_name, playbook)
        await runner.run(hosts)

    async def gather_facts(self, hosts="all"):
        collector = FactsCollector(self)
        await collector.run(hosts)

    async def wake_on_lan(self, macs=()):
        """
        Asyncronously run ansible wake on lan
        """
        cmd_kw = dict(ANSIBLE_BASE_KW)
        cmd_kw["stdout"] = asyncio.subprocess.DEVNULL
        cmd_kw["stderr"] = asyncio.subprocess.DEVNULL

        procs = []
        for m in macs:
            cmd = [
                'ansible',
                '-m', 'wakeonlan',
                '-a', 'mac=' + m,
                '-i', 'localhost,',
                '-c', 'local',
                'all',
            ]
            # https://docs.ansible.com/ansible/latest/modules/wakeonlan_module.html
            # wakeonlan does not know whether the magic packet has
            # worked, so we have no reason to store its output.
            procs.append(await asyncio.create_subprocess_exec(*cmd, **cmd_kw))

        results = await asyncio.gather(*(p.wait() for p in procs))
        return all(x == 0 for x in results)


async def store_stream(stream, dest):
    """
    Append lines from stream to dest
    """
    while True:
        line = await stream.readline()
        if not line:
            break
        dest.append(line)


class PlaybookRunner:
    """
    Asyncronously run an ansible playbook on an inventory of hosts.
    """

    def __init__(self, ansible, playbook_name, playbook):
        self.ansible = ansible
        self.playbook_name = playbook_name
        self.playbook = playbook

    def get_cmd(self, hosts):
        return [
            'ansible-playbook',
            '-i', self.ansible.config.path_ansible_inventory,
            '-l', hosts,
            '-u', 'root',
            self.playbook,
        ]

    async def run(self, hosts):
        cmd = self.get_cmd(hosts)
        proc = await asyncio.create_subprocess_exec(*cmd, **ANSIBLE_BASE_KW)
        stdout = []
        stderr = []
        await asyncio.wait(
            [
                store_stream(proc.stdout, stdout),
                store_stream(proc.stderr, stderr),
            ]
        )
        res = await proc.wait()
        if res != 0:
            self.ansible.log.error(
                "running playbook %s on %s failed. stderr: %r",
                self.playbook,
                hosts,
                b"\n".join(stderr),
            )
            # When a playbook fails, ansible helpfully prints on stdout
            # ``to retry, use: --limit @<playbook>.retry``
            # followed by the json output we expect, so we need to
            # remove that line before parsing the json.
            stdout.pop(0)
        await self.parse_stdout(stdout)
        return res == 0

    async def parse_stdout(self, lines):
        data = json.loads(b"".join(lines).decode())
        first_play = data["plays"][0]["play"]
        playbook_id = first_play["id"]

        self.ansible.playbook_log.log(self.playbook_name, data)
        await self.ansible.event_hub.publish(
            events.AnsiblePlaybookEvent(
                self.ansible,
                playbook=self.playbook_name,
                playbook_id=playbook_id,
                results=data,
            )
        )

        for host in data["stats"].keys():
            await self.ansible.event_hub.publish(
                events.HostPlaybookEvent(
                    self.ansible,
                    name=host,
                    playbook=self.playbook_name,
                    playbook_id=playbook_id,
                )
            )


class FactsCollector:
    """
    Asyncronously run ansible data gathering on an inventory of hosts.
    """

    def __init__(self, ansible):
        self.ansible = ansible
        self.stdout = []

    def get_cmdline(self, hosts):
        return [
            'ansible',
            '-m', 'setup',
            '-i', self.ansible.config.path_ansible_inventory,
            '-u', 'root',
            hosts,
        ]

    async def run(self, hosts="all"):
        cmd = self.get_cmdline(hosts)
        proc = await asyncio.create_subprocess_exec(*cmd, **ANSIBLE_BASE_KW)
        stderr = []
        await asyncio.wait(
            [
                self.parse_stdout(proc.stdout),
                store_stream(proc.stderr, stderr),
            ]
        )
        res = await proc.wait()
        if res != 0:
            self.ansible.log.error(
                "gathering ansible facts on %s failed. stderr: %r",
                hosts,
                b"\n".join(stderr),
            )
        return res == 0

    async def parse_stdout(self, stream):
        """
        Parse ansible facts as they come
        """
        while True:
            line = await stream.readline()
            if not line:
                break
            self.stdout.append(line)
            if line.rstrip() == b"}":
                await self.parse_facts()
                self.stdout = []

    async def parse_facts(self):
        # a timestamp taken on the server, because the facts usually
        # include a timestamp from the target, which could have a
        # drifting clock.
        timestamp = time.time()
        if not self.stdout:
            self.ansible.log.warning("ansible facts output is empty")
            return
        mo = re_head.match(self.stdout[0].decode())
        if not mo:
            self.ansible.log.warning(
                "ansible facts output has malformed header line %r",
                self.stdout[0],
            )
            return

        host = mo.group("host")
        result = mo.group("result")

        self.stdout[0] = b"{\n"
        payload = b"".join(self.stdout).decode()
        data = json.loads(payload)
        data['timestamp'] = timestamp

        if result != "SUCCESS":
            await self.ansible.event_hub.publish(
                events.HostFactsFailed(
                    self.ansible, name=host, result=result, details=data,
                )
            )
        else:
            await self.ansible.event_hub.publish(
                events.HostFactsEvent(self.ansible, name=host, facts=data,)
            )
