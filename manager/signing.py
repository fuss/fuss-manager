try:
    import django.core.signing
    from django.core.signing import BadSignature, SignatureExpired

    HAVE_SIGNING = True
except ModuleNotFoundError:
    HAVE_SIGNING = False

    class BadSignature(Exception):
        pass

    class SignatuerExpired(BadSignature):
        pass


if HAVE_SIGNING:

    def loads(s, key, salt, max_age=None):
        return django.core.signing.loads(s, key, salt, max_age=max_age)

    def dumps(obj, key, salt, compress=False):
        return django.core.signing.dumps(obj, key, salt, compress=False)


else:

    def loads(s, key, salt, max_age=None):
        raise NotImplementedError(
            "This functionality requires django to be installed"
        )

    def dumps(obj, key, salt, compress=False):
        raise NotImplementedError(
            "This functionality requires django to be installed"
        )


__all__ = ("BadSignature", "SignatureExpired", "loads", "dumps")
