import asyncio
import logging
import os

from .base import DataSource, MachineDataSourceMixin
from .. import events, compat, utils
from .inotifywait import Inotifywait


class ArpwatchDataSource(MachineDataSourceMixin, DataSource):
    """
    """

    def __init__(self, config=None, datafile=None, **kw):
        super().__init__(**kw)
        if datafile:
            self.fname = datafile
        else:
            self.fname = os.path.join(config.arpwatch_datadir, 'arp.dat')
        self.watch = Inotifywait(self.fname)
        loop = asyncio.get_event_loop()
        self.started = compat.create_future(loop)

    async def shutdown(self):
        self.watch.stop()

    async def wait_until_started(self):
        await self.started

    async def read_events(self):
        while True:
            event = await self.watch.next_event()
            if event == "MODIFIED":
                await self.parse_file()
            elif event == "END":
                break
            elif event == "START":
                self.started.set_result(True)
                await self.event_hub.publish(events.DataSourceStartEvent(self))
        await self.event_hub.publish(events.DataSourceStopEvent(self))

    async def run(self):
        await asyncio.gather(
            self.watch.run(), self.read_events(),
        )

    @classmethod
    def is_viable(cls, config, datafile=None):
        datafile = datafile or os.path.join(config.arpwatch_datadir, 'arp.dat')
        if not super().is_viable(config):
            return False
        if not os.path.isfile(datafile):
            return False
        return Inotifywait.is_viable(config)

    async def parse_file(self):
        with open(self.fname) as fp:
            for line in fp:
                parts = line.split()
                ts = int(parts[2])
                mac = parts[0]
                ip = parts[1]
                if utils.check_ip(ip) and utils.check_mac(mac):
                    await self.event_hub.publish(
                        events.HostSeenEvent(
                            self, mac=mac, ip=ip, timestamp=ts,
                        )
                    )
                else:
                    logging.warning(
                        "Found unparseable arpwatch line: %s", line
                    )
