import ipaddress
import logging
import re

from ..config import Config
from .. import utils

"""
Data sources for the fuss-manager.

To create a new data source, inherit from DataSource or one of its
generic children, and then conditionally load the data source in
manager.app.Application() so that it is run.
"""


class DataSource:
    """
    Base class to collect data from the system.
    """

    def __init__(self, event_hub, **kw):
        self.event_hub = event_hub
        self.log = logging.getLogger(self.__class__.__name__)

    @classmethod
    def is_viable(cls, config: Config) -> None:
        """
        Check if this DataSource can run.

        Override this in children classes with system tests that e.g.
        check that the right dependencies are installed.
        """
        return cls.__name__ not in config.disabled_sources

    async def run(self):
        """
        Run the data source.

        Override this in children classes
        """

    async def shutdown(self):
        """
        Shut down the data source

        Override this in children classes
        """
        pass


class MachineDataSourceMixin:
    """
    """

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.macs = []
        self.ips = []
        self.names = []

    def seen_mac(self, mac):
        if mac not in self.macs:
            self.macs.append(mac)

    def seen_ip(self, ip):
        if ip not in self.ips:
            self.ips.append(ip)

    def seen_name(self, name):
        if name not in self.names:
            self.names.append(name)
