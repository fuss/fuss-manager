import asyncio
import random
from .base import DataSource
from .. import events, compat


MOCK_NETWORK = [
    {
        "mac": "02:00:00:01:02:01",
        "ip": "192.168.123.2",
        "name": "test1",
    }, {
        "mac": "02:00:00:01:02:02",
        "ip": "192.168.123.3",
        "name": "test2",
    }
]


class ChaosDataSource(DataSource):
    """
    """

    def __init__(self, config=None, **kw):
        super().__init__(**kw)
        loop = asyncio.get_event_loop()
        self.started = compat.create_future(loop)
        self.stopped = False
        self.sleep_task = None

    async def shutdown(self):
        self.stopped = True
        if self.sleep_task is not None:
            self.sleep_task.cancel()

    async def wait_until_started(self):
        await self.started

    async def run(self):
        self.started.set_result(True)
        await self.event_hub.publish(events.DataSourceStartEvent(self))

        while not self.stopped:
            # Interruptible sleep
            delay = random.randint(0, 50) / 10.0
            self.sleep_task = compat.create_task(asyncio.sleep(delay))
            try:
                await self.sleep_task
            except asyncio.CancelledError:
                break
            self.sleep_task = None

            host = random.choice(MOCK_NETWORK)
            await self.event_hub.publish(events.HostSeenEvent(self, **host))

        await self.event_hub.publish(events.DataSourceStopEvent(self))

    @classmethod
    def is_viable(cls, config):
        if not super().is_viable(config):
            return False
        return config.debug is True
