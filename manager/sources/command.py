import asyncio
import signal
from .base import DataSource
from .. import events


class CommandDataSource(DataSource):
    """
    Collect data by watching the output of a program

    This is a base data source which should be inherited upon by
    command-specific DataSources that know how to interpreter the
    command output.
    """

    def __init__(self, command, logs=[], *args, **kw):
        """
        Initialize a CommandDataSource that runs ``command``.

        streams listed in ``logs`` are sent to the logs.

        Note that logging a stream will consume it, and thus interfere
        with parsing the same stream.
        """
        super().__init__(*args, **kw)
        self.command = command
        self.logs = logs
        self.proc = None

    async def run(self):
        """
        Run the command and call parsing and logging of its output.
        """
        # we need the kws in a dict to unpack self.command in python <
        # 3.5:
        # https://docs.python.org/3/whatsnew/3.5.html#whatsnew-pep-448
        command_kw = {
            'stdout': asyncio.subprocess.PIPE,
            'stderr': asyncio.subprocess.PIPE,
            # the following is useful in case the watched file is in
            # python, and does not hurt otherwise.
            'env': {'PYTHONUNBUFFERED': '1'},
        }
        proc = await asyncio.create_subprocess_exec(
            *self.command, **command_kw
        )

        parsers = [
            self.parse_stdout(proc.stdout),
            self.parse_stderr(proc.stderr),
        ]
        for stream in self.logs:
            parsers.append(self.log_stream(getattr(proc, stream), stream))

        self.proc = proc

        # FIXME: use something else than a wait, if possible. It isn't
        # with tail -f.
        await asyncio.sleep(0.1)
        await self.event_hub.publish(events.DataSourceStartEvent(self))

        await asyncio.wait(parsers)

        res = await self.proc.wait()
        if res != -signal.SIGTERM:
            self.log.warn("%s: exited with error code %d", self.command, res)
        await self.event_hub.publish(events.DataSourceStopEvent(self))

    async def wait_until_started(self):
        while self.proc is None:
            await asyncio.sleep(0.1)

    async def shutdown(self):
        await self.wait_until_started()
        self.send_signal(signal.SIGTERM)

    def send_signal(self, signal):
        self.proc.send_signal(signal)

    async def parse_stdout(self, stream):
        """
        Parse the stdout of the program.

        Override this in children classes with a method that is aware of
        the expected output.
        """
        # This simple default puts all lines in a queue, if available,
        # and is used in the tests to check that the parsing is
        # correctly called.
        while True:
            line = await stream.readline()
            if not line:
                break
            if self.queue:
                await self.queue.put(line)

    async def parse_stderr(self, stream):
        """
        Parse the stderr of the program.

        Override this in children classes with a method that is aware of
        the expected output.
        """
        # This default is empty so that the contents of stderr are not
        # consumed and are available for logging, especially in the
        # tests where this class is directly used.
        pass

    async def log_stream(self, stream, label):
        """
        Log all contents of one stream, prefixed with its source.

        Log format is <command>: <stream label>: <line from the stream>.
        """
        while True:
            line = await stream.readline()
            if not line:
                break
            self.log.warn("{}: {}: {}".format(self.command, label, line,))
