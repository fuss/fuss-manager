import os

import logging

import dateutil.parser

from .. import events, utils
from .base import MachineDataSourceMixin
from .command import CommandDataSource


class DhcpdDataSource(MachineDataSourceMixin, CommandDataSource):
    """
    Read lease data from dhcpd.

    Keep lists of all seen mac and ip addresses, and names.
    """

    def __init__(self, config=None, **kw):
        # TODO: change this with a command to read DCHP lease data
        command = ['tail', '-f', config.dhcp_log_file]
        super().__init__(command, **kw)
        self.log_file = config.dhcp_log_file

    async def parse_stdout(self, stream):
        while True:
            line = await stream.readline()
            if not line:
                break
            split_line = line.split()
            source = split_line[4]
            if b'dhcp' not in source:
                continue
            date = b' '.join(split_line[:2])
            timestamp = dateutil.parser.parse(date).timestamp()
            message = b' '.join(split_line[5:])
            if message.startswith(b'DHCPDISCOVER'):
                mac = message.split()[2].decode()
                if utils.check_mac(mac):
                    await self.event_hub.publish(
                        events.HostSeenEvent(
                            self, mac=mac, timestamp=timestamp,
                        )
                    )
                else:
                    logging.warning(
                        "Received an unparsable DHCPDISCOVER message: %r", line
                    )
            elif message.startswith(b'DHCPACK'):
                mac = message.split()[4].decode()
                ip = message.split()[2].decode()
                if utils.check_mac(mac) and utils.check_ip(ip):
                    await self.event_hub.publish(
                        events.HostSeenEvent(
                            self, mac=mac, ip=ip, timestamp=timestamp,
                        )
                    )
                else:
                    logging.warning(
                        "Received an unparsable DHCPACK message: %r", line
                    )
            elif message.startswith(b'Added new forward map'):
                ip = message.split()[7].decode()
                name = message.split()[5].decode()
                if utils.check_ip(ip) and utils.check_hostname(name):
                    await self.event_hub.publish(
                        events.HostSeenEvent(
                            self, ip=ip, name=name, timestamp=timestamp,
                        )
                    )
                else:
                    logging.warning(
                        "Received an unparsable forward map message: %r", line
                    )

    @classmethod
    def is_viable(cls, config):
        if not super().is_viable(config):
            return False
        if not os.access(config.dhcp_log_file, os.R_OK):
            return False
        return True
