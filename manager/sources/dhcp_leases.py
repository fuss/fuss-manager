import asyncio
import logging
import os

import isc_dhcp_leases

from .base import DataSource, MachineDataSourceMixin
from .. import events, compat
from .inotifywait import Inotifywait


class DhcpLeasesDataSource(MachineDataSourceMixin, DataSource):
    """
    """

    def __init__(self, config=None, datafile=None, **kw):
        super().__init__(**kw)
        if datafile:
            self.fname = datafile
        else:
            self.fname = config.dhcp_leases_file
        self.watch = Inotifywait(self.fname)
        loop = asyncio.get_event_loop()
        self.started = compat.create_future(loop)

    async def shutdown(self):
        self.watch.stop()

    async def wait_until_started(self):
        await self.started

    async def read_events(self):
        while True:
            event = await self.watch.next_event()
            if event == "MODIFIED":
                await self.parse_file()
            elif event == "END":
                break
            elif event == "START":
                self.started.set_result(True)
                await self.event_hub.publish(events.DataSourceStartEvent(self))
        await self.event_hub.publish(events.DataSourceStopEvent(self))

    async def run(self):
        await asyncio.gather(
            self.watch.run(), self.read_events(),
        )

    @classmethod
    def is_viable(cls, config, datafile=None):
        datafile = datafile or config.dhcp_leases_file
        if not super().is_viable(config):
            return False
        if not os.path.isfile(datafile):
            return False
        return Inotifywait.is_viable(config)

    async def parse_file(self):
        leases = isc_dhcp_leases.IscDhcpLeases(self.fname)
        for l in leases.get():
            mac = getattr(l, l.hardware, None)
            if not mac:
                continue
            ip = getattr(l, 'ip', None)
            ts = l.start.timestamp()
            await self.event_hub.publish(
                events.HostSeenEvent(
                    self,
                    mac = mac,
                    ip = ip,
                    timestamp = ts,
                )
            )

