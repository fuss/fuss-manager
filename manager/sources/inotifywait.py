import asyncio
import shutil
import signal
import logging
import os


class Inotifywait:
    """
    Use inotifywait to wait until a file changes
    """

    # The best way here would be to use a python interface to
    # inotify: this is a quick and dirty workaround because the
    # state of maintainance of python3-pyinotify is somewhat
    # dubious.
    path_inotifywait = shutil.which("inotifywait")

    def __init__(self, fname=None):
        self.log = logging.getLogger(self.__class__.__name__)
        self.dirname = os.path.dirname(fname)
        self.basename = os.path.basename(fname)
        self.proc = None
        self.queue = asyncio.Queue()

    @classmethod
    def is_viable(cls, config) -> bool:
        return cls.path_inotifywait is not None

    def has_events(self) -> bool:
        """
        Return True if there are pending events
        """
        return self.queue.qsize() > 0

    async def next_event(self) -> str:
        """
        Return the next event in the queue. Events can be;

        "START": inotifywait has started and set up all watches
        "MODIFIED": the file has been modified
        "END": inotifywait has quit
        """
        return await self.queue.get()

    async def run(self):
        # To catch when a file is updated atomically using move, as in:
        #   echo "new contents" > file.tmp
        #   mv file.tmp file
        # We need to watch the whole directory for "CLOSE_WRITE" and "MOVED_TO"
        # with the right file name.
        # MODIFY is required for files that are modified in the regular
        # way, instead.
        self.log.debug("monitoring %s in %s", self.basename, self.dirname)
        self.proc = await asyncio.create_subprocess_exec(
            self.path_inotifywait,
            '-m',
            '-e', 'CLOSE_WRITE',
            "-e", "MOVED_TO",
            "-e", "MODIFY",
            "--format", "%e %f",
            self.dirname,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        await asyncio.gather(
            self.check_stdout(self.proc.stdout),
            self.check_stderr(self.proc.stderr),
        )

        res = await self.proc.wait()
        if res != -signal.SIGTERM:
            self.log.warn(
                "%s exited with error code %d", self.path_inotifywait, res
            )
        if self.queue:
            await self.queue.put("END")

    async def check_stdout(self, stream):
        while True:
            line = await stream.readline()
            if not line:
                break
            events, fname = line.strip().decode("utf-8").split(None, 1)
            if fname != self.basename:
                continue
            await self.queue.put("MODIFIED")

    async def check_stderr(self, stream):
        while True:
            line = await stream.readline()
            if not line:
                break
            line = line.strip()
            if line == b"Setting up watches.":
                pass
            elif line == b"Watches established.":
                await self.queue.put("START")
            else:
                self.log.warn("inotifywait stderr: %s", line)

    def stop(self):
        self.proc.send_signal(signal.SIGTERM)
