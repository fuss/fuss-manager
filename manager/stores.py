from typing import Optional, Dict, Tuple, TypeVar, Iterable
from typing import List  # noqa: currently used in a mypy type comment
import asyncio
import copy
import json
import logging
import os
import socket
import time
import ruamel.yaml
import aiodns

from . import events, utils

"""
Data store for the fuss-manager.
"""


T = TypeVar("T")

# Convenience variable with all of the fields of a machine that can be
# saved as json
ALL_MACHINE_FIELDS = (
    "mac",
    "ip",
    "name",
    "first_seen",
    "last_seen",
    "registered",
    "groups",
    "host_vars",
    "facts",
    "facts_log",
)
# Fields of a machine that we want to save in the stats file (excluding
# what is already saved in the inventory and what isn't sensible to
# save).
STATS_MACHINE_FIELDS = (
    "mac",
    "ip",
    "name",
    "first_seen",
    "last_seen",
    "registered",
    "facts",
    "facts_log",
)


class RRList(list):
    """
    A Round Robin list of timestamped entries that will drop older
    entries.
    """

    def __init__(
        self,
        *args,
        max_age: Optional[int] = 60,  # days
        max_size: Optional[int] = None,
        **kw
    ):
        super().__init__(*args, **kw)
        self.max_age = max_age
        self.max_size = max_size

    def append(self, *args, **kw):
        super().append(*args, **kw)
        self.cleanup()

    def cleanup(self):
        if self.max_size:
            while len(self) > self.max_size:
                self.pop(0)
        if self.max_age:
            cut_stamp = time.time() - self.max_age * 24 * 60 * 60
            while len(self) > 0 and self[0].get('timestamp', 0) < cut_stamp:
                self.pop(0)


class Machine:
    """
    Known data about a machine
    """

    def __init__(
        self,
        mac: str,
        ip: Optional[str] = None,
        name: Optional[str] = None,
        registered: Optional[bool] = None,
        first_seen: Optional[float] = None,
        last_seen: Optional[float] = None,
        groups: Iterable[str] = None,
        host_vars: Optional[dict] = None,
        facts: Optional[dict] = None,
        facts_log: Optional[list] = None,
    ):
        self.mac = mac
        self.ip = ip
        # A machine always needs a name
        self.name = name or ip or mac
        self.registered = registered
        self.first_seen = first_seen
        self.last_seen = last_seen
        self.groups = list(groups or ())  # type: List[str]
        self.host_vars = host_vars or {}
        self.facts = facts
        self.facts_log = RRList(facts_log or [])

    def to_jsonable(self, fields: Iterable[str] = ()):
        if not fields:
            fields = ALL_MACHINE_FIELDS
        res = {}
        for name in fields:
            val = getattr(self, name)
            if val is not None:
                res[name] = val
        return res

    def update(self, evt: events.HostSeenEvent) -> Dict[str, Tuple[T, T]]:
        """
        Update the machine data according to what found in the event.

        Return a dict mapping the name of the field that changed to a couple
        (old value, new value).
        """
        changes = {}
        if self.last_seen < evt.timestamp:
            changes["last_seen"] = (self.last_seen, evt.timestamp)
            self.last_seen = evt.timestamp
        else:
            # If the event has an older timestamp than the machine,
            # ignore it
            return {}
        if evt.ip is not None and self.ip != evt.ip:
            changes["ip"] = (self.ip, evt.ip)
            self.ip = evt.ip
        if evt.name is not None and self.name != evt.name:
            changes["name"] = (self.name, evt.name)
            self.name = evt.name
        if evt.registered is not None and self.registered != evt.registered:
            changes["registered"] = (self.registered, evt.registered)
            self.registered = evt.registered
        # When we are using the mac as a name, try to get something
        # better as soon as possible.
        if self.name == self.mac and self.ip is not None:
            self.name = self.ip
        # When using the ip as a name, keep it up to date with changes
        if "ip" in changes and self.name == changes["ip"][0]:
            self.name = self.ip
        return changes

    @classmethod
    def from_jsonable(cls, fields):
        return cls(**fields)

    def update_from_jsonable(self, fields):
        for f in ALL_MACHINE_FIELDS:
            if f in fields:
                setattr(self, f, fields[f])

    @classmethod
    def from_host_seen_event(cls, evt: events.HostSeenEvent) -> "Machine":
        return cls(
            mac=evt.mac,
            ip=evt.ip,
            name=evt.name,
            first_seen=evt.timestamp,
            last_seen=evt.timestamp,
            registered=evt.registered,
        )


class MachineStore:
    """
    """

    def __init__(
        self, event_hub, config,
    ):
        self.log = logging.getLogger(self.__class__.__name__)
        self.event_hub = event_hub
        # mac -> Machine
        self.machines = {}
        self.inventory_fn = config.path_ansible_inventory
        self.stats_fn = config.path_stats_cache
        self.event_hub.subscribe(self.host_seen, [events.HostSeenEvent])
        self.event_hub.subscribe(self.facts_loaded, [events.HostFactsEvent])
        self.event_hub.subscribe(self.facts_failed, [events.HostFactsFailed])
        loop = asyncio.get_event_loop()
        self.resolver = aiodns.DNSResolver(loop=loop)

    def get_machine(
        self, ip: Optional[str] = None, name: Optional[str] = None,
    ):
        """
        Get a machine by ip or name.

        If there is more than one machine with the same ip / name,
        returns the first one.
        """
        if sum((bool(x) for x in (ip, name))) > 1:
            raise NotImplementedError
        if ip:
            for m in self.machines.values():
                if m.ip == ip:
                    return m
        if name:
            for m in self.machines.values():
                if m.name == name:
                    return m
        return None

    async def add_machine_to_group(self, mac: str, group: str):
        machine = self.machines.get(mac)
        if machine is None:
            return
        if group in machine.groups:
            return
        old_groups = tuple(machine.groups)
        machine.groups.append(group)
        await self.event_hub.publish(
            events.HostChangedEvent(
                self,
                mac=mac,
                changes={"groups": (old_groups, tuple(machine.groups))},
            )
        )
        await self.flush()

    async def remove_machine_from_group(self, mac: str, group: str):
        machine = self.machines.get(mac)
        if machine is None:
            return
        if group not in machine.groups:
            return
        old_groups = tuple(machine.groups)
        machine.groups.remove(group)
        await self.event_hub.publish(
            events.HostChangedEvent(
                self,
                mac=mac,
                changes={"groups": (old_groups, tuple(machine.groups))},
            )
        )
        await self.flush()

    async def host_seen(self, evt):
        """
        Confirm that a machine has been seen recently.

        One of mac, ip or name should be present to identify the
        machine; the missing data will be discovered.
        TODO: decide whether to trust calls that specify more than one,
        or check the validity of the data.

        If a timestamp is provided, the machine is only refreshed if the
        timestamp is more recent than the last known one.
        """
        if not evt.ip and not evt.mac and not evt.name:
            return

        # Make sure we have at least a MAC, because we need it to identify the
        # machine in the store
        if not evt.mac:
            if not evt.ip:
                evt.ip = await self.name_to_ip(evt.name)
            evt.mac = await self.ip_to_mac(evt.ip)

        machine = self.machines.get(evt.mac)
        if machine is None:
            # New machine: try to get all possible information
            if not evt.ip:
                evt.ip = await self.mac_to_ip(evt.mac)
            if not evt.name and evt.ip is not None:
                evt.name = await self.ip_to_name(evt.ip)
            if evt.registered is None:
                evt.registered = await self.is_registered(evt.name)
            machine = Machine.from_host_seen_event(evt)
            self.machines[machine.mac] = machine
            await self.event_hub.publish(
                events.HostNewEvent(
                    self,
                    timestamp=evt.timestamp,
                    mac=machine.mac,
                    ip=machine.ip,
                    name=machine.name,
                )
            )
            await self.flush()
        else:
            # Existing machine: only keep track of data that changed
            changes = machine.update(evt)
            if changes:
                await self.event_hub.publish(
                    events.HostChangedEvent(
                        self,
                        timestamp=evt.timestamp,
                        mac=evt.mac,
                        changes=changes,
                    )
                )
                await self.flush()

    async def facts_loaded(self, evt):
        machine = self.get_machine(name=evt.name)
        old_facts_log = machine.facts_log.copy()
        machine.facts_log.append(
            {'timestamp': evt.timestamp, 'result': 'SUCCESS', 'details': {}}
        )
        changes = {
            "facts": (machine.facts, evt.facts),
            "facts_log": (old_facts_log, machine.facts_log),
        }
        machine.facts = evt.facts
        if not machine.registered:
            machine.registered = True
            changes['registered'] = (False, True)
        await self.event_hub.publish(
            events.HostChangedEvent(self, mac=machine.mac, changes=changes)
        )

    async def facts_failed(self, evt):
        machine = self.get_machine(name=evt.name)
        old_facts_log = machine.facts_log.copy()
        machine.facts_log.append(
            {
                'timestamp': time.time(),
                'result': evt.result,
                'details': evt.details,
            }
        )
        changes = {
            "facts_log": (old_facts_log, machine.facts_log),
        }
        await self.event_hub.publish(
            events.HostChangedEvent(self, mac=machine.mac, changes=changes)
        )

    async def mac_to_ip(self, mac):
        """
        Find the IP of a machine with a known mac address.
        """
        proc = await asyncio.create_subprocess_shell(
            '/usr/sbin/arp -n', stdout=asyncio.subprocess.PIPE,
        )
        out = await proc.communicate()
        for line in out[0].decode().split('\n'):
            if line and mac in line:
                ip = line.split()[0]
                if utils.check_ip(ip):
                    return line.split()[0]
        return None

    async def ip_to_mac(self, ip):
        """
        Find the mac address of a machine with a known ip.
        """
        proc = await asyncio.create_subprocess_shell(
            '/usr/sbin/arp -n {ip}'.format(ip=ip),
            stdout=asyncio.subprocess.PIPE,
        )
        out = await proc.communicate()
        if ip in out[0].decode().split('\n')[1]:
            mac = out[0].decode().split('\n')[1][33:50]
            if utils.check_mac(mac):
                return mac
        return None

    async def ip_to_name(self, ip):
        """
        Find the fqdn of a machine with a known ip.
        """
        try:
            name = socket.gethostbyaddr(ip)[0]
        except socket.herror:
            name = None
        if not utils.check_hostname(name):
            name = None
        # TODO: aiodns 1.2.0 adds a gethostbyaddr which can be used in
        # the following way
        # res = await self.resolver.gethostbyaddr(ip)
        # name = res.name
        return name or ip

    async def name_to_ip(self, name):
        """
        Find the ip of a machine with a known fqdn.
        """
        res = await self.resolver.gethostbyname(name, socket.AF_INET)
        ip = res.addresses[0]
        if utils.check_ip:
            return ip
        return None

    async def is_registered(self, name):
        """
        Check if the machine at name is registered.

        Registered means that we can connect via ssh as root to it.

        Return None for machines that can't be reached (as they are
        probably off), true for machines that are registered, false
        otherwise.
        """
        proc = await asyncio.create_subprocess_shell(
            'ssh -oStrictHostKeyChecking=yes root@{n} "whoami"'.format(n=name),
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        out, err = await proc.communicate()

        if b'No route to host' in err:
            return None

        return b'root\n' == out

    async def flush(self):
        """
        Save all current data to file.
        """
        self.save_inventory()
        self.save_stats_file()

    async def load(self):
        await self.load_stats_file()
        await self.load_inventory()

    async def load_inventory(self):
        """
        Read machine data from an ansible inventory.
        """
        inventory = SimpleAnsibleInventory(self.inventory_fn)
        inventory.load()
        for m, g in inventory.get_machines():
            # if there is no macaddress in the inventory, we try to load
            # it from the network, otherwise the machine will have to be
            # dropped.
            name = m[0]
            if not m[1] or 'macaddress' not in m[1]:
                ip = await self.name_to_ip(name)
                mac = await self.ip_to_mac(ip)
            else:
                mac = m[1]['macaddress']
                ip = None
            if not mac:
                self.log.warning(
                    "Could not find a valid mac for {m}, it will be removed".format(m=m[0],)  # noqa: E501
                )
                continue

            # and then we add the machine if it's not already available,
            # and add it to the relevant groups and load its variables
            if mac not in self.machines:
                self.machines[mac] = Machine(mac=mac, name=name, ip=ip,)
            for var, value in m[1].items():
                if var != 'macaddress':
                    self.machines[mac].host_vars[var] = value
            self.machines[mac].groups.append(g)

    def save_inventory(self):
        """
        Save machine data to an ansible inventory.
        """
        inventory = SimpleAnsibleInventory(self.inventory_fn)
        for m in self.machines.values():
            inventory.add_machine(m)
        inventory.save()

    async def load_stats_file(self):
        """
        Read machine statistics from file.
        """
        try:
            with open(self.stats_fn) as fp:
                loaded_stats = json.load(fp)
        except FileNotFoundError:
            loaded_stats = {}
        for mac, m in loaded_stats.items():
            if mac in self.machines:
                self.machines[mac].update_from_jsonable(m)
            else:
                machine = Machine.from_jsonable(m)
                self.machines[machine.mac] = machine

    def save_stats_file(self):
        """
        Save machine statistics to file.
        """
        stats = {
            m.mac: m.to_jsonable(fields=STATS_MACHINE_FIELDS)
            for m in self.machines.values()
        }
        with open(self.stats_fn, 'w') as fp:
            json.dump(stats, fp)


class MockMachineStore(MachineStore):
    """
    A machine store that does not require external resources.
    """

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        with open('tests/data/local_network_data.json') as fp:
            self.network = json.load(fp)

    async def mac_to_ip(self, mac):
        """
        Find the IP of a machine with a known mac address.
        """
        for m in self.network:
            if m['mac'] == mac:
                return m.get('ip')
        return None

    async def ip_to_mac(self, ip):
        """
        Find the mac address of a machine with a known ip.
        """
        for m in self.network:
            if m.get('ip') == ip:
                return m['mac']
        return None

    async def ip_to_name(self, ip):
        """
        Find the fqdn of a machine with a known ip.
        """
        for m in self.network:
            if m.get('ip') == ip:
                return m.get('name')
        return ip

    async def name_to_ip(self, name):
        """
        Find the ip of a machine with a known fqdn.
        """
        for m in self.network:
            if m.get('name') == name:
                return m.get('ip')
        return None

    async def is_registered(self, name):
        """
        Check if the machine at name is registered.

        Registered means that we can connect via ssh as root to it.

        This mock version will return False, except for the first
        machine in our mock network.
        """
        if name == self.network[0]['name']:
            return True
        return False


class SimpleAnsibleInventory:
    """
    Manage an ansible inventory file.

    Only static formats are supported.
    """

    # This is kept separated from the MachineStore to make it easier to
    # reimplement it using ansible's own inventory parsing code.
    # At the moment this is problematic because ansible in jessie is
    # still using python2, and the stability of its interface still
    # needs to be evalued.

    # In case of a rewrite, this implementation can be kept as a backup
    # option in case of problems with the ansible-based one.
    def _get_empty_inventory(self):
        return copy.deepcopy({
            'all': {
                'hosts': {},
                'children': {},
            }
        })

    def __init__(self, fname: str, inv_format: Optional[str] = 'yaml'):
        self.inventory = self._get_empty_inventory()
        self.fname = fname
        self.var_dir = os.path.dirname(self.fname)
        self.inv_format = inv_format

    def load(
        self, fname: Optional[str] = None, inv_format: Optional[str] = None
    ):
        """
        Load inventory data.

        Use fname and/or inv_format to load data from a different file;
        this does not change the instance defaults (and thus further
        writes will use them).
        """
        fname = fname or self.fname
        inv_format = inv_format or self.inv_format

        if inv_format == 'ini':
            self.inventory = self._parse_ini(fname)
        elif inv_format == 'yaml':
            self.inventory = self._parse_yaml(fname)
        else:
            raise ValueError(
                '{} is not a supported format for an inventory'.format(
                    inv_format
                )
            )
        self._load_vars()

    def save(
        self, fname: Optional[str] = None, inv_format: Optional[str] = None
    ):
        """
        Save inventory data to file.

        Use fname and/or inv_format to save data to a different file;
        this does not change the instance defaults (and thus further
        writes will use them).
        """
        fname = fname or self.fname
        inv_format = inv_format or self.inv_format

        if inv_format == 'ini':
            self._save_ini(fname)
        elif inv_format == 'yaml':
            self._save_yaml(fname)
        else:
            raise ValueError(
                '{} is not a supported format for an inventory'.format(
                    inv_format
                )
            )
        self._save_vars()

    def _parse_ini(self, fname: str):
        # At the moment we're parsing just one level of children groups
        inventory = self._get_empty_inventory()
        cur_group = inventory['all']['hosts']
        try:
            with open(fname) as fp:
                for line in fp.readlines():
                    if not line.strip():
                        continue
                    elif line.startswith('['):
                        group = line.strip('[]\n\r\t ')
                        inventory['all']['children'][group] = {
                            'hosts': {},
                        }
                        cur_group = inventory['all']['children'][group][
                            'hosts'
                        ]
                    else:
                        split_l = line.strip().split()
                        cur_group[split_l[0]] = {}
                        for item in split_l[1:]:
                            try:
                                k, v = item.split('=')
                                cur_group[split_l[0]][k] = v
                            except ValueError as e:
                                # TODO: manage this error
                                print("Error loading inventory file", e)
        except FileNotFoundError:
            pass

        return inventory

    def _parse_yaml(self, fname: str):
        yaml = ruamel.yaml.YAML()
        try:
            with open(self.fname) as fp:
                inventory = yaml.load(fp)
        except FileNotFoundError:
            inventory = None
        return inventory or self._get_empty_inventory()

    def _save_ini(self, fname: str):
        with open(fname, 'w') as fp:
            for h, v in self.inventory['all']['hosts'].items():
                fp.write(
                    '{h} macaddress={m}\n'.format(h=h, m=v['macaddress']),
                )
            for c in self.inventory['all']['children']:
                fp.write("[{}]\n".format(c))
                for h, v in self.inventory['all']['children'][c][
                    'hosts'
                ].items():
                    fp.write(
                        '{h} macaddress={m}\n'.format(h=h, m=v['macaddress']),
                    )

    def _save_yaml(self, fname: str):
        yaml = ruamel.yaml.YAML()
        data = copy.deepcopy(self.inventory)
        for h, v in data['all']['hosts'].items():
            mac = v.get('macaddress')
            if mac:
                data['all']['hosts'][h] = {'macaddress': mac}
            else:
                data['all']['hosts'][h] = {}
        if 'children' in data['all']:
            for c in data['all']['children']:
                for h, v in data['all']['children'][c]['hosts'].items():
                    mac = v.get('macaddress')
                    if mac:
                        data['all']['children'][c]['hosts'][h] = {
                            'macaddress': mac
                        }
                    else:
                        data['all']['children'][c]['hosts'][h] = {}
        with open(fname, 'w') as fp:
            yaml.dump(data, fp)

    def _cycle_hosts(self, f):
        if self.inventory['all']['hosts']:
            for h, v in self.inventory['all']['hosts'].items():
                f(h, v)
        if 'children' in self.inventory['all']:
            for c in self.inventory['all']['children']:
                for h, v in self.inventory['all']['children'][c][
                    'hosts'
                ].items():
                    f(h, v)

    def _load_host_vars(self, h, v):
        var_file = os.path.join(self.var_dir, 'host_vars', h + '.yaml')
        if os.path.isfile(var_file):
            yaml = ruamel.yaml.YAML()
            with open(var_file) as fp:
                data = yaml.load(fp)
            for key, val in data.items():
                v[key] = val

    def _load_vars(self):
        self._cycle_hosts(self._load_host_vars)

    def _save_host_vars(self, host, variables):
        if not host:
            return
        data = {}
        for k, v in variables.items():
            if k != 'macaddress':
                data[k] = v
        yaml = ruamel.yaml.YAML()
        yaml.explicit_start = True
        os.makedirs(os.path.join(self.var_dir, 'host_vars'), exist_ok=True)
        with open(
            os.path.join(self.var_dir, 'host_vars', host + '.yaml'), 'w'
        ) as fp:
            yaml.dump(data, fp)

    def _save_vars(self):
        if not self.inventory:
            return
        self._cycle_hosts(self._save_host_vars)

    def add_machine(self, machine: Machine):
        if machine.groups:
            for g in machine.groups:
                if g == 'all':
                    self.inventory['all']['hosts'][machine.name] = {
                        'macaddress': machine.mac,
                    }
                    self.inventory['all']['hosts'][machine.name].update(
                        machine.host_vars
                    )
                else:
                    if g not in self.inventory['all']['children']:
                        self.inventory['all']['children'][g] = {'hosts': {}}
                    self.inventory['all']['children'][g]['hosts'][
                        machine.name
                    ] = {
                        'macaddress': machine.mac,
                    }
                    self.inventory['all']['children'][g]['hosts'][
                        machine.name
                    ].update(machine.host_vars)

        else:
            self.inventory['all']['hosts'][machine.name] = {
                'macaddress': machine.mac,
            }
            self.inventory['all']['hosts'][machine.name].update(
                machine.host_vars
            )

    def _group_machines(self, group, gname):
        for m in group['hosts'].items():
            yield m, gname
        if 'children' in group:
            for n, c in group['children'].items():
                yield from self._group_machines(c, n)

    def get_machines(self):
        yield from self._group_machines(self.inventory['all'], 'all')
