from dataclasses import dataclass, asdict, field
from typing import List, Dict, Any
from manager.config import Config


@dataclass
class Group:
    name: str = None
    id: int = None


@dataclass
class User:
    name: str = None
    id: int = None
    display_name: str = None
    groups: List[Group] = field(default_factory=list)

    def to_jsonable(self):
        """
        Return this event as a dict of values that can be serialized to JSON
        """
        return asdict(self)

    @classmethod
    def from_dict(cls, d: Dict[str, Any]):
        """
        """
        res = cls(**d)
        # res.groups is created as a list of dicts, but we want a list of Group
        res.groups = [Group(**g) for g in d["groups"]]
        return res


class AuthenticationError(Exception):
    """
    Exception rasied in case user authentication failed
    """
    pass


class DB:
    """
    Base class for implementing user databases
    """
    @classmethod
    async def is_viable(cls, config: Config) -> bool:
        """
        Return True if this user database can work with the given configuration
        """
        return True

    async def authenticate(self, uid: str, password: str) -> User:
        """
        Authenticate a user given their username and password.

        Returns a User object with all the user information.

        Raises AuthenticationError if the authentication failed.
        """
        raise NotImplementedError("DB.authenticate")
