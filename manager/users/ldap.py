import ldap3
import asyncio
from . import db
from manager.config import Config
import logging

log = logging.getLogger("ldap_user_db")


class LDAP(db.DB):
    """
    Lightweight asynchronous interface to LDAP
    """

    def __init__(self, config):
        self.uri = config.ldap_uri
        self.user_search_base = config.ldap_user_search_base
        self.group_search_base = config.ldap_group_search_base

    @classmethod
    def _sync_try_connect(cls, uri):
        try:
            with ldap3.Connection(uri, auto_bind=True):
                return True
        except ldap3.core.exceptions.LDAPException as e:
            log.info("failed to connect to LDAP server %s: %s", uri, e)
            return False

    @classmethod
    async def is_viable(cls, config: Config) -> bool:
        if not config.ldap_uri:
            log.info("ldap_uri not found in configuration")
            return False
        if not config.ldap_user_search_base:
            log.info("ldap_user_search_base not found in configuration")
            return False
        if not config.ldap_group_search_base:
            log.info("ldap_group_search_base not found in configuration")
            return False
        loop = asyncio.get_running_loop()
        return await loop.run_in_executor(
            None, cls._sync_try_connect, config.ldap_uri
        )

    def _sync_authenticate(self, bind_dn, password):
        try:
            with ldap3.Connection(
                self.uri, auto_bind=True, user=bind_dn, password=password
            ) as conn:
                conn.search(
                    bind_dn, "(objectclass=*)", attributes=ldap3.ALL_ATTRIBUTES
                )
                res = db.User(
                    name=conn.entries[0].uid.value,
                    id=conn.entries[0].uidNumber.value,
                    display_name=conn.entries[0].cn.value,
                )
                # res.entry = conn.entries[0]

                primary_gid = conn.entries[0].gidNumber.value
                conn.search(
                    self.group_search_base,
                    "(gidNumber={})".format(primary_gid),
                    attributes="cn",
                )
                for e in conn.entries:
                    res.groups.append(
                        db.Group(name=e.cn.value, id=primary_gid)
                    )

                conn.search(
                    self.group_search_base,
                    "(memberUid={})".format(res.name),
                    attributes=["cn", "gidNumber"],
                )
                for e in conn.entries:
                    res.groups.append(
                        db.Group(name=e.cn.value, id=e.gidNumber.value)
                    )

                return res
        except ldap3.core.exceptions.LDAPBindError as e:
            raise db.AuthenticationError(str(e))

    async def authenticate(self, user: str, password: str) -> db.User:
        loop = asyncio.get_running_loop()
        bind_dn = "uid={},{}".format(user, self.user_search_base)
        return await loop.run_in_executor(
            None, self._sync_authenticate, bind_dn, password
        )
