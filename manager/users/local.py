import pwd
import spwd
import grp
import asyncio
import crypt
import os
from hmac import compare_digest
from . import db
from manager.config import Config
import logging

log = logging.getLogger("unix_user_db")


class Local(db.DB):
    """
    Lightweight asynchronous interface to the local Unix user/group database
    """

    def __init__(self, config):
        pass

    @classmethod
    async def is_viable(cls, config: Config) -> bool:
        try:
            pw = pwd.getpwuid(os.getuid())
        except KeyError:
            log.info("cannot get user information for the current user")
            return False

        try:
            spwd.getspnam(pw.pw_name)
        except KeyError:
            log.info("current user does not exist in shadow database")
            return False
        except PermissionError:
            log.info("shadow password database is not accessible")
            return False

        return True

    def _sync_authenticate(self, user, password):
        try:
            userinfo = pwd.getpwnam(user)
        except KeyError:
            raise db.AuthenticationError("User does not exist")

        cryptedpasswd = userinfo[1]

        if cryptedpasswd in ('x', '*'):
            try:
                cryptedpasswd = spwd.getspnam(user)[1]
            except KeyError:
                raise db.AuthenticationError(
                    "User does not exist in shadow database"
                )
            except PermissionError:
                raise db.AuthenticationError(
                    "Shadow password database is not accessible"
                )

        if cryptedpasswd in ["NP", "!", "", None]:
            raise db.AuthenticationError("user has no password set")
        if cryptedpasswd in ["LK", "*"]:
            raise db.AuthenticationError("account is locked")
        if cryptedpasswd == "!!":
            raise db.AuthenticationError("password has expired")

        if not compare_digest(
            crypt.crypt(password, cryptedpasswd), cryptedpasswd
        ):
            raise db.AuthenticationError("invalid password")

        display_name = userinfo.pw_gecos.split(",")[0]
        groups = []
        try:
            ginfo = grp.getgrgid(userinfo.pw_gid)
        except KeyError:
            raise db.AuthenticationError(
                "primary group ID does not exist in group database"
            )

        groups.append(db.Group(name=ginfo.gr_name, id=userinfo.pw_gid))
        for ginfo in grp.getgrall():
            if user in ginfo.gr_mem:
                groups.append(db.Group(name=ginfo.gr_name, id=ginfo.gr_gid))
        return db.User(
            name=user,
            id=userinfo.pw_uid,
            display_name=display_name,
            groups=groups,
        )

    async def authenticate(self, user: str, password: str) -> db.User:
        loop = asyncio.get_running_loop()
        return await loop.run_in_executor(
            None, self._sync_authenticate, user, password
        )
