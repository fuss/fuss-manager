import asyncio
import logging

import ruamel.yaml

from . import db
from manager.config import Config

log = logging.getLogger("unix_user_db")


class Master(db.DB):
    """
    Lightweight asynchronous interface to a master password.
    """

    def __init__(self, config):
        self.yaml = ruamel.yaml.YAML()
        self.pass_file = config.master_pass_file

    @classmethod
    async def is_viable(cls, config: Config) -> bool:
        yaml = ruamel.yaml.YAML()
        pass_file = config.master_pass_file
        if not pass_file:
            log.info("Master password file not configured")
            return False
        try:
            with open(pass_file) as fp:
                data = yaml.load(fp)
        except PermissionError:
            log.info("Master password file is not accessible")
            return False
        except FileNotFoundError:
            log.info("Master password file does not exist")
            return False

        if not data.get('pass'):
            log.info("Master password is not set")
            return False

        return True

    def _sync_authenticate(self, user, password):
        if user != 'root':
            raise db.AuthenticationError(
                "User is not allowed to use the master password"
            )

        try:
            with open(self.pass_file) as fp:
                data = self.yaml.load(fp)
        except PermissionError:
            raise db.AuthenticationError(
                "Master password file is not accessible"
            )

        masterpwd = data.get('pass')
        if not masterpwd:
            raise db.AuthenticationError("No master password has been set")

        if not password == masterpwd:
            raise db.AuthenticationError("invalid password")

        # TODO: should this be some specific value?
        uid = sum(ord(x) for x in user)
        groups = [db.Group(name='root', id=uid)]
        display_name = 'root'
        return db.User(
            name=user, id=uid, display_name=display_name, groups=groups
        )

    async def authenticate(self, user: str, password: str) -> db.User:
        loop = asyncio.get_running_loop()
        return await loop.run_in_executor(
            None, self._sync_authenticate, user, password
        )
