from . import db
from manager.config import Config
import logging

log = logging.getLogger("mock_user_db")


def mock_id(name):
    return sum(ord(x) for x in name)


class Mock(db.DB):
    """
    Mock user and group database implementation
    """
    def __init__(self, config):
        pass

    @classmethod
    async def is_viable(cls, config: Config) -> bool:
        return config.debug is True

    async def authenticate(self, user: str, password: str) -> db.User:
        if password == user:
            groups = [
                db.Group(name=user, id=mock_id(user)),
            ]
            if user in ('root', 'admin'):
                groups.append(db.Group(name="admin", id=mock_id("admin")))
            return db.User(
                    name=user,
                    id=mock_id(user),
                    display_name=user.capitalize(),
                    groups=groups
            )
        else:
            raise db.AuthenticationError("invalid password")
