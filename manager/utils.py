import ipaddress
import re

"""
Common data validation functions
"""

re_validate_mac = re.compile(r'^(?:[0-9A-Fa-f]{2}:){5}[0-9A-Fa-f]{2}$')
re_validate_hostname = re.compile(
    # Each component cannot start or end with a dash
    r"^(?:(?:[a-z0-9]|[a-z0-9][a-z0-9-]*[a-z0-9])\.)*"
    # Hostname cannot end with a dot
    r"(?:[a-z0-9]|[a-z0-9][a-z0-9-]*[a-z0-9])$", flags=re.I
)


def check_mac(s):
    """
    Check that a string looks like a macaddress
    """
    return bool(re_validate_mac.match(s))


def check_ip(s):
    """
    Check that a string looks like an ipv4 address
    """
    try:
        ipaddress.IPv4Address(s)
    except ipaddress.AddressValueError:
        return False
    return True


def check_hostname(s):
    """
    Check that a string looks like an hostname.
    """
    if not s:
        return False
    if not re_validate_hostname.match(s):
        return False
    return not check_ip(s)
