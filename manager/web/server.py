import os
import json
import logging
import time

import tornado.web
import tornado.websocket
from tornado.web import url

from ..manager import Manager
from .static import StaticFileHandler
from .webapi import WebAPI, WebAPIError
from ..users.db import User
from . import views


class EventSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print("WebSocket opened")
        self.application.add_event_socket_handlers(self)

    def on_message(self, message):
        print("Message received", message)
        # self.write_message(u"You said: " + message)

    def on_close(self):
        print("WebSocket closed")
        self.application.remove_event_socket_handlers(self)


class RestGET(views.SessionRequestHandler):
    """
    WebAPI front-end for GET requests
    """
    def initialize(self, function, **kwargs):
        self.function = function
        self.kwargs = kwargs

    async def get(self, **kwargs):
        self.set_header("content-type", "application/json")
        self.kwargs.update(kwargs)
        for name, vals in self.request.query_arguments.items():
            self.kwargs[name] = vals[-1]
        try:
            self.write(
                await self.application.webapi(
                    self.current_user,
                    self.function,
                    **self.kwargs
                    )
                )
        except WebAPIError as e:
            self.set_status(e.code, str(e))
            self.write({
                "error": True,
                "code": e.code,
                "message": str(e),
                "time": time.time(),
            })


class RestPOST(views.SessionRequestHandler):
    """
    WebAPI front-end for POST requests
    """
    def initialize(self, function, **kwargs):
        self.function = function
        self.kwargs = kwargs

    async def post(self, **kwargs):
        self.set_header("content-type", "application/json")
        args = json.loads(self.request.body.decode("utf8"))
        args.update(kwargs)
        args.update(self.kwargs)
        try:
            self.write(
                await self.application.webapi(
                    self.current_user, self.function, **args)
                )
        except WebAPIError as e:
            self.set_status(e.code, str(e))
            self.write({
                "error": True,
                "code": e.code,
                "message": str(e)
            })


class Application(tornado.web.Application):
    """
    Main Tornado Application
    """
    def __init__(self, config, *args):
        urls = [
            views.Login.as_url(r"/login"),
            views.Home.as_url(r"/"),
            views.Playbooks.as_url(r"/playbooks/"),
            views.EventMonitor.as_url(r"/event-monitor/"),

            url(r"/websocket", EventSocketHandler, name="event_socket"),

            url(
                r"/api/1.0/ping",
                RestGET,
                kwargs={"function": "ping"},
                name="api1.0_ping"
                ),
            url(
                r"/api/1.0/async_ping",
                RestGET,
                kwargs={"function": "async_ping"},
                name="api1.0_async_ping"
                ),
            url(
                r"/api/1.0/whoami",
                RestGET,
                kwargs={"function": "whoami"},
                name="api1.0_whoami"
                ),
            url(
                r"/api/1.0/machines",
                RestGET,
                kwargs={"function": "list_machines"},
                name="api1.0_list_machines"
                ),
            url(
                r"/api/1.0/machine/(?P<mac>[A-Fa-f0-9:]+)",
                RestGET,
                kwargs={"function": "get_machine"},
                name="api1.0_get_machine"
                ),
            url(
                r"/api/1.0/operation",
                RestPOST,
                kwargs={"function": "perform_operation"},
                name="api1.0_operation"
                ),
            url(
                r"/api/1.0/playbooks",
                RestGET,
                kwargs={"function": "list_playbooks"},
                name="api1.0_list_playbooks"
                ),
            url(
                r"/api/1.0/playbook_results/(?P<id>[^/]+)",
                RestGET,
                kwargs={"function": "get_playbook_results"},
                name="api1.0_get_playbook_results"
                ),
        ]
        if views.AuthForward is not None:
            urls.append(views.AuthForward.as_url("/auth-forward"))

        settings = {}
        settings.setdefault(
            "template_path",
            os.path.join(os.path.dirname(__file__), "templates")
            )
        settings.setdefault("static_handler_class", StaticFileHandler)
        settings.setdefault(
            "static_path",
            os.path.join(os.path.dirname(__file__), "static")
            )
        settings.setdefault("autoreload", config.debug)
        settings.setdefault("xsrf_cookies", True)

        super().__init__(urls, **settings)
        self.log = logging.getLogger(self.__class__.__name__)
        self.config = config
        self.manager = Manager(config)
        self.event_socket_handlers = set()
        self.manager.event_hub.subscribe(self.on_event)
        self.webapi = WebAPI(self.manager)
        self.session_manager = None

    async def init(self):
        await self.manager.init()
        self.session_manager = await self.load_session_manager()

    def add_event_socket_handlers(self, handler):
        self.event_socket_handlers.add(handler)

    def remove_event_socket_handlers(self, handler):
        self.event_socket_handlers.discard(handler)

    async def on_event(self, evt):
        if not self.event_socket_handlers:
            return

        payload = evt.to_jsonable()
        payload["description"] = evt.description

        # Add 'machine' attribute if the event refers to a known machine
        mac = getattr(evt, "mac", None)
        if mac is not None:
            machine = self.manager.store.machines.get(mac)
            if machine is not None:
                payload["machine"] = machine.to_jsonable()

        payload = json.dumps(payload)

        for handler in self.event_socket_handlers:
            handler.write_message(payload)

    async def run(self):
        self.log.info(
            "Listening on %s:%d",
            self.config.web_address,
            self.config.web_port
            )
        self.listen(self.config.web_port, address=self.config.web_address)
        await self.manager.run()

    async def shutdown(self):
        await self.manager.shutdown()

    async def load_session_manager(self):
        """
        Try to load a user database
        """
        from .session import MemorySessionManager
        from .session import FilesystemSessionManager
        for session_manager in [
                FilesystemSessionManager,
                MemorySessionManager,
                ]:
            if not await session_manager.is_viable(self.manager.config):
                self.log.warn(
                    "%s: user database will not be used",
                    session_manager.__name__
                    )
                continue
            return session_manager(self.manager.config)
        raise RuntimeError("Cannot instantiate a valid session manager")

    def get_user_perms(self, user: User):
        """
        Get the array of permissions for the given user
        """
        permissions = self.config.permissions
        # Get permissions for user
        perms = set(permissions['users'].get(user.name, []))
        # Get permission that the user has thanks to its groups
        for group in user.groups:
            perms.update(permissions['groups'].get(group.name, []))
        # Return the set as a sorted list
        return sorted(perms)
