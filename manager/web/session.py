from typing import Dict, Any, Optional
import string
import random
import json
import os
import time
from manager.config import Config


class SessionKeyNotFoundError(KeyError):
    """
    Exception raised when using a session key not found in the session store
    """
    pass


class MalformedSessionKeyError(ValueError):
    """
    Exception raised when using a malformed session key
    """
    pass


class Session:
    """
    Session information stored in a manager.

    Session data elements can only contain types that can be serialized to
    JSON.
    """
    def __init__(self, manager: "SessionManager", key: str, data: Dict[Any, Any] = None):
        self.manager = manager
        self.key = key
        self.data = data if data is not None else {}
        self.dirty = False

    def get(self, key: Any, default=None) -> Any:
        return self.data.get(key, default)

    def set(self, key: Any, val: Any) -> None:
        if key.startswith("_"):
            raise ValueError("session data keys must not start with an undescore")
        self.data[key] = val
        self.dirty = True

    async def save(self) -> None:
        if self.dirty:
            await self.manager.save(self)
            self.dirty = False

    @property
    def time_created(self):
        """
        Get the unix timestamp of when this session was created
        """
        return self.data.get("_created")

    @property
    def time_saved(self):
        """
        Get the unix timestamp of when this session was last saved
        """
        return self.data.get("_saved")


class SessionManager:
    """
    Base class for implementing session managers
    """
    # Session key generation is taken from Django
    VALID_KEY_CHARS = string.ascii_lowercase + string.digits

    @classmethod
    async def is_viable(cls, config: Config) -> bool:
        """
        Return True if this user database can work with the given configuration
        """
        return True

    async def load_or_create(self, key: Optional[str]) -> Session:
        """
        Load a session given a session key, creating it if the session key is
        None or does not exist.

        :returns: the Session object
        """
        if key is None:
            return await self.create()
        try:
            return await self.load(key)
        except (MalformedSessionKeyError, SessionKeyNotFoundError):
            return await self.create()

    async def load(self, key: str) -> Session:
        """
        Load a session given a session key.

        :returns: the Session object
        """
        raise SessionKeyNotFoundError("session key not found")

    async def create(self) -> Session:
        """
        Create a new session.

        :returns: the Session object
        """
        key = await self._get_new_session_key()
        session = Session(self, key, {"_created": time.time()})
        await self.save(session)
        return session

    async def exists(self, key: str) -> bool:
        """
        Check if a session key already exists
        """
        raise NotImplementedError("SessionManager.exists")

    async def save(self, session: Session):
        """
        Persist the changes in the given session
        """
        raise NotImplementedError("SessionManager.save")

    @classmethod
    def _get_random_string(cls, length: int, allowed_chars: str) -> str:
        """
        Returns a securely generated random string.
        """
        # Taken from django.utils.crypto.get_random_string
        return ''.join(random.choice(allowed_chars) for i in range(length))

    def _validate_session_key(self, key):
        """
        Key must be truthy and at least 8 characters long. 8 characters is an
        arbitrary lower bound for some minimal key security.
        """
        # Taken from django.contrib.sessions.base.SessionBase
        return key and len(key) >= 8

    async def _get_new_session_key(self) -> str:
        """
        Generate a new session key that is not in use
        """
        while True:
            session_key = self._get_random_string(32, self.VALID_KEY_CHARS)
            if not await self.exists(session_key):
                return session_key

    @classmethod
    def _serialize(self, data: Dict[Any, Any]) -> str:
        return json.dumps(data)

    @classmethod
    def _deserialize(self, data: str) -> Dict[Any, Any]:
        return json.loads(data)


class MemorySessionManager(SessionManager):
    """
    In-memory session manager. Sessions are destroyed when the process is
    restarted. Use only for testing.
    """
    def __init__(self, config: Config):
        self.store = {}

    async def load(self, key: str) -> Session:
        if not self._validate_session_key(key):
            raise MalformedSessionKeyError("Invalid session key")
        if key not in self.store:
            raise SessionKeyNotFoundError(key)
        return Session(self, key, self._deserialize(self.store[key]))

    async def exists(self, key: str) -> bool:
        if not self._validate_session_key(key):
            return False
        return key in self.store

    async def save(self, session):
        session.data["_saved"] = time.time()
        self.store[session.key] = self._serialize(session.data)


class FilesystemSessionManager(SessionManager):
    """
    Session Manager using a directory in the file system.
    """
    def __init__(self, config: Config):
        self.root = config.web_session_dir

    @classmethod
    async def is_viable(cls, config: Config) -> bool:
        return config.web_session_dir is not None

    def _pathname(self, key: str) -> str:
        return os.path.join(self.root, key[:2], key)

    async def load(self, key: str) -> Session:
        if not self._validate_session_key(key):
            raise MalformedSessionKeyError("Invalid session key")
        try:
            with open(self._pathname(key), "rt") as fd:
                data = fd.read()
        except FileNotFoundError:
            raise SessionKeyNotFoundError(key)
        return Session(self, key, self._deserialize(data))

    async def exists(self, key: str) -> bool:
        if not self._validate_session_key(key):
            return False
        return os.path.exists(self._pathname(key))

    async def save(self, session):
        orig_umask = os.umask(0o007)
        try:
            dir_pathname = os.path.join(self.root, session.key[:2])
            os.makedirs(dir_pathname, exist_ok=True)
            file_pathname = os.path.join(dir_pathname, session.key)
            with open(file_pathname, "wt") as fd:
                session.data["_saved"] = time.time()
                fd.write(self._serialize(session.data))
        finally:
            os.umask(orig_umask)
