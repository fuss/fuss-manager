(function($) {
"use strict";

// Javascript mapping of manager.stores.Machine
class Machine
{
    constructor(info)
    {
        this.mac = info.mac;
        this.ip = info.ip;
        this.name = info.name;
        this.registered = info.registered;
        this.first_seen = info.first_seen;
        this.last_seen = info.last_seen;
        this.groups = info.groups;
        this.facts = info.facts;
        this.facts_log = info.facts_log;
    }
}


class API
{
    constructor() {
        this.xsrf_token = window.fussmanager.config.xsrf_token;
    }

    async _fetch_json(name, args, url, init)
    {
        let response;
        try {
            response = await fetch(url, init);
        } catch (error) {
            console.log("API GET ERROR", {
                name: name, 
                args: args,
                url: url,
                init: init,
                msg: error.message,
                response: response,
            });
            throw error;
        }

        const content_type = response.headers.get("Content-Type");
        if (!content_type || !content_type.includes("application/json"))
        {
            const text = await response.text();
            const errmsg = `content-type is '${content_type}' instead of application/json.`;
            console.log("API GET ERROR", {
                name: name, 
                args: args,
                url: url,
                init: init,
                msg: errmsg,
                response: response,
                body: text,
            });
            throw new Error(errmsg);
        }

        const json = await response.json();

        if (!response.ok)
        {
            const errmsg = `response result: ${response.status} ${response.statusText}`;
            console.log("API GET ERROR", {
                name: name, 
                args: args,
                url: url,
                init: init,
                msg: errmsg,
                response: response,
                body: json,
            });
            throw new Error(errmsg);
        }

        return json;
    };

    async _get(name, args) {
        let url = new URL("/api/1.0/" + name, window.location.href);
        for (const name of Object.keys(args))
            url.searchParams.append(name, args[name])

        return await this._fetch_json(name, args, url, {
            method: "GET",
            mode: "same-origin",
            credentials: "same-origin",
        });
    }

    async _post(name, args) {
        let url = new URL("/api/1.0/" + name, window.location.href);
        const data = JSON.stringify(args);
        console.log("_POST", data);
        return await this._fetch_json(name, args, url, {
            method: "POST",
            mode: "same-origin",
            credentials: "same-origin",
            headers: new Headers({
                "Content-Type": "application/json",
                "Content-Length": data.length,
		"X-XSRFToken": this.xsrf_token,
            }),
            body: data,
        });
    }

    async ping() {
        return await this._get("ping", {});
    }

    async async_ping() {
        return await this._get("async_ping", {});
    }

    async list_machines() {
        return await this._get("machines", {})
    }

    async get_machine(mac) {
        return await this._get("machine/" + mac, {});
        // let response = await this._get("machine/" + mac, {});
        // return new Machine(response.machine);
    }

    async operation(op) {
        return await this._post("operation", {
            op: op.jsonable,
        });
    }

    async list_playbooks() {
        return await this._get("playbooks", {})
    }

    async get_playbook_results(id) {
        return await this._get("playbook_results/" + id, {})
    }
}

/**
 * Receives events from the fuss-manager websocket interface
 */
class Events
{
    constructor() {
        this.socket = new WebSocket(window.fussmanager.config.url_events_ws);
        this.socket.onopen = () => { this.on_open() };
        this.socket.onmessage = (evt) => { this.on_message(evt) };
    }

    on_open() {
        console.log("Websocket channel open");
    }

    on_message(evt) {
        let new_evt = new CustomEvent("fussmanager.event", {
            detail: JSON.parse(evt.data),
        });
        document.dispatchEvent(new_evt);
    }

    on_close() {
        console.log("Websocket channel closed");
        // TODO: reconnect?
    }
}


/**
 * Client-side implementation of the fuss-manager REST interface
 */
class Manager
{
    constructor(view_name) {
        this.view_name = view_name;
        this.events = new Events();
        this.api = new API();
        // Machines listed by group
        this.machine_groups = {};
        // All the machines that do not have a group
        this.machines_without_group = [];
        // All the machines indexed by mac address
        this.machines_by_mac = {};

        // Update Machine information when new data arrives for a Machine
        document.addEventListener("fussmanager.event", evt => {
            const machine_data = evt.detail.machine;
            if (machine_data)
            {
                const machine = new Machine(machine_data);
                this.refresh_machine(machine);

                // Fire "fussmanager.machine_updated" event
                let new_evt = new CustomEvent("fussmanager.machine_updated", {
                    detail: machine,
                });
                document.dispatchEvent(new_evt);
            }
        });
    }

    // Refresh machine information given a new Machine object
    refresh_machine(machine)
    {
        const old = this.machines_by_mac[machine.mac];

        // Update groups

        // Remove machine from old groups
        if (old)
        {
            if (old.groups.length)
            {
                for (const group of old.groups)
                {
                    let elements = this.machine_groups[group];
                    const idx = elements.indexOf(old);
                    if (idx != -1)
                        elements.splice(idx, 1);
                    if (elements.length == 0)
                        delete this.machine_groups[group];
                }
            } else {
                const idx = this.machines_without_group.indexOf(old);
                if (idx != -1)
                    this.machines_without_group.splice(idx, 1);
            }
        }

        // Add machine to new groups
        if (machine.groups.length)
        {
            for (const group of machine.groups)
            {
                let groups = this.machine_groups[group];
                if (groups === undefined)
                    this.machine_groups[group] = [machine];
                else
                    groups.push(machine);
            }
        } else {
            this.machines_without_group.push(machine);
        }

        // Replace by mac
        this.machines_by_mac[machine.mac] = machine;
    }

    async reload_machines()
    {
        let response = await this.api.list_machines();
        let groups = {};
        let machines_without_group = [];
        let machines_by_mac = {};
        for (let info of response.machines)
        {
            let machine = new Machine(info)
            machines_by_mac[machine.mac] = machine;
            if (machine.groups.length)
            {
                for (let group of machine.groups)
                {
                    let lst = groups[group];
                    if (lst === undefined)
                        groups[group] = [machine];
                    else
                        lst.push(machine);
                }
            } else {
                machines_without_group.push(machine);
            }
        }

        this.machine_groups = groups;
        this.machines_without_group = machines_without_group;
        this.machines_by_mac = machines_by_mac;

        let new_evt = new CustomEvent("fussmanager.machine_list_updated", {
        });
        document.dispatchEvent(new_evt);
    }

    async init()
    {
        await this.reload_machines();
    }

    get_machine_by_mac(mac)
    {
        return this.machines_by_mac[mac];
    }

    async post_operation(op)
    {
        console.debug("Post operation", op);
        await this.api.operation(op);
    }
}

window.fussmanager = $.extend(window.fussmanager || {}, {
    Machine: Machine,
    Events: Events,
    API: API,
    Manager: Manager,
});

})(jQuery);
