(function($) {
"use strict";


class Operation
{
    constructor()
    {
        this.source = null;
    }

    get jsonable()
    {
        return {
            name: this.constructor.name,
            source: this.source,
        };
    }
}


class AddGroup extends Operation
{
    constructor(mac, group)
    {
        super()
        this.mac = mac;
        this.group = group;
    }

    get jsonable()
    {
        let res = super.jsonable;
        res.mac = this.mac;
        res.group = this.group;
        return res;
    }
}


class DelGroup extends Operation
{
    constructor(mac, group)
    {
        super()
        this.mac = mac;
        this.group = group;
    }

    get jsonable()
    {
        let res = super.jsonable;
        res.mac = this.mac;
        res.group = this.group;
        return res;
    }
}


class RunPlaybook extends Operation
{
    constructor(hosts, playbook)
    {
        super()
        this.hosts = hosts;
        this.playbook = playbook;
    }

    get jsonable()
    {
        let res = super.jsonable;
        res.hosts = this.hosts;
        res.playbook = this.playbook;
        return res;
    }
}


class RefreshFacts extends Operation
{
    constructor(hosts)
    {
        super();
        this.hosts = hosts;
    }

    get jsonable()
    {
        let res = super.jsonable;
        res.hosts = this.hosts;
        return res;
    }
}


class WakeOnLan extends Operation
{
    constructor(macs)
    {
        super();
        this.macs  = macs;
        console.log("MACS:", macs);
    }

    get jsonable()
    {
        let res = super.jsonable;
        res.macs = this.macs;
        return res;
    }
}


window.fussmanager = window.fussmanager || {};
window.fussmanager.ops = $.extend(window.fussmanager.ops || {}, {
    Operation: Operation,
    AddGroup: AddGroup,
    DelGroup: DelGroup,
    RunPlaybook: RunPlaybook,
    RefreshFacts: RefreshFacts,
    WakeOnLan: WakeOnLan,
});

})(jQuery);
