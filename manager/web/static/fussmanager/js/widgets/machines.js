(function($) {
"use strict";

let Widget = window.fussmanager.widgets.Widget;

class GroupList extends Widget
{
    constructor(element, manager)
    {
        super(element);
        this.manager = manager;
        this.current_group = null;

        // Keep the machine list up to date with reloads
        document.addEventListener("fussmanager.machine_list_updated", evt => {
            this.render();
        });

        // Keep the machine list up to date with updates
        document.addEventListener("fussmanager.machine_updated", evt => {
            this.render();
        });

        this.on("click", evt => {
            if (evt.target.tagName != "A")
                return;
            const group = evt.target.dataset.group;
            if (group == this.current_group)
                this.current_group = null;
            else
                this.current_group = group;
            this.render();
            this.trigger("fussmanager.group_selected", this.current_group);
        });
    }

    render()
    {
        let group_names = Object.keys(this.manager.machine_groups);
        group_names.sort();
        let groups = [];
        for (let name of group_names)
        {
            groups.push({
                name: name,
                active: name == this.current_group,
            });
        }
        this.element.innerHTML = this.render_template("list-items", {groups: groups});
    }
}


class GroupDetail extends Widget
{
    constructor(element, manager)
    {
        super(element);
        this.table = this.element.getElementsByTagName("TABLE")[0];
        this.manager = manager;
        this.group_name = null;

        // Keep the machine list up to date with reloads
        document.addEventListener("fussmanager.machine_list_updated", evt => {
            this.render(this.group_name);
        });
        document.addEventListener("fussmanager.machine_updated", evt => {
            this.render(this.group_name);
        });

        this.element.addEventListener("click", evt => {
            const dataset = evt.target.dataset;
            if (!dataset || !dataset.action)
                return;
            // If there is a data-action attribute, we perform the action
            evt.preventDefault();

            if (dataset.action == "run-playbook")
            {
                let hosts = this.group_name;
                if (!hosts)
                {
                    let names = [];
                    for (let m of this.get_machine_list())
                        names.push(m.name)
                    hosts = names.join(",");
                }

                if (!hosts)
                    return;

                // Let this event bubble, so one can catch all playbook run
                // requests also at the document level
                this.trigger("playbook-run-requested", {
                    hosts: hosts,
                }, true);
            }
            // refs #748
            else if (dataset.action == "wake-on-lan") {
                let macs = [];
                for (let m of this.get_machine_list())
                  macs.push(m.mac);
                let op = new fussmanager.ops.WakeOnLan(macs);
                op.source = `html5.${this.manager.view_name}.${this.element.id}.wake-on-lan`;
                this.manager.post_operation(op);
            }
        });

        // refs #825
        var datetime_format = "YYYY-MM-DD HH:mm:ss";
        // Set up the table
        this.datatable = $(this.table).DataTable({
            paging: false,
            dom:
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>",
            infoCallback: (settings, start, end, max, total, pre) => {
                return this.render_template("group-info", {
                    group: this.group_name,
                    max: max,
                    start: start,
                    end: end,
                    total: total,
                    filtered: max != total,
                });
            },
            columns: [
                {
                    data: null,
                    render: (data, type, row, meta) => {
                        return data.name || "--";
                    },
                },
                { data: "ip", },
                { data: "mac", },
                {
                    data: "first_seen",
                    searchable: false,
                    render: (data, type, row, meta) => {
                        if (type == "sort")
                            return data;
                        // refs #825
                        var time = document.createElement("time");
                        time.setAttribute("datetime", moment(new Date(data * 1000)).format());
                        time.setAttribute("title", moment(new Date(data * 1000)).format(datetime_format));
                        time.textContent = moment(new Date(data * 1000)).fromNow();
                        return time.outerHTML;
                    },
                },
                {
                    data: "last_seen",
                    searchable: false,
                    render: (data, type, row, meta) => {
                        if (type == "sort")
                            return data;
                        // refs #825
                        var time = document.createElement("time");
                        time.setAttribute("datetime", moment(new Date(data * 1000)).format());
                        time.setAttribute("title", moment(new Date(data * 1000)).format(datetime_format));
                        time.textContent = moment(new Date(data * 1000)).fromNow();
                        return time.outerHTML;
                    },
                },
                {
                    data: "registered",
                    searchable: false,
                    render: (data, type, row, meta) => {
                        if (type == "sort")
                            return data ? 1 : 0;
                        return data ? "yes" : "no";
                    },
                },
            ]
        });

        // On click, post fussmanager.machine_selected with the Machine object
        // as detail
        this.datatable.on("click", "tr", evt => {
            const machine = this.datatable.row(evt.currentTarget).data();
            if (!machine)
                return;
            this.trigger("fussmanager.machine_selected", machine);
        });
    }

    get_machine_list()
    {
        let machine_list = undefined;
        if (this.group_name !== null)
            machine_list = this.manager.machine_groups[this.group_name];
        if (machine_list === undefined)
            machine_list = this.manager.machines_without_group;
        return machine_list;
    }

    render(group_name)
    {
        this.group_name = group_name;
        let machine_list = this.get_machine_list();
        if (machine_list === undefined)
            this.group_name = null;

        let rows = []
        for (let machine of machine_list)
            rows.push(machine);
        this.datatable.clear();
        this.datatable.rows.add(rows);
        this.datatable.draw();
    }

//   $("#update_machine_list").click(evt => {
//     evt.preventDefault();
//     fuss_manager.update_machine_list().then();
//   });
}


class MachineDetail extends Widget
{
    constructor(element, manager)
    {
        super(element + "_modal");
        this.manager = manager;
        this.machine = null;

        $(this.element).modal({
            focus: false,
            show: false,
        });

        this.el_title = this.get_element(element + "-title");
        this.el_body = this.get_element(element + "-body");
        this.el_tab_general = this.get_element(element + "-tab-general");
        this.el_tab_facts = this.get_element(element + "-tab-facts");
        this.el_facts = this.get_element(element + "-facts");
        this.el_factlog = this.get_element(element + "-factlog");

        // Keep the machine list up to date with reloads
        document.addEventListener("fussmanager.machine_list_updated", evt => {
            this.refresh();
        });

        // Keep the machine list up to date with updates
        document.addEventListener("fussmanager.machine_updated", evt => {
            this.refresh();
        });

        this.el_body.addEventListener("click", evt => {
            const target = $(evt.target).closest("[data-action]");
            if (!target.length)
                return;
            const dataset = target[0].dataset;
            if (!dataset || !dataset.action)
                return;
            // If there is a data-action attribute, we perform the action
            evt.preventDefault();

            if (dataset.action == "machine-group-add-new")
            {
                const name = window.prompt("New group name:", "");
                if (!name)
                    return;
                let op = new fussmanager.ops.AddGroup(this.machine.mac, name);
                op.source = `html5.${this.manager.view_name}.${this.element.id}.add-new`;
                this.manager.post_operation(op);
            } else if (dataset.action == "machine-group-remove") {
                const group = dataset.group;
                let op = new fussmanager.ops.DelGroup(this.machine.mac, group);
                op.source = `html5.${this.manager.view_name}.${this.element.id}.remove`;
                this.manager.post_operation(op);
            } else if (dataset.action == "machine-playbook-run") {
                // Let this event bubble, so one can catch all playbook run
                // requests also at the document level
                $(this.element).modal("hide");
                this.trigger("playbook-run-requested", {
                    hosts: this.machine.name,
                }, true);
            } else if (dataset.action == "machine-facts-refresh") {
                let op = new fussmanager.ops.RefreshFacts(this.machine.name);
                op.source = `html5.${this.manager.view_name}.${this.element.id}.refresh-facts`;
                this.manager.post_operation(op);
            // refs #748
            } else if (dataset.action == "machine-wake-on-lan") {
                console.log("WAKE ON LAN");
                let op = new fussmanager.ops.WakeOnLan([this.machine.mac]);
                op.source = `html5.${this.manager.view_name}.${this.element.id}.wake-on-lan`;
                this.manager.post_operation(op);
            } else {
                console.error("Unsupported action", dataset.action, "for", evt)
            }
        });
    }

    show(machine) {
        this.render(machine);
        $(this.element).modal("show");
    }

    // Update the current machine data, and rerender
    refresh()
    {
        if (this.machine === null)
            return;
        const machine = this.manager.machines_by_mac[this.machine.mac];
        if (machine === undefined)
            return;
        this.render(machine);
    }

    // Render the given machine in the widget, without popping up the dialog
    render(machine)
    {
        this.machine = machine;
        this.el_title.textContent = machine.name;

        this.el_tab_general.innerHTML = this.render_template("machine-info", {
            machine: this.machine,
            first_seen: new Date(machine.first_seen * 1000).toLocaleString(),
            last_seen: new Date(machine.last_seen * 1000).toLocaleString(),
        });

        this.el_facts.innerHTML = this.render_template("machine-facts", {
            machine: this.machine,
            facts_json: machine.facts ? JSON.stringify(machine.facts.ansible_facts, null, 2) : null,
        });

        let facts_log = [];
        for (let l of this.machine.facts_log)
        {
            const ts = new Date(l.timestamp * 1000);
            facts_log.push({
                ts: ts,
                ts_formatted: ts.toLocaleString(),
                result: l.result == "SUCCESS" ? "ok" : l.result,
                details: l.details,
                details_json: JSON.stringify(l.details, null, 2),
            });
        }
        facts_log.sort((a, b) => { return b.ts - a.ts });

        this.el_factlog.innerHTML = this.render_template("machine-factlog", {
            machine: this.machine,
            facts_log: facts_log,
        });
    }
}


class PlaybookRunner extends Widget
{
    constructor(element, manager)
    {
        super(element + "_modal");
        this.manager = manager;
        this.hosts = null;

        $(this.element).modal({
            focus: false,
            show: false,
        });

        this.el_title = this.get_element(element + "_title");
        this.el_body = this.get_element(element + "_body");
        this.el_hosts = this.get_element(element + "_hosts");
        this.el_playbook = this.get_element(element + "_playbook");
        this.el_run = this.get_element(element + "_run");

        this.el_run.addEventListener("click", evt => {
            const playbook = $(this.el_playbook).val();
            let op = new fussmanager.ops.RunPlaybook(this.hosts, playbook);
            op.source = `html5.${this.manager.view_name}.${this.element.id}.run`;
            // TODO: this will keep going until the playbook has finished running.
            // instead of calling .then(), we can show a list of the playbooks
            // being run.
            this.manager.post_operation(op).then();
            $(this.element).modal("hide");
        });

        this.el_playbook.addEventListener("change", evt => {
            this.refresh_run_button();
        });
    }

    refresh_run_button()
    {
        const val = $(this.el_playbook).val();
        if (val)
        {
            this.el_run.removeAttribute("disabled");
        } else {
            this.el_run.setAttribute("disabled", "");
        }
    }

    show(hosts) {
        $(this.element).modal("show");
        this.render(hosts);
    }

    // Render the given machine in the widget, without popping up the dialog
    async render(hosts)
    {
        this.hosts = hosts;

        this.el_hosts.textContent = this.hosts;

        this.el_run.setAttribute("disabled", "");
        this.el_playbook.setAttribute("disabled", "");

        let select = $(this.el_playbook);
        select.find("option[value!='']").remove();
        // TODO: show a loading indicator?

        let playbooks = await this.manager.api.list_playbooks();
        for (const pb of playbooks.playbooks)
        {
            select.append($("<option>").attr("value", pb.name).text(pb.title));
        }

        this.el_playbook.removeAttribute("disabled");
        this.refresh_run_button();
    }
}


window.fussmanager = window.fussmanager || {};
window.fussmanager.widgets = $.extend(window.fussmanager.widgets || {}, {
    Widget: Widget,
    GroupList: GroupList,
    GroupDetail: GroupDetail,
    MachineDetail: MachineDetail,
    PlaybookRunner: PlaybookRunner,
});

})(jQuery);
