(function($) {
"use strict";

const mustache_delimiters = ["[[", "]]"];

class Widget
{
    constructor(element)
    {
        this.element_name = element;
        this.element = document.getElementById(element);
        this.templates = {};

        let found = [];
        for (let el of this.element.getElementsByTagName("script"))
        {
            console.debug("Found template", el);
            if (el.getAttribute("type") != "x-tmpl-mustache")
                continue;
            console.debug("Found template", el.dataset.name, el);
            const body = el.innerHTML;
            this.templates[el.dataset.name] = body;
            Mustache.parse(body, mustache_delimiters);
            found.push(el);
        }
        for (let el of found)
            el.remove();
    }

    get_element(id)
    {
        let res = document.getElementById(id);
        if (!res)
            console.error("%s: element %s not found", this.element_name, id);
        return res
    }

    render_template(template_name, args)
    {
        const body = this.templates[template_name];
        if (body === undefined)
        {
            console.error("Template", template_name, "not found in", this.element);
            return `[MISSING TEMPLATE ${template_name}]`;
        }
        return Mustache.render(body, args, {}, mustache_delimiters);
    }

    // Shortcut for calling addEventListener on the widget main element
    on(event_name, cb)
    {
        this.element.addEventListener(event_name, cb);
    }

    // Shortcut for dispatching a CustomEvent on the widget main element
    trigger(event_name, detail, bubbles=false)
    {
        let new_evt = new CustomEvent(event_name, {detail: detail, bubbles: bubbles});
        this.element.dispatchEvent(new_evt);
    }
}


window.fussmanager = window.fussmanager || {};
window.fussmanager.widgets = $.extend(window.fussmanager.widgets || {}, {
    Widget: Widget,
});

})(jQuery);
