import datetime
import logging
import tornado.web
from tornado.escape import xhtml_escape
from ..compat import URLSpec
from manager.users.db import User, Group
from manager import signing


class SessionRequestHandler(tornado.web.RequestHandler):
    async def prepare(self):
        # Load or create session based on 'sid' cookie value
        sid = self.get_cookie("sid", None)
        self.session = await self.application.session_manager.load_or_create(sid)

        # Save or refresh session cookie
        self.set_cookie("sid", self.session.key,
                        max_age=6 * 3600)

        # Load user information if the user is logged in
        user = self.session.get("user", None)
        if user is None:
            self.current_user = None
        else:
            self.current_user = User.from_dict(user)

        perms = self.session.get("perms", [])
        self.perms = perms


class TemplateView(SessionRequestHandler):
    # URLSpec name
    name = None
    # HTML template file name
    template_name = None

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.log = logging.getLogger("views." + self.name)

    @classmethod
    def as_url(cls, pattern):
        return URLSpec(pattern, cls, name=cls.name)

    def make_nav_link(self, name, title):
        active = name == self.name
        return (
            '<li id="head-nav-{name}" class="nav-item{active}">'
            '<a class="nav-link" href="{href}">'
            '{title}{current}'
            '<span id="head-nav-notify-{name}" class="d-none float-right text-warning">'
            ' <span class="fa fa-bell"></span>'
            '</span>'
            '</a>'
            '</li>'
        ).format(
            name=name,
            active=" active" if active else "",
            current=" <span class='sr-only'>(current)</span>" if active else "",
            href=self.reverse_url(name),
            title=xhtml_escape(title),
        )

    def get_context_data(self):
        if self.request.protocol == "http":
            ws_url = "ws://" + self.request.host + \
                    self.application.reverse_url("event_socket")
        else:
            ws_url = "wss://" + self.request.host + \
                    self.application.reverse_url("event_socket")
        return {
            "view_name": self.name,
            "ws_url": ws_url,
            "nav_link": self.make_nav_link,
        }

    def get(self):
        # Do not use the standard render, as we do not need the UIModule
        # machinery
        html = self.render_string(
            self.template_name,
            **self.get_context_data()
            )
        self.finish(html)


# refs #862
class Login(TemplateView):
    name = "login"
    template_name = "login.html"

    def get_context_data(self):
        _ = self.locale.translate
        ctx = super().get_context_data()
        ctx["title"] = _("Login")
        return ctx

    async def post(self):
        from manager.users.db import AuthenticationError
        username = self.get_body_argument("username")
        password = self.get_body_argument("password")
        users = self.application.manager.users

        try:
            user = await users.authenticate(username, password)
        except AuthenticationError as e:
            self.finish(str(e))
        else:
            perms = self.application.get_user_perms(user)
            jsonable_user = user.to_jsonable()
            self.session.set("user", jsonable_user)
            # I didn't use .to_jsonable() because perms is a sorted list of strings
            self.session.set("perms", perms)
            await self.session.save()
            # Redirect to home after successful login
            self.redirect("/")


if signing.HAVE_SIGNING:
    class AuthForward(TemplateView):
        name = "auth_forward"

        async def get(self):
            secret = self.application.config.auth_forward_secret
            try:
                token = self.get_query_argument("t")
            except tornado.web.MissingArgumentError:
                self.log.error("'t' query argument not found")
                self.send_error(403)
                return

            # Decode the signed auth forward token
            try:
                data = signing.loads(token, key=secret, salt="fuss_manager.auth", max_age=60)
            except signing.BadSignature as e:
                self.log.error("auth token does not decoded: %s", e)
                self.send_error(403)
                return

            # Get the redirect url
            try:
                url = data["url"]
            except KeyError:
                self.log.error("url not found in decoded structure %r", data)
                self.send_error(403)
                return

            # Decode the user structure
            try:
                self.current_user = User.from_dict(data["user"])
            except KeyError as e:
                self.log.error("element not found in decoded structure %r (%s)", data, e)
                self.send_error(403)
                return

            # Save the user in the session
            jsonable_user = self.current_user.to_jsonable()
            self.session.set("user", jsonable_user)
            # Get the user permissions and save them in the session
            perms = self.application.get_user_perms(self.current_user)
            self.session.set("perms", perms)
            await self.session.save()
            # Redirect to home after successful login
            self.redirect(url)
else:
    AuthForward = None


class Home(TemplateView):
    name = "home"
    template_name = "machines.html"

    def get_context_data(self):
        _ = self.locale.translate
        ctx = super().get_context_data()
        ctx["title"] = _("Fuss Manager")
        return ctx


class EventMonitor(TemplateView):
    name = "event_monitor"
    template_name = "event_monitor.html"

    def get_context_data(self):
        _ = self.locale.translate
        ctx = super().get_context_data()
        ctx["title"] = _("Event Monitor")
        return ctx


class Playbooks(TemplateView):
    name = "playbooks"
    template_name = "playbooks.html"

    def get_context_data(self):
        _ = self.locale.translate
        playbook_log = self.application.manager.ansible.playbook_log

        ctx = super().get_context_data()
        ctx["title"] = _("Playboog log")
        qmonth = self.get_query_argument("month", None)
        today = datetime.date.today().replace(day=1)
        earliest = playbook_log.get_first_month()
        if qmonth is None:
            month = today
        else:
            month = datetime.datetime.strptime(qmonth, "%Y-%m").date()

        ctx["month"] = month

        if earliest and month > earliest:
            ctx["month_prev"] = (month - datetime.timedelta(days=15)).replace(day=1)
        else:
            ctx["month_prev"] = None

        if month < today:
            ctx["month_next"] = (month + datetime.timedelta(days=40)).replace(day=1)
        else:
            ctx["month_next"] = None

        ctx["log"] = list(playbook_log.list_month(month.year, month.month))
        return ctx
