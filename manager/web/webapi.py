import time
import logging
import asyncio
from functools import wraps

from manager import ops, perms

log = logging.getLogger(__name__)


class WebAPIError(Exception):
    def __init__(self, code, msg):
        super().__init__(msg)
        self.code = code


class WebAPIPermissionDenied(WebAPIError):
    """
    """
    def __init__(self):
        super().__init__(403, 'Permission Denied')


def permission_required(perm):
    """
    Decorator to be used on WebAPI methods to check for permissions.
    """
    def decorator(func):
        @wraps(func)
        async def wrapper(webapi, user, *args, **kw):
            manager = webapi.manager
            if perms.has_permissions(user, perm, manager.config.permissions):
                if asyncio.iscoroutinefunction(func):
                    return await func(webapi, user, *args, **kw)
                else:
                    return func(webapi, user, *args, **kw)
            else:
                raise WebAPIPermissionDenied()
        return wrapper
    return decorator


class WebAPI:
    """
    Backend-independent functions exposed via REST or WebSocket APIs
    """
    def __init__(self, manager):
        self.manager = manager

    async def __call__(self, user, function=None, **kw):
        """
        Call a web API function by name and keyword arguments
        """
        log.debug("API call %s %r", function, kw)
        if function is None:
            log.debug("API call %s %s: function is missing", function, kw)
            return None
        f = getattr(self, "do_" + function)
        if f is None:
            log.debug("API call %s %s: function not found", function, kw)
            return None
        if asyncio.iscoroutinefunction(f):
            res = await f(user=user, **kw)
        else:
            res = f(user, **kw)
        log.debug("API call %s %r result %r", function, kw, res)
        res["time"] = time.time()
        return res

    @permission_required(perms.ReadonlyBaseAccess)
    def do_ping(self, user, **kw):
        return {
            "pong": True,
        }

    @permission_required(perms.ReadonlyBaseAccess)
    async def do_async_ping(self, user, **kw):
        return {
            "pong": True,
        }

    def do_whoami(self, user, **kw):
        return {
            "user": user.to_jsonable() if user is not None else None,
        }

    @permission_required(perms.ReadonlyBaseAccess)
    def do_list_machines(self, user):
        return {
            "machines": [
                m.to_jsonable()
                for m in self.manager.store.machines.values()
                ],
        }

    @permission_required(perms.ReadonlyBaseAccess)
    def do_get_machine(self, user, mac):
        res = self.manager.store.machines.get(mac)
        if res is None:
            raise WebAPIError(404, "machine not found")
        return {
            "machine": res.to_jsonable(),
        }

    async def do_perform_operation(self, op, user=None):
        op = ops.Operation.from_json(op, user=user)
        await self.manager.execute(op)
        return {}

    @permission_required(perms.RunPlaybooks)
    def do_list_playbooks(self, user):
        return {
            "playbooks": list(self.manager.ansible.list_playbooks()),
        }

    @permission_required(perms.ShowPlaybookLogs)
    def do_get_playbook_results(self, user, id):
        res = self.manager.ansible.playbook_log.load(id)
        if res is None:
            raise WebAPIError(404, "machine not found")
        return {
            "playbook": res,
        }
