Power Up Role
=============

This role can be added as the first role of a playbook to make sure that
the target machines are turned on (via Wake on Lan) before the playbook
is run.

Note that ansible doesn't know whether the machines were already on or
not, so it is not possible to turn them back off at the end.

To use this role add ``gather_facts: no`` in the playbook, and this role as the first one in the list, e.g.::

    - name: Ping all machines
      hosts: all
      gather_facts: no
      roles:
       - power_up
      tasks:
       - name: PING
	 ping:
