#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(
    name='fuss-manager',
    version='0.7.7',
    description='',
    author='Provincia Autonoma di Bolzano',
    author_email='',
    url='https://work.fuss.bz.it/projects/fuss-manager',
    packages=find_packages(),
    include_package_data=True,
    scripts=['fuss-manager'],
    install_requires=[
        'python-dateutil',
        'tornado',
        'ruamel.yaml',
        'aiodns',
        'isc_dhcp_leases',
    ],
    test_suite='nose2.discover',
    test_requires=[
        'nose2',
        'python-dateutil',
        'tornado',
        'ruamel.yaml',
        'aiodns',
        'isc_dhcp_leases',
    ],
    classifiers=[
        'Framework :: Twisted',
        'Intended Audience :: Education',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',  # noqa: E501
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: System :: Systems Administration',
        'Topic :: System :: Installation/Setup',
    ],
)
