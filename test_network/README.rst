libvirt programming:

 * API reference: https://libvirt.org/sources/java/javadoc/overview-summary.html
 * Python API: https://libvirt.org/docs/libvirt-appdev-guide-python/en-US/html/
 * XML formats: https://libvirt.org/format.html

Get the base image::

   virt-builder debian-9 --format qcow2 \
     -o /tmp/fuss-manager-test-disks/master/debian-9.qcow2 \
     --root-password password:root --ssh-inject root:file:$HOME/.ssh/id_rsa.pub \
     --firstboot provision.sh

See ``test_network/provision/`` for provisioning scripts.

Start the sandbox machine::

   # Network setup
   virsh -c qemu:///system net-define network.xml
   virsh -c qemu:///system net-start fuss_manager_test
   # Check with virsh -c qemu:///system net-list --all

   # Disk setup
   mkdir /tmp/fuss-manager-test-disks
   chgrp libvirt-qemu /tmp/fuss-manager-test-disks
   chmod g+w /tmp/fuss-manager-test-disks

   virsh -c qemu:///system pool-define-as fuss_manager_disks dir --target /tmp/fuss-manager-test-disks
   virsh -c qemu:///system pool-start fuss_manager_disks
   # Check with virsh -c qemu:///system vol-list fuss_manager_disks

   # Clone a sandbox machine. See https://stackoverflow.com/a/26938505
   qemu-img create -f qcow2 -o backing_file=`pwd`/master/debian-9-openstack-amd64.qcow2 /tmp/fuss-manager-test-disks/vm1.qcow2
   virsh -c qemu:///system pool-refresh fuss_manager_disks

   # Machine setup
   virt-install --connect qemu:///system --ram 1024 \
       -n fuss_manager_vm1 --os-type=linux --os-variant=debian9 \
       --network network=fuss_manager_test \
       --disk vol=fuss_manager_disks/vm1.qcow2,device=disk,format=qcow2 \
       --vcpus=1 --vnc --import


Shut down everything and restore system state::

   virsh -c qemu:///system pool-destroy fuss_manager_disks
   virsh -c qemu:///system pool-undefine fuss_manager_disks
   virsh -c qemu:///system net-stop fuss_manager_test
   virsh -c qemu:///system net-undefine fuss_manager_test
   

Other links:

 * http://rabexc.org/posts/how-to-get-started-with-libvirt-on/
