import libvirt
import logging

log = logging.getLogger("test_network")


class Component:
    """
    Lazily instantiated wrapper for a libvirt object
    """

    def __init__(self, system, name):
        self.system = system
        self.name = name
        self.instance = None

    def start(self):
        """
        Configure, start and return a libvirt object for this component.

        This function should be idempotent with regards to side effects on the
        system.
        """
        raise NotImplementedError(
            "{}.start called but not implemented".format(
                self.__class__.__name__
            )
        )

    def shutdown(self):
        """
        Configure, start and return a libvirt object for this component.

        This function should be idempotent with regards to side effects on the
        system.
        """
        raise NotImplementedError(
            "{}.shutdown called but not implemented".format(
                self.__class__.__name__
            )
        )

    def get(self):
        """
        Return the libvirt object for this component, starting it if needed.
        """
        if self.instance is None:
            self.instance = self.start()
        return self.instance


class Connection(Component):
    """
    libvirt connection
    """

    def start(self):
        """
        Open and return a libvirt connection
        """
        log.debug("connecting to %s", self.system.libvirt_uri)
        return libvirt.open(self.system.libvirt_uri)

    def shutdown(self):
        if self.instance:
            self.instance.close()
        self.instance = None
