import logging
import io
import uuid
import random
import os
import xml.etree.ElementTree as ET
from .base import Component
from .image import Image

log = logging.getLogger("test_network")


class Domain(Component):
    def __init__(self, system, name, base="debian-9", mac=None, ip=None):
        super().__init__(system, name)
        self.base = base
        if mac is None:
            mac = "02:00:00:{:02x}:{:02x}:{:02x}".format(
                random.randint(0, 255),
                random.randint(0, 255),
                random.randint(0, 255),
            )
        self.mac = mac
        if ip is None:
            # This is a convenience fallback with works for one machine or two,
            # but conflicts on such a small range are quite likely
            ip = "192.168.123.{:d}".format(random.randint(2, 254))
        self.ip = ip
        self.domain_xml = os.path.join(
            os.path.dirname(__file__), "provision", base + ".xml"
        )
        self._image = None

    @property
    def image(self):
        self._image = Image(self.system, self.name, self.base)
        return self._image.get()

    def lookup(self):
        """
        Lookup a domain object for this domain
        """
        # Iterate domains to prevent a spurious message on stderr if
        # lookupByName fails
        for domain in self.system.connection.listAllDomains():
            if domain.name() == self.name:
                return domain
        return None

        # This prints noise on stderr if lookup fails
        # try:
        #     return self.system.connection.lookupByName(self.name)
        # except libvirt.libvirtError as e:
        #     if str(e).startswith("Domain not found:"):
        #         return None
        #     raise

    def get_xml_config(self):
        tree = ET.parse(self.domain_xml)
        root = tree.getroot()

        el = ET.SubElement(root, "name")
        el.text = self.name
        el = ET.SubElement(root, "uuid")
        el.text = str(uuid.uuid4())
        for devices in root.findall("devices"):
            for disk in devices.findall("disk"):
                el = ET.SubElement(disk, "source")
                el.set("file", self.image)
                el.text = str(uuid.uuid4())
            for interface in devices.findall("interface"):
                if interface.attrib.get("type", None) == "network":
                    el = ET.SubElement(interface, "mac")
                    el.set("address", self.mac)
                    el = ET.SubElement(interface, "source")
                    el.set("network", self.system.network.name())

        # # Machine setup
        # virt-install --connect qemu:///system --ram 1024 \
        #     -n $NAME --os-type=linux --os-variant=debian9 \
        #     --network network=fuss_manager_test \
        #     --disk vol=fuss_manager_disks/$NAME.qcow2,device=disk,format=qcow2 \  # noqa: E501
        #     --vcpus=1 --vnc --import

        # virt-install --dry-run --print-xml --connect qemu:///system
        #  --ram 1024 -n fuss_manager_vm1 --os-type=linux --os-variant=debian9
        #  --network network=fuss_manager_test --disk vol=fuss_manager_disks/test.qcow2,device=disk,format=qcow2  # noqa: E501
        #  --vcpus=1 --vnc --import

        with io.StringIO() as fd:
            tree.write(fd, encoding="unicode")
            return fd.getvalue()

    def start(self):
        domain = self.lookup()
        if domain is not None:
            if domain.isActive():
                log.info("Halting existing domain %s", self.name)
                domain.destroy()
            log.info("Undefining existing domain %s", self.name)
            domain.undefine()

        log.info("Defining domain %s using %s", self.name, self.domain_xml)
        domain = self.system.connection.defineXML(self.get_xml_config())
        domain.create()

        return domain

#        self.ip = None
#        start = time.time()
#        while self.ip is None and time.time() < start + 20:
#            for lease in network.get_dhcp_leases(self.mac):
#                ipaddr = lease.get("ipaddr", None)
#                if ipaddr is not None:
#                    self.ip = ipaddr
#                    break
#            else:
#                log.info("waiting 0.5s for an IP address to appear")
#                time.sleep(0.5)
#        if self.ip is None:
#            log.warning("no IP found for the domain")

#    def get_dhcp_leases(self, mac):
#        return self.network.DHCPLeases(mac=mac)

    def shutdown(self):
        domain = self.instance
        if domain is None:
            domain = self.lookup()
        if domain is None:
            return
        if domain.isActive():
            log.info("Halting domain %s", self.name)
            domain.destroy()
        log.info("Undefining domain %s", self.name)
        domain.undefine()
        if self._image:
            self._image.shutdown()
        self.instance = None
