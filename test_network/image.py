import logging
import os
import subprocess
from .base import Component

log = logging.getLogger("test_network")


class Image(Component):
    def __init__(self, system, name, base):
        super().__init__(system, name)
        self.base = base
        self.pathname = os.path.abspath(
            os.path.join(
                self.system.storage_root, "images", self.name + ".qcow2"
            )
        )

    def start(self):
        src = self.system.master_image(self.base)
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        else:
            os.makedirs(os.path.dirname(self.pathname), exist_ok=True)

        # Clone a sandbox machine. See https://stackoverflow.com/a/26938505
        log.info("Creating %s based on %s", self.pathname, src)
        subprocess.run(
            [
                "qemu-img",
                "create",
                "-f",
                "qcow2",
                "-o",
                "backing_file=" + src,
                self.pathname,
            ],
            stdout=subprocess.DEVNULL,
            check=True,
        )

        # virsh -c qemu:///system pool-refresh fuss_manager_disks
        self.system.storage.refresh(0)

        return self.pathname

    def shutdown(self):
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.instance = None
