import logging
import os
import subprocess
from .base import Component

log = logging.getLogger("test_network")


class MasterImage(Component):
    """
    Wrap a path to a base image
    """

    def __init__(self, system, name):
        super().__init__(system, name)
        self.pathname = os.path.abspath(
            os.path.join(
                self.system.storage_root, "master", self.name + ".qcow2"
            )
        )
        self.provision_script = os.path.join(
            os.path.dirname(__file__), "provision", self.name + ".sh"
        )

        self.ssh_public_key = None
        for path in ("~/.ssh/id_rsa.pub",):
            path = os.path.expanduser(path)
            if os.path.exists(path):
                self.ssh_public_key = path
                break

    def lookup(self):
        if os.path.exists(self.pathname):
            return self.pathname
        return None

    def start(self):
        pathname = self.lookup()
        if pathname is None:
            os.makedirs(os.path.dirname(self.pathname), exist_ok=True)

            # virt-builder debian-9 --format qcow2 \
            #   -o /tmp/fuss-manager-test-disks/master/debian-9.qcow2 \
            #   --root-password password:root \
            #   --ssh-inject root:file:/home/enrico/.ssh/id_rsa.pub
            #   --firstboot provision.sh
            log.info(
                "Building master image %s as %s", self.name, self.pathname
            )
            cmd = [
                "virt-builder",
                self.name,
                "--format",
                "qcow2",
                "-o",
                self.pathname,
                "--root-password",
                "password:root",
            ]
            if self.ssh_public_key:
                log.info(" using ssh public key %s", self.ssh_public_key)
                cmd += ["--ssh-inject", "root:file:" + self.ssh_public_key]
            if os.path.exists(self.provision_script):
                log.info(
                    " using provisioning script %s", self.provision_script
                )
                cmd += ["--firstboot", self.provision_script]
            else:
                log.info(
                    " no provisioning script found for %s (%s)",
                    self.name,
                    self.provision_script,
                )
            subprocess.run(cmd, check=True)

        return self.pathname

    def shutdown(self):
        # Do not delete the image, which is expensive to make
        self.instance = None
