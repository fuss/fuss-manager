import logging
import libvirt
import uuid
import xml.etree.ElementTree as ET
from .base import Component

log = logging.getLogger("test_network")


class Network(Component):
    """
    virNetwork management
    """
    def lookup(self):
        """
        Lookup a virNetwork object for the test network.
        """
        # Iterate networks to prevent a spurious message on stderr if
        # networkLookupByName fails
        if self.name not in self.system.connection.listNetworks():
            return None

        try:
            return self.system.connection.networkLookupByName(self.name)
        except libvirt.libvirtError as e:
            if str(e).startswith("Network not found:"):
                return None
            raise

    def get_xml_config(self):
        root = ET.Element("network")
        ET.SubElement(root, "name").text = self.name
        ET.SubElement(root, "uuid").text = str(uuid.uuid4())
        ET.SubElement(root, "forward").set("mode", "nat")
        bridge = ET.SubElement(root, "bridge")
        bridge.set("stp", "on")
        bridge.set("delay", "0")
        ET.SubElement(root, "mac").set("address", "52:54:00:17:36:c7")
        ip = ET.SubElement(root, "ip")
        ip.set("address", "192.168.123.1")
        ip.set("netmask", "255.255.255.0")
        dhcp = ET.SubElement(ip, "dhcp")
        dhcp_range = ET.SubElement(dhcp, "range")
        dhcp_range.set("start", "192.168.123.1")
        dhcp_range.set("end", "192.168.123.254")
        for domain in self.system.domains:
            host = ET.SubElement(dhcp, "host")
            host.set("mac", domain.mac)
            host.set("ip", domain.ip)
            host.set("name", domain.name)

        return ET.tostring(root, encoding="unicode")

    def start(self):
        """
        Define, activate and return the virNetwork object for this system
        """
        # virsh -c qemu:///system net-define network.xml
        network = self.lookup()
        if network is None:
            log.info("define network %s", self.name)
            xml_config = self.get_xml_config()
            network = self.system.connection.networkDefineXML(xml_config)

        if not network.isActive():
            # virsh -c qemu:///system net-start $name
            log.info("start network %s", self.name)
            network.create()

        return network

    def shutdown(self):
        """
        Make sure the networking is stopped and completely deconfigured.
        """
        network = self.instance
        if network is None:
            network = self.lookup()
        if network is None:
            return

        # virsh -c qemu:///system net-stop $name
        # virsh -c qemu:///system net-undefine $name
        if network.isActive():
            log.info("destroy network %s", self.name)
            network.destroy()
        log.info("undefine network %s", self.name)
        network.undefine()
        self.instance = None
