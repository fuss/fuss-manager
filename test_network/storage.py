import logging
import os
import libvirt
from .base import Component

log = logging.getLogger("test_network")


class Storage(Component):
    """
    virStoragePool management
    """

    def lookup(self):
        # Iterate storage pools to prevent a spurious message on stderr if
        # toragePoolLookupByName fails
        if self.name not in self.system.connection.listAllStoragePools(0):
            return None

        try:
            return self.system.connection.storagePoolLookupByName(self.name)
        except libvirt.libvirtError as e:
            if str(e).startswith("Storage pool not found:"):
                return None
            raise

    def start(self):
        """
        Define, start and return the virStoragePool object for this system

        This method is idempotent.
        """
        storage = self.lookup()
        if storage is None:
            # virsh -c qemu:///system pool-define-as fuss_manager_disks \
            #       dir --target {{self.root}}/images
            images_dir = os.path.join(self.system.storage_root, "images")
            os.makedirs(images_dir, exist_ok=True)

            # TODO: check that things are readable/writable by the group
            # libvirt (how?)
            # TIL: namei -l `readlink -f .` which is nice, but still not
            # useful enough
            # master_dir = os.path.join(self.root, "master")
            # images_dir = os.path.join(self.root, "images")

            storage_cfg = """
<pool type='dir'>
  <name>{name}</name>
  <uuid>1309e8e7-6705-4d64-a584-55d539b0a201</uuid>
  <target>
    <path>{path}</path>
  </target>
</pool>
""".format(
                name=self.name, path=images_dir
            )
            log.info("Defining storage pool %s on %s", self.name, images_dir)
            storage = self.system.connection.storagePoolDefineXML(
                storage_cfg, 0
            )

        # virsh -c qemu:///system pool-start fuss_manager_disks
        if not storage.isActive():
            storage.create(0)

        return storage

    def shutdown(self):
        """
        Make sure the storage pool is stopped and completely deconfigured.

        This method is idempotent.
        """
        storage = self.instance
        if storage is None:
            storage = self.lookup()
        if storage is None:
            return

        log.info("Undefining storage %s", self.name)
        # virsh -c qemu:///system pool-destroy fuss_manager_disks
        if storage.isActive():
            storage.destroy()
        # virsh -c qemu:///system pool-undefine fuss_manager_disks
        storage.undefine()
        # TODO: remove snapshot images?
        self.instance = None
