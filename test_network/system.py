import logging
import sys
import os
import socket
import time
from . import Connection, Storage, Network, Domain, MasterImage

log = logging.getLogger("test_network")


class System:
    """
    Holds the configuration about a test system, and the machinery to start it,
    stop it, and control it
    """
    def __init__(
            self,
            uri: str = "qemu:///system",
            root: str = None,
            storage_root: str = None):

        self.libvirt_uri = uri

        if root is None:
            self.root = os.path.dirname(sys.argv[0])
        else:
            self.root = root

        if storage_root is None:
            self.storage_root = root
        else:
            self.storage_root = storage_root

        self._connection = None
        self._network = None
        self._storage = None
        self._master_images = {}
        self._domains = {}

    def add_domain(self, name, **kw):
        self._domains[name] = Domain(self, name, **kw)

    @property
    def connection(self):
        if self._connection is None:
            self._connection = Connection(self, "libvirt")
        return self._connection.get()

    @property
    def network(self):
        if self._network is None:
            self._network = Network(self, "fuss_manager_test")
        return self._network.get()

    @property
    def storage(self):
        if self._storage is None:
            self._storage = Storage(self, 'fuss_manager_disks')
        return self._storage.get()

    @property
    def domains(self):
        return self._domains.values()

    def master_image(self, name):
        master_image = self._master_images.get(name)
        if master_image is None:
            master_image = MasterImage(self, name)
            master_image.start()
            self._master_images[name] = master_image
        return master_image.get()

    def start_domains(self):
        for domain in self._domains.values():
            domain.start()
        # TODO: wait until started?

    def wait_for_domains(self, timeout=60):
        ips = set(x.ip for x in self.domains)
        start = time.time()
        while ips and time.time() < start + timeout:
            log.info("Waiting for domains to become reachable via ssh")
            for ip in list(ips):
                try:
                    conn = socket.create_connection((ip, 22))
                except (ConnectionRefusedError, OSError):
                    continue
                conn.close()
                ips.discard(ip)
                log.info("%s is now reachable", ip)
            time.sleep(0.2)

    def shutdown(self):
        for domain in self._domains.values():
            domain.shutdown()
        if self._network is None:
            self._network = Network(self, "fuss_manager_test")
        self._network.shutdown()
        if self._storage is None:
            self._storage = Storage(self, 'fuss_manager_disks')
        self._storage.shutdown()
        for img in self._master_images.values():
            img.shutdown()
        if self._connection:
            self._connection.shutdown()
