import asyncio
import shutil
import http.cookiejar
from unittest import SkipTest

import tornado.testing
import tornado.httpclient

from manager import events, compat


class StopTest(events.Event):
    pass


class GatherEvents:
    def __init__(self, event_hub, event_filter=None):
        self.gathered = []
        loop = asyncio.get_event_loop()
        self.received_first = compat.create_future(loop)
        event_hub.subscribe(self.gather_events, event_filter)

    async def gather_events(self, evt):
        if not self.received_first.done():
            self.received_first.set_result(True)
        self.gathered.append(evt)


class TestDataSourceMixin:
    """
    Start a data source on setUp and stop it when StopTest is posted on the
    event hub
    """

    def setUp(self, *args, **kw):
        super().setUp(*args, **kw)
        self.event_hub = events.Hub()
        self.event_hub.subscribe(self.stop_test, [StopTest])
        self.ds = self.make_datasource()

    async def stop_test(self, evt):
        await self.ds.shutdown()


class FussTestNetwork:
    def __init__(self):
        self.require_test_machines()

    def require_test_machines(self):
        """
        Raise SkipTest unless the fuss-test-network VMs are up and running
        """
        import socket

        try:
            conn = socket.create_connection(("192.168.123.2", 22), timeout=0.1)
        except (ConnectionRefusedError, OSError):
            raise SkipTest("test network is not set up")
        else:
            conn.close()

    def write_inventory(self, fd, group_name="test"):
        print("[{}]".format(group_name), file=fd)
        for name, ip in (
            ("test1", "192.168.123.2"),
            ("test2", "192.168.123.3"),
        ):
            print(
                (
                    '{name} ansible_host={ip} ansible_user=root'
                    ' ansible_ssh_extra_args="-oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no"'  # noqa: E501
                ).format(name=name, ip=ip),
                file=fd,
            )


class InotifyTestCase(tornado.testing.AsyncTestCase):
    def setUp(self):
        path_inotifywait = shutil.which("inotifywait")
        if not path_inotifywait:
            raise SkipTest("inotify is not available")
        super().setUp()


#
# AsyncHTTPClientWithCookies implementation
#


class JarRequestAdapter:
    """
    Work around stdlib's cookie jar having a stupidly limited API that works
    only with stdlib request/response objects
    """

    def __init__(self, tornado_request):
        self.tornado_request = tornado_request
        self.unverifiable = False
        self.origin_req_host = http.cookiejar.request_host(self)

    def get_full_url(self):
        return self.tornado_request.url

    def has_header(self, name):
        return name in self.tornado_request.headers

    def add_unredirected_header(self, key, val):
        self.tornado_request.headers[key] = val


class JarResponseAdapter:
    """
    Work around stdlib's cookie jar having a stupidly limited API that works
    only with stdlib request/response objects
    """

    def __init__(self, tornado_response):
        self.tornado_response = tornado_response

    def info(self):
        from email.message import Message

        res = Message()
        for name, value in self.tornado_response.headers.get_all():
            res.add_header(name, value)
        return res


class AsyncHTTPClientWithCookies:
    """
    Wrapper for AsyncHTTPClient which handles cookies between requests
    """

    def __init__(self, client):
        self.client = client
        self.jar = http.cookiejar.CookieJar()
        self.xsrf_token = None

    def close(self):
        self.client.close()

    async def fetch(self, *args, **kw):
        raise_error = kw.pop("raise_error", False)
        request = tornado.httpclient.HTTPRequest(*args, **kw)
        self.jar.add_cookie_header(JarRequestAdapter(request))
        if self.xsrf_token is not None:
            request.headers["X-XSRFToken"] = self.xsrf_token
        response = await self.client.fetch(request, raise_error=raise_error)
        self.jar.extract_cookies(
            JarResponseAdapter(response), JarRequestAdapter(request)
        )
        return response

    def get_cookie(self, name):
        for cookie in self.jar:
            if cookie.name == name:
                return cookie.value
        raise KeyError(name)

    def enable_xsrf_header(self):
        self.xsrf_token = self.get_cookie("_xsrf")

    def disable_xsrf_header(self):
        self.xsrf_token = None
