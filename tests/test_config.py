import tempfile
import unittest

from manager import config


class TestEventHub(unittest.TestCase):
    def test_config_no_file(self):
        conf = config.Config()
        # All values should have been loaded, with default values
        for k, v in config.DEFAULTS.items():
            self.assertEqual(getattr(conf, k), v)

    def test_config_empty_file(self):
        conf_file = tempfile.NamedTemporaryFile()
        conf = config.Config(conf_file.name)
        # All values should have been loaded, with default values
        for k, v in config.DEFAULTS.items():
            self.assertEqual(getattr(conf, k), v)

    def test_config_different_file(self):
        conf = config.Config('tests/data/fuss_manager.yaml.override')
        # One "randomish" value that should have the default value
        self.assertEqual(
            conf.path_ansible_inventory, '/etc/fuss-manager/hosts'
        )
        # And the values that have been changed in the file above
        self.assertEqual(conf.web_port, 8888)
        self.assertEqual(conf.debug, True)

    def test_config_mock(self):
        conf = config.MockConfig()
        # All mock values should have been loaded
        for k, v in config.MOCK_VALUES.items():
            self.assertEqual(getattr(conf, k), v)
        # And all values should be available
        for k, v in config.DEFAULTS.items():
            self.assertTrue(hasattr(conf, k))

    def test_permissions(self):
        conf = config.MockConfig()
        self.assertIn('admin', conf.permissions['users'])
        self.assertIn('user', conf.permissions['groups'])

    def test_permissions_empty_file(self):
        conf = config.MockConfig()
        emptyfile = tempfile.NamedTemporaryFile()
        conf.permission_file = emptyfile.name
        conf._load_perms()
        self.assertEqual(conf.permissions['users'], {})
        self.assertEqual(conf.permissions['groups'], {})
