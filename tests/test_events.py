from tornado.testing import gen_test, AsyncTestCase
from manager import events


class TestEventHub(AsyncTestCase):
    @gen_test
    async def test_subscribe_all(self):
        hub = events.Hub()

        ev1 = events.Event(None)
        ev2 = events.Event(None)

        collected = []

        async def collect(evt):
            collected.append(evt)

        hub.subscribe(collect)

        await hub.publish(ev1)
        await hub.publish(ev2)

        self.assertEqual(collected, [ev1, ev2])

    @gen_test
    async def test_subscribe_filtered(self):
        class Custom(events.Event):
            pass

        hub = events.Hub()

        ev1 = Custom(None)
        ev2 = events.Event(None)

        collected = []

        async def collect(evt):
            collected.append(evt)

        hub.subscribe(collect, [Custom])

        await hub.publish(ev1)
        await hub.publish(ev2)

        self.assertEqual(collected, [ev1])

    @gen_test
    async def test_unsubscribe(self):
        hub = events.Hub()

        ev1 = events.Event(None)
        ev2 = events.Event(None)

        collected = []

        async def collect(evt):
            collected.append(evt)

        hub.subscribe(collect)

        await hub.publish(ev1)
        hub.unsubscribe(collect)
        await hub.publish(ev2)

        self.assertEqual(collected, [ev1])
