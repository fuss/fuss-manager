import asyncio
import tempfile
import os

from tornado.testing import gen_test

from .common import InotifyTestCase

from manager.sources.inotifywait import Inotifywait


class TestInotifywatch(InotifyTestCase):
    """
    Test the Inotifywatch helper
    """
    @gen_test
    async def test_start_stop(self):
        with tempfile.TemporaryDirectory() as path:
            fname = os.path.join(path, "testfile")
            watch = Inotifywait(fname)

            async def do_test():
                self.assertEqual(await watch.next_event(), "START")
                watch.stop()
                self.assertEqual(await watch.next_event(), "END")

            await asyncio.gather(watch.run(), do_test())

    @gen_test
    async def test_write(self):
        with tempfile.TemporaryDirectory() as path:
            fname = os.path.join(path, "testfile")
            watch = Inotifywait(fname)

            async def do_test():
                self.assertEqual(await watch.next_event(), "START")
                # Do a normal write on the file
                with open(fname, "at") as fd:
                    print("UPDATE", file=fd)
                    # No changes detected while the file is still open
                    self.assertFalse(watch.has_events())
                # we get two events here: one triggered by a MODIFY and
                # one triggered by a CLOSE_WRITE
                self.assertEqual(await watch.next_event(), "MODIFIED")
                self.assertEqual(await watch.next_event(), "MODIFIED")
                watch.stop()
                self.assertEqual(await watch.next_event(), "END")

            await asyncio.gather(watch.run(), do_test())

    @gen_test
    async def test_atomic_write(self):
        with tempfile.TemporaryDirectory() as path:
            fname = os.path.join(path, "testfile")
            watch = Inotifywait(fname)

            async def do_test():
                self.assertEqual(await watch.next_event(), "START")
                # Do an atomic update via move
                with open(fname + ".tmp", "at") as fd:
                    print("UPDATE", file=fd)
                # No changes detected while only the tempfile has been written
                self.assertFalse(watch.has_events())
                os.rename(fname + ".tmp", fname)
                # The move is detected
                self.assertEqual(await watch.next_event(), "MODIFIED")
                watch.stop()
                self.assertEqual(await watch.next_event(), "END")

            await asyncio.gather(watch.run(), do_test())
