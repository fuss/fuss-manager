import tempfile
import os.path
from unittest import TestCase, mock

from manager.manager import JsonableLog
from manager import ops


class TestOperationLog(TestCase):
    def test_create(self):
        with tempfile.TemporaryDirectory() as td:
            pathname = os.path.join(td, "logfile")

            with mock.patch("time.time", return_value=100):
                oplog = JsonableLog(pathname)
                oplog.log(ops.Noop())
                oplog.close()

            with open(pathname, "rt") as fd:
                read_data = fd.read()
            self.assertIn('"name": "Noop"', read_data)
            self.assertIn('"source": null', read_data)
            self.assertIn('"ts": 100', read_data)

    def test_append(self):
        with tempfile.TemporaryDirectory() as td:
            pathname = os.path.join(td, "logfile")

            with open(pathname, "wt") as fd:
                fd.write("TEST")

            with mock.patch("time.time", return_value=100):
                oplog = JsonableLog(pathname)
                oplog.log(ops.Noop())
                oplog.close()

            with open(pathname, "rt") as fd:
                read_data = fd.read()
            self.assertIn('"name": "Noop"', read_data)
            self.assertIn('"source": null', read_data)
            self.assertIn('"ts": 100', read_data)

    def test_dirnotexists(self):
        with tempfile.TemporaryDirectory() as td:
            pathname = os.path.join(td, "wrongdir", "logfile")

            oplog = JsonableLog(pathname)
            oplog.log(ops.Noop())
            oplog.close()

            self.assertTrue(os.path.exists(pathname))
