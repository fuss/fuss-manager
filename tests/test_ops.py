import asyncio
import functools
from contextlib import contextmanager
import tempfile
import os
import sys
import traceback
import textwrap
from unittest import TestCase

from manager.config import Config
from manager.manager import Manager
from manager import ops, events
from manager.users.db import User, Group


class ManagerTestException(Exception):
    def __init__(self, cls, value, tb_fmt):
        self.cls = cls
        self.value = value
        self.tb_fmt = tb_fmt

    def __str__(self):
        return "\n".join(
            (
                "{}({})".format(self.cls.__name__, repr(self.value)),
                "----8<---- traceback in manager test ----8<----",
                textwrap.indent(self.tb_fmt.rstrip(), "  "),
                "----8<---- traceback in manager test ----8<----",
            )
        )


def manager_test(testfunc):
    testfunc = asyncio.coroutine(testfunc)

    @functools.wraps(testfunc)
    def wrap(self):
        exception_type = None
        exception_val = None
        exception_formatted = None

        @asyncio.coroutine
        def test_main():
            try:
                yield from testfunc(self)
            except Exception:
                nonlocal exception_type, exception_val, exception_formatted
                exception_type, exception_val, exception_tb = sys.exc_info()
                exception_formatted = traceback.format_exc()
            yield from self.manager.shutdown()

        self.loop.run_until_complete(
            asyncio.gather(self.manager.run(), test_main(),)
        )

        if exception_type is not None:
            raise ManagerTestException(
                exception_type, exception_val, exception_formatted
            )

    return wrap


class TestManagerMixin(TestCase):
    def get_config(self):
        config = Config()
        config.operation_log_file = os.path.join(self.tempdir.name, "ops.log")
        config.path_ansible_inventory = os.path.join(
            self.tempdir.name, "inventory"
        )
        with open(config.path_ansible_inventory, 'w') as fp:
            fp.write('')
        config.path_stats_cache = os.path.join(self.tempdir.name, "stats")
        config.disabled_sources = (
            "ArpwatchDataSource",
            "DhcpdDataSource",
            "ChaosDataSource",
        )
        config.debug = True
        config.permission_file = 'tests/data/perms.yaml'
        config._load_perms()
        return config

    def get_manager(self):
        return Manager(self.config)

    def setUp(self):
        super().setUp()
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.loop.set_debug(True)
        self.tempdir = tempfile.TemporaryDirectory()
        self.config = self.get_config()
        self.manager = self.get_manager()
        self.loop.run_until_complete(self.manager.init())

    def tearDown(self):
        self.tempdir.cleanup()
        self.manager = None
        self.config = None
        try:
            pending = asyncio.all_tasks(self.loop)
            try:
                self.loop.run_until_complete(asyncio.gather(*pending))
            except asyncio.CancelledError:
                pass
        finally:
            asyncio.set_event_loop(None)
            self.loop.close()
        super().tearDown()

    @contextmanager
    def gather_events(self):
        gathered = []

        @asyncio.coroutine
        def gather_events(evt):
            gathered.append(evt)

        self.manager.event_hub.subscribe(gather_events)
        try:
            yield gathered
        finally:
            self.manager.event_hub.unsubscribe(gather_events)
        return gathered


class TestOps(TestManagerMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.config = self.get_config()
        self.tf_op_log = tempfile.NamedTemporaryFile()
        self.config.operation_log_file = self.tf_op_log.name
        self.tf_stats = tempfile.NamedTemporaryFile()
        self.tf_stats.write(b"{}")
        self.tf_stats.flush()
        self.config.path_stats_cache = self.tf_stats.name
        self.users = {
            'admin': User(name='admin', id=123),
            'user': User(
                name='user', id=124, groups=[Group(name='user', id=124)]
            ),
            'none': None,
        }

    @manager_test
    def test_noop(self):
        with self.gather_events() as gathered:
            yield from self.manager.execute(ops.Noop())
        self.assertEqual(gathered, [])

    @manager_test
    def test_add_del_group(self):
        test_mac = "11:22:33:44:55:66"
        yield from self.manager.event_hub.publish(
            events.HostSeenEvent(None, mac=test_mac)
        )
        for user in ['admin']:
            with self.gather_events() as gathered:
                yield from self.manager.execute(
                    ops.AddGroup(
                        mac=test_mac, group="test", user=self.users[user],
                    )
                )
            self.assertEqual(gathered[0].changes, {'groups': ((), ('test',))})

        for user in ['admin']:
            with self.gather_events() as gathered:
                yield from self.manager.execute(
                    ops.DelGroup(
                        mac=test_mac, group="test", user=self.users[user],
                    )
                )
            self.assertEqual(gathered[0].changes, {'groups': (('test',), ())})

        for user in ['user', 'none']:
            with self.gather_events() as gathered:
                with self.assertRaises(ops.OpPermissionDenied):
                    yield from self.manager.execute(
                        ops.AddGroup(
                            mac=test_mac, group="test", user=self.users[user],
                        )
                    )
            self.assertEqual(gathered, [])

        for user in ['user', 'none']:
            with self.gather_events() as gathered:
                with self.assertRaises(ops.OpPermissionDenied):
                    yield from self.manager.execute(
                        ops.DelGroup(
                            mac=test_mac, group="test", user=self.users[user],
                        )
                    )
            self.assertEqual(gathered, [])

    @manager_test
    def test_delgroup(self):
        test_mac = "11:22:33:44:55:66"
        yield from self.manager.event_hub.publish(
            events.HostSeenEvent(None, mac=test_mac)
        )

        for user in ['admin']:
            with self.gather_events() as gathered:
                yield from self.manager.execute(
                    ops.DelGroup(
                        mac=test_mac, group="test", user=self.users[user],
                    )
                )
            self.assertEqual(gathered, [])

        for user in ['user', 'none']:
            with self.gather_events() as gathered:
                with self.assertRaises(ops.OpPermissionDenied):
                    yield from self.manager.execute(
                        ops.DelGroup(
                            mac=test_mac, group="test", user=self.users[user],
                        )
                    )
            self.assertEqual(gathered, [])

    @manager_test
    def test_wakeonlan(self):
        mac = "11:22:33:44:55:66"
        for user in ['admin']:
            with self.gather_events() as gathered:
                yield from self.manager.execute(
                    ops.WakeOnLan(macs=[mac], user=self.users[user],)
                )
            self.assertEqual(gathered, [])

        for user in ['user', 'none']:
            with self.gather_events() as gathered:
                with self.assertRaises(ops.OpPermissionDenied):
                    yield from self.manager.execute(
                        ops.WakeOnLan(macs=[mac], user=self.users[user],)
                    )
            self.assertEqual(gathered, [])
