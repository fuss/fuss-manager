from unittest import mock
import tempfile
import os

from tornado.testing import gen_test, AsyncTestCase

import manager.playbook as pb
from .common import (
    GatherEvents,
    FussTestNetwork,
)
from manager import events
from manager.config import Config
from manager.compat import isoparse


class TestPlaybook(AsyncTestCase):
    """
    Test running ansible playbooks
    """

    def setUp(self, *args, **kw):
        super().setUp(*args, **kw)
        self.workdir = tempfile.TemporaryDirectory()
        self.config = Config()
        self.config.path_ansible_inventory = os.path.join(
            self.workdir.name, "inventory"
        )
        self.config.path_playbooks = self.workdir.name
        self.config.playbook_log_dir = os.path.join(
            self.workdir.name, "playbook.log"
        )
        self.event_hub = events.Hub()
        self.ansible = pb.Ansible(self.event_hub, self.config)

    def tearDown(self, *args, **kw):
        self.workdir.cleanup()
        super().tearDown(*args, **kw)

    def test_list_playbooks(self):
        def write_playbook(name, title):
            pathname = os.path.join(self.config.path_playbooks, name)
            os.makedirs(os.path.dirname(pathname), exist_ok=True)
            with open(pathname, "wt") as fd:
                print("---", file=fd)
                print(" - name:", title, file=fd)

        write_playbook("test1.yaml", "Test 1")
        write_playbook("test1.yml", "Test 1 yml")
        write_playbook("test2.yaml", "Test 2")
        write_playbook("subdir/test.yaml", "Test Subdir")

        res = self.ansible.list_playbooks()
        self.assertEqual(
            res,
            [
                {"name": "subdir/test.yaml", "title": "Test Subdir"},
                {"name": "test1.yaml", "title": "Test 1"},
                {"name": "test1.yml", "title": "Test 1 yml"},
                {"name": "test2.yaml", "title": "Test 2"},
            ],
        )

    @gen_test(timeout=10)
    async def test_run_playbook(self):
        net = FussTestNetwork()

        with open(
            os.path.join(self.config.path_playbooks, "test.yaml"), "wt"
        ) as fd:
            fd.write(
                """
---
 - name: Test
   hosts: all
   tasks:
    - name: Ping machine
      ping:
    - name: Echo
      command: echo Test
"""
            )

        with open(self.config.path_ansible_inventory, "wt") as fd:
            net.write_inventory(fd)

        evts = GatherEvents(
            self.event_hub, [events.HostEvent, events.AnsibleEvent]
        )

        await self.ansible.run_playbook("test.yaml")

        self.assertEqual(len(evts.gathered), 3)

        evt = evts.gathered.pop(0)
        self.assertIsInstance(evt, events.AnsiblePlaybookEvent)
        self.assertEqual(evt.playbook, "test.yaml")
        results = evt.results
        self.assertEqual(
            results["stats"],
            {
                "test1": {
                    'changed': 1,
                    'failures': 0,
                    'ok': 3,
                    'skipped': 0,
                    'unreachable': 0,
                    'ignored': 0,
                    'rescued': 0,
                },
                "test2": {
                    'changed': 1,
                    'failures': 0,
                    'ok': 3,
                    'skipped': 0,
                    'unreachable': 0,
                    'ignored': 0,
                    'rescued': 0,
                },
            },
        )
        plays = results["plays"]
        self.assertEqual(len(plays), 1)
        play = plays[0]
        self.assertEqual(play["play"]["name"], "Test")

        play_id = play["play"]["id"]
        self.assertEqual(evt.playbook_id, play_id)

        tasks = play["tasks"]
        self.assertEqual(len(tasks), 3)
        self.assertEqual(tasks[0]["task"]["name"], "Gathering Facts")
        self.assertEqual(tasks[1]["task"]["name"], "Ping machine")
        self.assertEqual(tasks[2]["task"]["name"], "Echo")

        evts.gathered.sort(key=lambda x: x.name)

        self.assertIsInstance(evts.gathered[0], events.HostPlaybookEvent)
        self.assertEqual(evts.gathered[0].name, "test1")
        self.assertEqual(evts.gathered[0].playbook_id, play_id)

        self.assertIsInstance(evts.gathered[1], events.HostPlaybookEvent)
        self.assertEqual(evts.gathered[1].name, "test2")
        self.assertEqual(evts.gathered[1].playbook_id, play_id)

        data = self.ansible.playbook_log.load(play_id)
        self.assertEqual(data["results"], results)

        start = isoparse(play["play"]["duration"]["start"])
        end = isoparse(play["play"]["duration"]["end"])
        logs = list(
            self.ansible.playbook_log.list_month(start.year, start.month)
        )
        self.assertEqual(len(logs), 1)
        self.assertEqual(logs[0]["playbook"], "test.yaml")
        self.assertEqual(logs[0]["start"], start.timestamp())
        self.assertEqual(logs[0]["end"], end.timestamp())
        self.assertEqual(logs[0]["id"], play_id)
        self.assertEqual(logs[0]["title"], "Test")


class TestSetup(AsyncTestCase):
    """
    Test running ansible data collection
    """

    def setUp(self, *args, **kw):
        super().setUp(*args, **kw)
        self.event_hub = events.Hub()

    @gen_test
    async def test_run_playbook_fail(self):
        with tempfile.NamedTemporaryFile("wt") as inventory:
            inventory.write(
                """
[tests]
host.invalid
another.invalid
"""
            )
            inventory.flush()

            config = Config()
            config.path_ansible_inventory = inventory.name
            ansible = pb.Ansible(self.event_hub, config)

            evts = GatherEvents(self.event_hub, [events.HostEvent])
            await ansible.gather_facts('host.invalid,another.invalid')
            self.assertEqual(len(evts.gathered), 2)

            evts.gathered.sort(key=lambda x: x.name)

            self.assertIsInstance(evts.gathered[0], events.HostFactsFailed)
            self.assertEqual(evts.gathered[0].name, "another.invalid")
            self.assertEqual(evts.gathered[0].result, "UNREACHABLE!")

            self.assertIsInstance(evts.gathered[1], events.HostFactsFailed)
            self.assertEqual(evts.gathered[1].name, "host.invalid")
            self.assertEqual(evts.gathered[1].result, "UNREACHABLE!")

    @gen_test
    async def test_run_playbook_success(self):
        net = FussTestNetwork()  # noqa: F841
        with tempfile.NamedTemporaryFile("wt") as inventory:
            inventory.write(
                """
[tests]
192.168.123.2
192.168.123.3
"""
            )
            inventory.flush()

            config = Config()
            config.path_ansible_inventory = inventory.name
            ansible = pb.Ansible(self.event_hub, config)

            evts = GatherEvents(self.event_hub, [events.HostEvent])
            await ansible.gather_facts('192.168.123.2,192.168.123.3')
            self.assertEqual(len(evts.gathered), 2)

            evts.gathered.sort(key=lambda x: x.name)

            self.assertIsInstance(evts.gathered[0], events.HostFactsEvent)
            self.assertEqual(evts.gathered[0].name, "192.168.123.2")
            self.assertIsInstance(evts.gathered[0].facts, dict)

            self.assertIsInstance(evts.gathered[1], events.HostFactsEvent)
            self.assertEqual(evts.gathered[1].name, "192.168.123.3")
            self.assertIsInstance(evts.gathered[1].facts, dict)

    @gen_test
    async def test_parse_setup(self):
        config = Config()
        ansible = pb.Ansible(self.event_hub, config)

        evts = GatherEvents(self.event_hub, [events.HostEvent])

        with mock.patch(
            "manager.playbook.FactsCollector.get_cmdline",
            return_value=["cat", "tests/data/ansible_setup.txt"],
        ):
            await ansible.gather_facts()

        self.assertEqual(len(evts.gathered), 3)

        self.assertIsInstance(evts.gathered[0], events.HostFactsEvent)
        self.assertEqual(evts.gathered[0].name, "192.168.6.105")
        facts = evts.gathered[0].facts
        self.assertIn("ansible_facts", facts)
        self.assertIn('epoch', facts['ansible_facts']['ansible_date_time'])

        self.assertIsInstance(evts.gathered[1], events.HostFactsEvent)
        self.assertEqual(evts.gathered[1].name, "192.168.6.1")
        facts = evts.gathered[1].facts
        self.assertIn("ansible_facts", facts)
        self.assertIn('epoch', facts['ansible_facts']['ansible_date_time'])
        self.assertIn(
            'fuss_server_upgrade', facts['ansible_facts']['ansible_local']
        )

        self.assertIsInstance(evts.gathered[2], events.HostFactsFailed)
        self.assertEqual(evts.gathered[2].name, "invalid")
        self.assertEqual(evts.gathered[2].result, "UNREACHABLE!")


class TestWakeOnLan(AsyncTestCase):
    """
    Test running ansible wakeonlan module
    """

    def setUp(self, *args, **kw):
        super().setUp(*args, **kw)
        self.event_hub = events.Hub()
        self.config = Config()
        self.ansible = pb.Ansible(self.event_hub, self.config)

    @gen_test
    async def test_run_playbook(self):
        await self.ansible.wake_on_lan(
            ["02:00:00:01:02:01", "02:00:00:01:02:02"]
        )
        # no output is saved, and there is really nothing to test
        # (unless we check the network for the magic packet).
