import tempfile
from unittest import mock
import contextlib
from tornado.testing import AsyncTestCase, gen_test
from manager.web.session import (
    SessionManager,
    MemorySessionManager,
    FilesystemSessionManager,
)
from manager.config import MockConfig


class SessionTestMixin:
    pass


class SessionTestCommon:
    @gen_test
    async def test_empty(self):
        with self.session_manager() as sm:
            with self.assertRaises(ValueError):
                await sm.load("")
            with self.assertRaises(ValueError):
                await sm.load("foo")
            with self.assertRaises(KeyError):
                await sm.load("foobarbazwibblewobble")

            self.assertFalse(await sm.exists(""))
            self.assertFalse(await sm.exists("foo"))

    @gen_test
    async def test_create(self):
        with self.session_manager() as sm:
            with mock.patch("time.time", return_value=100):
                session = await sm.create()
            self.assertTrue(await sm.exists(session.key))
            self.assertEqual(session.time_created, 100)
            self.assertEqual(session.time_saved, 100)

            session1 = await sm.load(session.key)
            self.assertEqual(session.key, session1.key)
            self.assertEqual(session.data, session1.data)
            self.assertEqual(session1.time_created, 100)
            self.assertEqual(session1.time_saved, 100)

    @gen_test
    async def test_getset(self):
        with self.session_manager() as sm:
            with mock.patch("time.time", return_value=100):
                session = await sm.create()
            self.assertEqual(session.time_created, 100)
            self.assertEqual(session.time_saved, 100)
            self.assertFalse(session.dirty)
            self.assertIsNone(session.get("test", None))

            session.set("test", "foo")
            self.assertEqual(session.get("test", None), "foo")
            self.assertEqual(session.time_created, 100)
            self.assertEqual(session.time_saved, 100)
            self.assertTrue(session.dirty)

            session.set("test", "bar")
            self.assertEqual(session.get("test", None), "bar")
            self.assertEqual(session.time_created, 100)
            self.assertEqual(session.time_saved, 100)
            self.assertTrue(session.dirty)

            with self.assertRaises(ValueError):
                session.set("_test", "foo")

    @gen_test
    async def test_save(self):
        with self.session_manager() as sm:
            with mock.patch("time.time", return_value=100):
                session = await sm.create()
            self.assertIsNone(session.get("test", None))

            session.set("test", "foo")
            self.assertEqual(session.get("test", None), "foo")
            self.assertTrue(session.dirty)

            with mock.patch("time.time", return_value=200):
                await session.save()
            self.assertEqual(session.get("test", None), "foo")
            self.assertEqual(session.time_created, 100)
            self.assertEqual(session.time_saved, 200)
            self.assertFalse(session.dirty)

            session1 = await sm.load(session.key)
            self.assertEqual(session1.get("test"), "foo")
            self.assertEqual(session1.time_created, 100)
            self.assertEqual(session1.time_saved, 200)


class TestMemorySessionManager(
    SessionTestMixin, SessionTestCommon, AsyncTestCase
):
    @contextlib.contextmanager
    def session_manager(self) -> SessionManager:
        config = MockConfig()
        session_manager = MemorySessionManager(config)
        yield session_manager

    @gen_test
    async def test_is_viable(self):
        config = MockConfig()
        self.assertTrue(await MemorySessionManager.is_viable(config))


class TestFilesystemSessionManager(
    SessionTestMixin, SessionTestCommon, AsyncTestCase
):
    @contextlib.contextmanager
    def session_manager(self) -> SessionManager:
        config = MockConfig()
        with tempfile.TemporaryDirectory() as tdir:
            config.web_session_dir = tdir
            session_manager = FilesystemSessionManager(config)
            yield session_manager

    @gen_test
    async def test_is_viable(self):
        config = MockConfig()
        with tempfile.TemporaryDirectory() as tdir:
            config.web_session_dir = tdir
            self.assertTrue(await FilesystemSessionManager.is_viable(config))
        config.web_session_dir = None
        self.assertFalse(await FilesystemSessionManager.is_viable(config))
