from unittest import TestCase, SkipTest
from unittest import mock
from manager.signing import (
    HAVE_SIGNING,
    loads,
    dumps,
    BadSignature,
    SignatureExpired,
)
import datetime


class TestSigning(TestCase):
    def setUp(self):
        if not HAVE_SIGNING:
            raise SkipTest("django is not installed")

    def test_loads(self):
        with mock.patch("django.core.signing.time.time", return_value=1000):
            token = dumps({"test": 1}, key="testkey", salt="foo")

        with mock.patch("django.core.signing.time.time", return_value=1000):
            res = loads(token, key="testkey", salt="foo")

        self.assertEqual(res, {"test": 1})

    def test_loads_wrong_key(self):
        with mock.patch("django.core.signing.time.time", return_value=1000):
            token = dumps({"test": 1}, key="testkey", salt="foo")

        with mock.patch("django.core.signing.time.time", return_value=1000):
            with self.assertRaises(BadSignature):
                loads(token, key="testkey1", salt="foo")

    def test_loads_wrong_salt(self):
        with mock.patch("django.core.signing.time.time", return_value=1000):
            token = dumps({"test": 1}, key="testkey", salt="foo")

        with mock.patch("django.core.signing.time.time", return_value=1000):
            with self.assertRaises(BadSignature):
                loads(token, key="testkey", salt="foo1")

    def test_loads_expired(self):
        with mock.patch("django.core.signing.time.time", return_value=1000):
            token = dumps({"test": 1}, key="testkey", salt="foo")

        with mock.patch("django.core.signing.time.time", return_value=1100):
            res = loads(token, key="testkey", salt="foo", max_age=100)
        self.assertEqual(res, {"test": 1})

        with mock.patch("django.core.signing.time.time", return_value=1101):
            with self.assertRaises(SignatureExpired):
                loads(token, key="testkey", salt="foo", max_age=100)

        with mock.patch("django.core.signing.time.time", return_value=1100):
            res = loads(
                token,
                key="testkey",
                salt="foo",
                max_age=datetime.timedelta(seconds=100),
            )
        self.assertEqual(res, {"test": 1})

        with mock.patch("django.core.signing.time.time", return_value=1101):
            with self.assertRaises(SignatureExpired):
                loads(
                    token,
                    key="testkey",
                    salt="foo",
                    max_age=datetime.timedelta(seconds=100),
                )
