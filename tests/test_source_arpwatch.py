import asyncio
import tempfile
import os
from tornado.testing import gen_test
import manager.sources as ds
from manager.events import HostSeenEvent
from manager.config import Config
from .common import (
    TestDataSourceMixin,
    StopTest,
    GatherEvents,
    InotifyTestCase,
)
from manager import compat


class TestArpwatchDataSource(TestDataSourceMixin, InotifyTestCase):
    """
    test a data source that checks an arpwatch file.
    """

    def make_datasource(self):
        config = Config()
        return ds.ArpwatchDataSource(
            event_hub=self.event_hub, config=config, datafile=self.tmpfile.name
        )

    def setUp(self, *args, **kw):
        self.tmpfile = tempfile.NamedTemporaryFile()
        super().setUp(*args, **kw)

    def tearDown(self, *args, **kw):
        self.tmpfile.close()
        super().tearDown(*args, **kw)

    @gen_test
    async def test_command_run(self):
        events = GatherEvents(self.event_hub, [HostSeenEvent])

        @asyncio.coroutine
        def test():
            yield from self.ds.wait_until_started()
            with open(self.tmpfile.name, "wb") as out:
                out.write(b"52:54:00:5e:76:10       192.168.7.241   1549968902      server  eth0\n")  # noqa: E501
                out.write(b"52:54:00:4e:b3:61       192.168.7.1     1549968902              eth0\n")  # noqa: E501
                os.fdatasync(out.fileno())
            # Wait until changes start to be reported
            yield from events.received_first
            yield from self.event_hub.publish(StopTest(None))

        await asyncio.gather(
            compat.create_task(self.ds.run()), compat.create_task(test()),
        )

        events = events.gathered
        # We receive the events 3 times, but they are the same
        self.assertEqual(len(events), 6)
        self.assertIsNone(events[0].name)
        self.assertEqual(events[0].mac, "52:54:00:5e:76:10")
        self.assertEqual(events[0].ip, "192.168.7.241")
        self.assertEqual(events[0].timestamp, 1549968902)
        self.assertIsNone(events[1].name)
        self.assertEqual(events[1].mac, "52:54:00:4e:b3:61")
        self.assertEqual(events[1].ip, "192.168.7.1")
        self.assertEqual(events[1].timestamp, 1549968902)
