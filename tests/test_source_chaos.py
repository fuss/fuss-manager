import asyncio
import tempfile
from tornado.testing import gen_test, AsyncTestCase
import manager.sources as ds
from manager.events import HostSeenEvent
from manager.config import Config
from .common import (
    TestDataSourceMixin,
    StopTest,
    GatherEvents,
)
from manager import compat


class TestChaosDataSource(TestDataSourceMixin, AsyncTestCase):
    """
    test a data source that generates sample data.
    """

    def make_datasource(self):
        config = Config()
        config.arpwatch_datafile = self.tmpfile.name
        return ds.ChaosDataSource(event_hub=self.event_hub, config=config,)

    def setUp(self, *args, **kw):
        self.tmpfile = tempfile.NamedTemporaryFile()
        super().setUp(*args, **kw)

    def tearDown(self, *args, **kw):
        self.tmpfile.close()
        super().tearDown(*args, **kw)

    @gen_test
    async def test_command_run(self):
        events = GatherEvents(self.event_hub, [HostSeenEvent])

        @asyncio.coroutine
        def test():
            yield from self.ds.wait_until_started()

            # Wait until changes start to be reported
            yield from events.received_first
            yield from self.event_hub.publish(StopTest(None))

        await asyncio.gather(
            compat.create_task(self.ds.run()), compat.create_task(test()),
        )

        events = events.gathered
        # We don't really have to check what is in the event, since this
        # is just a test source, but at least we check that it looks
        # reasonable
        self.assertEqual(len(events), 1)
        self.assertTrue(getattr(events[0], 'source'))
