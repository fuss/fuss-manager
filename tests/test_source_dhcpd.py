import asyncio
import datetime
import tempfile
import shutil

from tornado.testing import gen_test, AsyncTestCase

import manager.sources as ds
from manager import compat
from manager.config import Config
from manager.events import HostSeenEvent
from .common import TestDataSourceMixin, StopTest, GatherEvents


class TestDhcpdDataSource(TestDataSourceMixin, AsyncTestCase):
    """
    test a data source that tail -f -s syslog for DHCP messages.
    """
    def make_datasource(self):
        config = Config()
        config.dhcp_log_file = self.tmpfile.name
        return ds.DhcpdDataSource(
            event_hub=self.event_hub,
            config=config,
        )

    def setUp(self, *args, **kw):
        self.tmpfile = tempfile.NamedTemporaryFile()
        super().setUp(*args, **kw)

    def tearDown(self, *args, **kw):
        self.tmpfile.close()
        super().tearDown(*args, **kw)

    @gen_test
    async def test_command_run(self):
        events = GatherEvents(self.event_hub, [HostSeenEvent])

        async def test():
            await self.ds.wait_until_started()

            # some valid dhcp data for one client
            with open('tests/data/dhcp_data_from_syslog.txt', 'rb') as fp:
                shutil.copyfileobj(fp, self.tmpfile)
                self.tmpfile.flush()

            # Wait until changes start to be reported
            await events.received_first
            await self.event_hub.publish(StopTest(None))

        await asyncio.gather(
            compat.create_task(self.ds.run()),
            compat.create_task(test()),
        )

        events = events.gathered
        self.assertEqual(len(events), 6)

        # Full checks on the 3 events for the first machine
        self.assertIsNone(events[0].name)
        self.assertEqual(events[0].mac, "52:54:00:f2:f7:62")
        self.assertIsNone(events[0].ip)
        # the timestamp obtained is dependent on the local timezone, as
        # that is what is printed in the syslog, and is also missing the
        # year, so we only check that it is in february, as that is
        # enough (most of the time) to check that it is not "right now".
        ts = datetime.datetime.fromtimestamp(events[0].timestamp)
        self.assertEqual(ts.month, 2)

        self.assertIsNone(events[1].name)
        self.assertEqual(events[1].mac, "52:54:00:f2:f7:62")
        self.assertEqual(events[1].ip, "192.168.6.100")
        ts = datetime.datetime.fromtimestamp(events[1].timestamp)
        self.assertEqual(ts.month, 2)

        self.assertEqual(events[2].name, "cliente.school.lan")
        self.assertEqual(events[2].ip, "192.168.6.100")
        self.assertIsNone(events[2].mac)
        ts = datetime.datetime.fromtimestamp(events[2].timestamp)
        self.assertEqual(ts.month, 2)

        # For the second machine, we just check that the timestamp has
        # been parsed correctly, so that the test doesn't have a false
        # negative on february.
        ts = datetime.datetime.fromtimestamp(events[3].timestamp)
        self.assertEqual(ts.month, 3)
        ts = datetime.datetime.fromtimestamp(events[4].timestamp)
        self.assertEqual(ts.month, 3)
        ts = datetime.datetime.fromtimestamp(events[5].timestamp)
        self.assertEqual(ts.month, 3)
