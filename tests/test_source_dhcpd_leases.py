import asyncio
import datetime
import tempfile
import shutil

from tornado.testing import gen_test, AsyncTestCase

import manager.sources as ds
from manager import compat
from manager.config import Config
from manager.events import HostSeenEvent
from .common import TestDataSourceMixin, StopTest, GatherEvents


class TestDhcpdLeasesDataSource(TestDataSourceMixin, AsyncTestCase):
    """
    test a data source that reads /var/lib/dhcp/dhcpd.leases as it changes
    """
    def make_datasource(self):
        config = Config()
        config.dhcp_leases_file = self.tmpfile.name
        return ds.DhcpLeasesDataSource(
            event_hub=self.event_hub,
            config=config,
        )

    def setUp(self, *args, **kw):
        self.tmpfile = tempfile.NamedTemporaryFile()
        super().setUp(*args, **kw)

    def tearDown(self, *args, **kw):
        self.tmpfile.close()
        super().tearDown(*args, **kw)

    @gen_test
    async def test_command_run(self):
        events = GatherEvents(self.event_hub, [HostSeenEvent])

        async def test():
            await self.ds.wait_until_started()

            # some valid dhcp data for one client
            with open('tests/data/dhcp_leases.txt', 'rb') as fp:
                shutil.copyfileobj(fp, self.tmpfile)
                self.tmpfile.flush()

            # Wait until changes start to be reported
            await events.received_first
            await self.event_hub.publish(StopTest(None))

        await asyncio.gather(
            compat.create_task(self.ds.run()),
            compat.create_task(test()),
        )

        events = events.gathered
        self.assertEqual(len(events), 1)

        # Full checks on the 3 events for the first machine
        #self.assertEqual(events[0].name, 'cliente.school.lan')
        self.assertEqual(events[0].mac, "52:54:00:f2:f7:62")
        self.assertEqual(events[0].ip, "192.168.6.100")
        ts = datetime.datetime.fromtimestamp(events[0].timestamp)
        self.assertEqual(
            ts,
            datetime.datetime(2020,8,20,10,20,2)
        )
