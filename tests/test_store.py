import asyncio
import copy
import os.path
import tempfile
import json
import time
from contextlib import contextmanager

import ruamel.yaml
from tornado.testing import gen_test, AsyncTestCase

from .common import FussTestNetwork

from manager import stores, events, config


class TestMockMachineStore(AsyncTestCase):
    """
    Tests for the common code of (Mock)MachineStore.
    """

    machine = {
        "mac": "52:54:00:f2:f7:62",
        "ip": "192.168.6.100",
        "name": "cliente.school.lan",
    }

    def setUp(self, *args, **kw):
        super().setUp(*args, **kw)
        self.inventory_dir = tempfile.TemporaryDirectory()
        self.inventory_fn = os.path.join(self.inventory_dir.name, 'hosts')
        self.stats = tempfile.NamedTemporaryFile()
        self.event_hub = events.Hub()
        self.config = config.Config()
        self.config.path_ansible_inventory = self.inventory_fn
        self.config.path_stats_cache = self.stats.name
        self.store = stores.MockMachineStore(
            self.event_hub, config=self.config
        )

    def tearDown(self, *args, **kw):
        self.stats.close()
        self.inventory_dir.cleanup()
        super().tearDown(*args, **kw)

    @contextmanager
    def gather_events(self):
        gathered = []

        @asyncio.coroutine
        def gather_host_events(evt):
            gathered.append(evt)

        self.event_hub.subscribe(gather_host_events, [events.HostEvent])
        try:
            yield gathered
        finally:
            self.event_hub.unsubscribe(gather_host_events)
        return gathered

    async def post_host_seen(self, **kw):
        with self.gather_events() as gathered:
            await self.event_hub.publish(events.HostSeenEvent(None, **kw))
        return gathered

    @gen_test
    async def test_add_machine_by_mac(self):
        mac = self.machine['mac']
        gathered = await self.post_host_seen(mac=mac)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, self.machine['ip'])
        self.assertEqual(self.store.machines[mac].ip, self.machine['ip'])
        self.assertEqual(self.store.machines[mac].name, self.machine['name'])
        self.assertTrue(self.store.machines[mac].registered)

    @gen_test
    async def test_add_machine_by_ip(self):
        mac = self.machine['mac']
        gathered = await self.post_host_seen(ip=self.machine["ip"])
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, self.machine['ip'])
        self.assertEqual(self.store.machines[mac].name, self.machine['name'])
        self.assertTrue(self.store.machines[mac].registered)

    @gen_test
    async def test_add_machine_by_name(self):
        mac = self.machine['mac']
        await self.post_host_seen(name=self.machine["name"])
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, self.machine['ip'])
        self.assertEqual(self.store.machines[mac].name, self.machine['name'])
        self.assertTrue(self.store.machines[mac].registered)

    @gen_test
    def test_add_machine_mac_and_ip(self):
        # TODO
        pass

    @gen_test
    def test_add_machine_mac_and_name(self):
        # TODO
        pass

    @gen_test
    def test_add_machine_ip_and_name(self):
        # TODO
        pass

    @gen_test
    async def test_add_machine_mac_and_wrong_ip(self):
        mac = self.machine['mac']
        wrong_ip = '10.0.0.6'
        # check that we didn't accidentally change the correct ip above.
        self.assertNotEqual(self.machine['ip'], wrong_ip)
        gathered = await self.post_host_seen(mac=mac, ip=wrong_ip)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(gathered[1].mac, mac)
        self.assertEqual(gathered[1].ip, wrong_ip)
        self.assertEqual(self.store.machines[mac].ip, wrong_ip)

    @gen_test
    async def test_add_machine_mac_and_wrong_name(self):
        mac = self.machine['mac']
        wrong_name = 'another.machine'
        # check that we didn't accidentally change the correct name above.
        self.assertNotEqual(self.machine['name'], wrong_name)
        gathered = await self.post_host_seen(mac=mac, name=wrong_name)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(gathered[1].mac, mac)
        self.assertEqual(gathered[1].name, wrong_name)
        self.assertEqual(self.store.machines[mac].name, wrong_name)

    @gen_test
    async def test_add_machine_ip_and_wrong_name(self):
        mac = self.machine['mac']
        wrong_name = 'another.machine'
        # check that we didn't accidentally change the correct name above.
        self.assertNotEqual(self.machine['name'], wrong_name)
        gathered = await self.post_host_seen(
            ip=self.machine['ip'], name=wrong_name
        )
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(gathered[1].mac, mac)
        self.assertEqual(gathered[1].ip, self.machine["ip"])
        self.assertEqual(gathered[1].name, wrong_name)
        self.assertEqual(self.store.machines[mac].ip, self.machine["ip"])
        self.assertEqual(self.store.machines[mac].name, wrong_name)

    @gen_test
    async def test_add_machine_no_data(self):
        gathered = await self.post_host_seen()
        self.assertEqual(len(gathered), 1)
        self.assertIsInstance(gathered[0], events.HostSeenEvent)
        self.assertFalse(self.store.machines)

    @gen_test
    async def test_add_machine_by_mac_no_name(self):
        mac = '52:54:00:f2:f7:64'
        self.assertNotEqual(mac, self.machine['mac'])
        gathered = await self.post_host_seen(mac=mac)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, None)
        self.assertEqual(self.store.machines[mac].name, mac)
        self.assertFalse(self.store.machines[mac].registered)

    @gen_test
    async def test_add_machine_by_ip_no_name(self):
        mac = '52:54:00:f2:f7:63'
        ip = '192.168.6.101'
        gathered = await self.post_host_seen(ip=ip)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, ip)
        self.assertEqual(self.store.machines[mac].name, ip)
        self.assertFalse(self.store.machines[mac].registered)

    @gen_test
    async def test_add_machine_by_mac_no_name_then_ip(self):
        mac = '52:54:00:f2:f7:64'
        ip = '192.168.6.102'
        self.assertNotEqual(mac, self.machine['mac'])
        gathered = await self.post_host_seen(mac=mac)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, None)
        self.assertEqual(self.store.machines[mac].name, mac)
        self.assertFalse(self.store.machines[mac].registered)
        gathered = await self.post_host_seen(mac=mac, ip=ip)
        self.assertIsInstance(gathered[1], events.HostChangedEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, ip)
        self.assertEqual(self.store.machines[mac].name, ip)
        self.assertFalse(self.store.machines[mac].registered)

    @gen_test
    async def test_add_machine_by_ip_no_name_then_change_ip(self):
        mac = '52:54:00:f2:f7:64'
        ip = '192.168.6.102'
        gathered = await self.post_host_seen(mac=mac, ip=ip)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, ip)
        self.assertEqual(self.store.machines[mac].name, ip)
        self.assertFalse(self.store.machines[mac].registered)
        new_ip = '192.168.6.103'
        gathered = await self.post_host_seen(mac=mac, ip=new_ip)
        self.assertIsInstance(gathered[1], events.HostChangedEvent)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].ip, new_ip)
        self.assertEqual(self.store.machines[mac].name, new_ip)
        self.assertFalse(self.store.machines[mac].registered)

    @gen_test
    async def test_add_machine_w_timestamp(self):
        mac = self.machine['mac']
        gathered = await self.post_host_seen(mac=mac, timestamp=100)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(gathered[1].timestamp, 100)
        self.assertEqual(gathered[1].mac, mac)
        self.assertEqual(gathered[1].ip, self.machine["ip"])
        self.assertEqual(gathered[1].name, self.machine["name"])
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

    @gen_test
    async def test_add_machine_earlier_timestamp(self):
        mac = self.machine['mac']
        gathered = await self.post_host_seen(
            mac=mac,
            timestamp=100,
            ip = self.machine['ip'])
        self.assertIsInstance(gathered[0], events.HostSeenEvent)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

        # HostSeen with an earlier timestamp generates no changes, even
        # if the ip has changed
        gathered = await self.post_host_seen(
            mac=mac,
            timestamp=80,
            ip = '192.168.6.101')
        self.assertNotEqual(self.machine['ip'], '192.168.6.101')
        self.assertIsInstance(gathered[0], events.HostSeenEvent)
        self.assertEqual(len(gathered), 1)
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

    @gen_test
    async def test_add_machine_later_timestamp(self):
        mac = self.machine['mac']
        gathered = await self.post_host_seen(mac=mac, timestamp=100)
        self.assertIsInstance(gathered[0], events.HostSeenEvent)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

        # HostSeen with a later timestamp generates no changes
        gathered = await self.post_host_seen(mac=mac, timestamp=200)
        self.assertIsInstance(gathered[0], events.HostSeenEvent)
        self.assertIsInstance(gathered[1], events.HostChangedEvent)
        self.assertEqual(gathered[1].changes, {"last_seen": (100, 200)})
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].last_seen, 200)

    @gen_test
    async def test_add_machine_to_group(self):
        mac = self.machine['mac']
        gathered = await self.post_host_seen(mac=mac, timestamp=100)
        self.assertIsInstance(gathered[0], events.HostSeenEvent)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

        with self.gather_events() as gathered:
            await self.store.add_machine_to_group(mac, "testgroup")
        self.assertIsInstance(gathered[0], events.HostChangedEvent)
        self.assertEqual(len(gathered), 1)
        self.assertEqual(gathered[0].changes, {"groups": ((), ("testgroup",))})
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

    @gen_test
    async def test_remove_machine_from_group(self):
        mac = self.machine['mac']
        gathered = await self.post_host_seen(mac=mac, timestamp=100)
        self.assertIsInstance(gathered[0], events.HostSeenEvent)
        self.assertIsInstance(gathered[1], events.HostNewEvent)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

        await self.store.add_machine_to_group(mac, "testgroup")

        with self.gather_events() as gathered:
            await self.store.remove_machine_from_group(mac, "testgroup")
        self.assertIsInstance(gathered[0], events.HostChangedEvent)
        self.assertEqual(len(gathered), 1)
        self.assertEqual(gathered[0].changes, {"groups": (("testgroup",), ())})
        self.assertIn(mac, self.store.machines)
        self.assertEqual(self.store.machines[mac].last_seen, 100)

    @gen_test
    async def test_get_machine_from_name(self):
        # fill the store with data from one machine
        await self.post_host_seen(
            mac=self.machine['mac'], timestamp=1000, name=self.machine['name'],
        )
        machine = self.store.get_machine(name=self.machine['name'])
        self.assertEqual(machine.mac, self.machine['mac'])
        self.assertEqual(machine.name, self.machine['name'])

    @gen_test
    async def test_get_machine_from_ip(self):
        # fill the store with data from one machine
        await self.post_host_seen(
            mac=self.machine['mac'], timestamp=1000, ip=self.machine['ip'],
        )
        machine = self.store.get_machine(ip=self.machine['ip'])
        self.assertEqual(machine.mac, self.machine['mac'])
        self.assertEqual(machine.ip, self.machine['ip'])

    @gen_test
    async def test_get_machine_facts(self):
        # fill the store with data from one machine
        await self.post_host_seen(
            mac=self.machine['mac'], timestamp=1000, name=self.machine['name'],
        )
        # and then get facts for that machine
        with self.gather_events() as gathered:
            await self.event_hub.publish(
                events.HostFactsEvent(
                    None,
                    name=self.machine['name'],
                    facts={
                        "ansible_facts": {},
                        'timestamp': 1010,
                        "changed": False,
                    },
                )
            )
        self.assertEqual(len(gathered), 2)
        self.assertIsInstance(gathered[1].changes['facts'][1], dict)
        self.assertEqual(gathered[1].mac, self.machine['mac'])
        self.assertNotIn('registered', gathered[1].changes)
        self.assertIn('facts_log', gathered[1].changes)

        machine = self.store.machines[self.machine['mac']]
        self.assertIn('ansible_facts', machine.facts)
        self.assertEqual(1010, machine.facts['timestamp'])

    @gen_test
    async def test_get_machine_facts_failure(self):
        # fill the store with data from one machine
        await self.post_host_seen(
            mac=self.machine['mac'], timestamp=1000, name=self.machine['name'],
        )
        # and then get facts for that machine
        with self.gather_events() as gathered:
            await self.event_hub.publish(
                events.HostFactsFailed(
                    None,
                    name=self.machine['name'],
                    result='UNREACHABLE',
                    details={'timestamp': 1010},
                )
            )
        self.assertEqual(len(gathered), 2)
        self.assertEqual(gathered[1].mac, self.machine['mac'])
        self.assertEqual(
            gathered[1].changes['facts_log'][1][-1]['result'], 'UNREACHABLE'
        )

    @gen_test
    async def test_save_inventory(self):
        # add a machine to the inventory
        await self.post_host_seen(ip=self.machine["ip"])
        # and then save the inventory
        self.store.save_inventory()
        yaml = ruamel.yaml.YAML()
        with open(self.inventory_fn) as fp:
            loaded_inv = yaml.load(fp)
        self.assertIn(self.machine["name"], loaded_inv['all']['hosts'])

    @gen_test
    async def test_load_inventory(self):
        mac = self.machine['mac']
        await self.post_host_seen(mac=mac)
        with open(self.inventory_fn, 'w') as dfp:
            with open('tests/data/inventories/good.yaml') as sfp:
                dfp.write(sfp.read())
        os.makedirs(
            os.path.join(self.inventory_dir.name, 'host_vars'), exist_ok=True,
        )
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'as01df00.school.lan.yaml',
            ),
            'w',
        ) as fp:
            fp.write('some_variable: some_value')
        await self.store.load_inventory()

        # a machine that is already available in the store
        self.assertIn(mac, self.store.machines)
        self.assertIn(
            'unknown', self.store.machines[mac].groups,
        )

        # a machine that wasn't in the store
        mac = '52:54:00:5e:76:10'
        self.assertIn(mac, self.store.machines)
        self.assertIn(
            'all', self.store.machines[mac].groups,
        )

        mac = '12:34:56:78:9a:01'
        self.assertIn(mac, self.store.machines)
        self.assertEqual(
            self.store.machines[mac].host_vars['some_variable'], 'some_value'
        )

    @gen_test
    async def test_load_empty_inventory(self):
        with open(self.inventory_fn, 'w') as fp:
            fp.write('')
        await self.store.load_inventory()
        self.assertEqual(self.store.machines, {})

    @gen_test
    async def test_load_inventory_without_macs(self):
        with open(self.inventory_fn, 'w') as dfp:
            with open('tests/data/inventories/no_mac.yaml') as sfp:
                dfp.write(sfp.read())
        await self.store.load_inventory()
        self.assertEqual(self.store.machines, {})

    @gen_test
    async def test_load_inventory_no_file(self):
        no_inventory_fn = os.path.join(
            self.inventory_dir.name, 'no-such-hosts-file'
        )
        self.store.inventory_fn = no_inventory_fn
        await self.store.load_inventory()

        self.assertEqual(self.store.machines, {})

    @gen_test
    async def test_save_stats(self):
        # fill the store with data from one machine
        mac = self.machine['mac']
        await self.post_host_seen(mac=mac, timestamp=1000)
        # write the stats file
        self.store.save_stats_file()
        # check the contents
        with open(self.stats.name) as fp:
            stats_contents = json.load(fp)
        c = stats_contents["52:54:00:f2:f7:62"]
        self.assertEqual(c["mac"], "52:54:00:f2:f7:62")
        self.assertEqual(c["ip"], self.machine["ip"])
        self.assertEqual(c["name"], self.machine["name"])
        self.assertEqual(c["first_seen"], 1000)
        self.assertEqual(c["last_seen"], 1000)
        # Depending on external factors the machine can be registered or
        # not, so we only check that the field is a boolean and not its
        # value.
        self.assertIsInstance(c["registered"], bool)
        self.assertEqual(c['facts_log'], [])

    @gen_test
    async def test_load_stats(self):
        with open(self.stats.name, 'w') as fp:
            fp.write(
                '{"52:54:00:f2:f7:62": {"mac": "52:54:00:f2:f7:62", "last_seen": 1000}}'  # noqa: E501
            )
        await self.store.load_stats_file()
        self.assertIn('52:54:00:f2:f7:62', self.store.machines)

    @gen_test
    async def test_load_stats_keep_existing(self):
        # fill the store with data from one machine and some facts
        mac = self.machine['mac']
        await self.post_host_seen(
            mac=mac, timestamp=1000, name=self.machine['name'],
        )
        machine = self.store.machines[mac]
        await self.event_hub.publish(
            events.HostFactsEvent(
                None,
                name=self.machine['name'],
                facts={"ansible_facts": {}, "changed": False},
            )
        )
        self.assertEqual(machine.last_seen, 1000)
        # load some stats from file
        with open(self.stats.name, 'w') as fp:
            fp.write(
                '{"52:54:00:f2:f7:62": {"mac": "52:54:00:f2:f7:62", "last_seen": 2000}}'  # noqa: E501
            )
        await self.store.load_stats_file()
        # reload the machine and check that the machine still has facts
        machine = self.store.machines[mac]
        self.assertEqual(len(machine.facts_log), 1)
        self.assertIn('ansible_facts', machine.facts)
        self.assertFalse(machine.facts['changed'])
        # but also that they have been updated
        self.assertEqual(machine.last_seen, 2000)


class TestMachineStore(AsyncTestCase):
    """
    Tests for the non-mock code in the MachineStore
    """

    def setUp(self, *args, **kw):
        super().setUp(*args, **kw)
        self.inventory = tempfile.NamedTemporaryFile()
        self.stats = tempfile.NamedTemporaryFile()
        self.event_hub = events.Hub()
        self.config = config.Config()
        self.config.path_ansible_inventory = self.inventory.name
        self.config.path_stats_cache = self.stats.name
        self.store = stores.MachineStore(self.event_hub, config=self.config)

    def tearDown(self, *args, **kw):
        self.stats.close()
        self.inventory.close()
        super().tearDown(*args, **kw)

    @gen_test
    async def test_mac_to_ip(self):
        net = FussTestNetwork()  # noqa: F841

        ip = await self.store.mac_to_ip('00:00:00:00:00:00')
        self.assertIsNone(ip)

        ip = await self.store.mac_to_ip('02:00:00:01:02:01')
        self.assertEqual(ip, '192.168.123.2')

    @gen_test
    async def test_ip_to_mac(self):
        net = FussTestNetwork()  # noqa: F841

        mac = await self.store.ip_to_mac('127.0.0.1')
        self.assertIsNone(mac)

        mac = await self.store.ip_to_mac('192.168.123.2')
        self.assertEqual(mac, '02:00:00:01:02:01')

    @gen_test
    async def test_ip_to_name(self):
        net = FussTestNetwork()  # noqa: F841

        name = await self.store.ip_to_name('127.0.0.1')
        self.assertEqual(name, 'localhost')

        name = await self.store.ip_to_name('192.168.123.2')
        self.assertEqual(name, '192.168.123.2')

    @gen_test
    async def test_name_to_ip(self):
        net = FussTestNetwork()  # noqa: F841

        ip = await self.store.name_to_ip('localhost')
        self.assertEqual(ip, '127.0.0.1')

    @gen_test
    async def test_is_registered(self):
        net = FussTestNetwork()  # noqa: F841

        # we know that we have access to these two machines from the
        # FussTestNetwork, but we don't know whether their fingerprint
        # has already been checked, so we just check that the method
        # runs with no errors
        for ip in ('192.168.123.2', '192.168.123.3'):
            registered = await self.store.is_registered(ip)
            self.assertIsInstance(registered, bool)
            # self.assertTrue(registered)

        # on the other hand, we do know that we have no access to
        # ``invalid``.
        registered = await self.store.is_registered('invalid')
        self.assertFalse(registered)

        # and a machine that can't be reached is unknown
        registered = await self.store.is_registered('192.168.123.5')
        self.assertIsNone(registered)


class TestSimpleAnsibleInventory(AsyncTestCase):
    def _get_simple(self):
        return copy.deepcopy(
            {
                'all': {
                    'hosts': {
                        'unknown.school.lan': {
                            'macaddress': '52:54:00:ab:cd:ef',
                            'variable': 'unknownvalue',
                        },
                    },
                    'children': {
                        'class': {
                            'hosts': {
                                'client1.school.lan': {
                                    'macaddress': '12:34:56:78:9a:01',
                                    'variable': 'client1value',
                                },
                                'client2.school.lan': {
                                    'macaddress': '12:34:56:78:9a:23',
                                    'variable': 'client2value',
                                },
                            }
                        }
                    },
                }
            }
        )

    def setUp(self):
        self.inventory_dir = tempfile.TemporaryDirectory()
        self.inventory_fname = os.path.join(self.inventory_dir.name, 'hosts')
        self.inventory = stores.SimpleAnsibleInventory(self.inventory_fname)

    def tearDown(self):
        self.inventory_dir.cleanup()

    def test_load_ini(self):
        with open(self.inventory_fname, 'w') as dest_fp:
            with open('tests/data/inventories/good.ini') as src_fp:
                dest_fp.write(src_fp.read())
        self.inventory.inv_format = 'ini'
        self.inventory.load()
        inv = self.inventory.inventory['all']
        self.assertIn('server.school.lan', inv['hosts'])
        self.assertIn('macaddress', inv['hosts']['server.school.lan'])
        self.assertEqual(
            '52:54:00:5e:76:10',
            inv['hosts']['server.school.lan']['macaddress'],
        )
        self.assertIn(
            'cliente.school.lan', inv['children']['unknown']['hosts']
        )
        self.assertIn(
            'macaddress',
            inv['children']['unknown']['hosts']['cliente.school.lan'],
        )
        for c in range(1, 3):
            self.assertIn('classe_{}'.format(c), inv['children'])
            for s in range(4):
                self.assertIn(
                    'as0{}df0{}.school.lan'.format(c, s),
                    inv['children']['classe_{}'.format(c)]['hosts'],
                )

    def test_load_yaml(self):
        with open(self.inventory_fname, 'w') as dest_fp:
            with open('tests/data/inventories/good.yaml') as src_fp:
                dest_fp.write(src_fp.read())
        self.inventory.load()
        inv = self.inventory.inventory['all']
        self.assertIn('server.school.lan', inv['hosts'])
        self.assertIn('macaddress', inv['hosts']['server.school.lan'])
        self.assertEqual(
            '52:54:00:5e:76:10',
            inv['hosts']['server.school.lan']['macaddress'],
        )
        self.assertIn(
            'cliente.school.lan', inv['children']['unknown']['hosts']
        )
        self.assertIn(
            'macaddress',
            inv['children']['unknown']['hosts']['cliente.school.lan'],
        )
        for c in range(1, 3):
            self.assertIn('classe_{}'.format(c), inv['children'])
            for s in range(4):
                self.assertIn(
                    'as0{}df0{}.school.lan'.format(c, s),
                    inv['children']['classe_{}'.format(c)]['hosts'],
                )

    def test_load_invalid(self):
        with self.assertRaises(ValueError):
            self.inventory.load(self.inventory_fname, inv_format='invalid')

    def test_load_empty_ini(self):
        with open(self.inventory_fname, 'w') as fp:
            fp.write('')
        self.inventory.inv_format = 'ini'
        self.inventory.load()
        inv = self.inventory.inventory['all']
        self.assertEqual(inv['hosts'], {})
        self.assertEqual(inv['children'], {})

    def test_load_empty_yaml(self):
        with open(self.inventory_fname, 'w') as fp:
            fp.write('')
        self.inventory.load()
        inv = self.inventory.inventory['all']
        self.assertEqual(inv['hosts'], {})
        self.assertEqual(inv['children'], {})

    def test_load_no_ini(self):
        self.inventory.fname = os.path.join(
            self.inventory_dir.name, 'no-such-file'
        )
        self.inventory.inv_format = 'ini'
        self.inventory.load()
        inv = self.inventory.inventory['all']
        self.assertEqual(inv['hosts'], {})
        self.assertEqual(inv['children'], {})

    def test_load_no_yaml(self):
        self.inventory.fname = os.path.join(
            self.inventory_dir.name, 'no-such-file'
        )
        self.inventory.load()
        inv = self.inventory.inventory['all']
        self.assertEqual(inv['hosts'], {})
        self.assertEqual(inv['children'], {})

    def test_save_empty_ini(self):
        self.inventory.inv_format = 'ini'
        self.inventory.save()
        with open(self.inventory_fname) as fp:
            saved = fp.read()
        self.assertFalse(saved)

    def test_save_empty_yaml(self):
        self.inventory.save()
        with open(self.inventory_fname) as fp:
            saved = fp.read()
        self.assertIn('all:\n', saved)

    def test_save_simple_ini(self):
        self.inventory.inv_format = 'ini'
        self.inventory.inventory = self._get_simple()
        self.inventory.save()
        with open(self.inventory_fname) as fp:
            saved = fp.read()
        # As a start, we're simply checking that some stuff exists in
        # the file, without checking that the file is strictly valid
        self.assertIn('[class]', saved)
        self.assertIn('client1.school.lan', saved)
        self.assertIn('unknown.school.lan', saved)
        self.assertIn('macaddress', saved)
        self.assertIn('macaddress=52:54:00:ab:cd:ef', saved)
        self.assertIn('macaddress=12:34:56:78:9a:01', saved)
        # that variables other than macaddress aren't saved in the
        # inventory
        self.assertNotIn('variable', saved)
        # but check that other variables are being saved elsewhere
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'unknown.school.lan.yaml',
            )
        ) as fp:
            self.assertIn('unknownvalue', fp.read())
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'client1.school.lan.yaml',
            )
        ) as fp:
            self.assertIn('client1value', fp.read())
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'client2.school.lan.yaml',
            )
        ) as fp:
            self.assertIn('client2value', fp.read())
        # And that it does not break when loading it back
        self.inventory.inv_format = 'ini'
        self.inventory.load()

    def test_save_simple_yaml(self):
        self.inventory.inventory = self._get_simple()
        self.inventory.save()
        with open(self.inventory_fname) as fp:
            saved = fp.read()
        # As a start, we're simply checking that some stuff exists in
        # the file, without checking that the file is strictly valid
        self.assertIn('all:', saved)
        self.assertIn('client1.school.lan', saved)
        self.assertIn('unknown.school.lan', saved)
        self.assertIn('macaddress: 52:54:00:ab:cd:ef', saved)
        self.assertIn('macaddress: 12:34:56:78:9a:01', saved)
        # that variables other than macaddress aren't saved in the
        # inventory
        self.assertNotIn('variable', saved)
        # but check that other variables are being saved elsewhere
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'unknown.school.lan.yaml',
            )
        ) as fp:
            self.assertIn('unknownvalue', fp.read())
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'client1.school.lan.yaml',
            )
        ) as fp:
            self.assertIn('client1value', fp.read())
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'client2.school.lan.yaml',
            )
        ) as fp:
            self.assertIn('client2value', fp.read())
        # And that it does not break when loading it back
        self.inventory.load()

    def test_save_vars(self):
        self.inventory.inventory = self._get_simple()
        self.inventory.save()
        with open(
            os.path.join(
                self.inventory_dir.name,
                'host_vars',
                'unknown.school.lan.yaml',
            )
        ) as fp:
            saved = fp.read()
        self.assertTrue(saved.startswith('---'))
        self.assertIn('variable', saved)

    def test_load_vars(self):
        with open(self.inventory_fname, 'w') as dest_fp:
            with open('tests/data/inventories/good.yaml') as src_fp:
                dest_fp.write(src_fp.read())
        hv_dir = os.path.join(self.inventory_dir.name, 'host_vars',)
        os.makedirs(hv_dir, exist_ok=True)
        with open(os.path.join(hv_dir, 'server.school.lan.yaml'), 'w') as fp:
            fp.write("some_var: server")
        with open(os.path.join(hv_dir, 'as01df00.school.lan.yaml'), 'w') as fp:
            fp.write("some_var: client0")
        with open(os.path.join(hv_dir, 'as01df01.school.lan.yaml'), 'w') as fp:
            fp.write("some_var: client1")
        self.inventory.load()
        host_data_server = self.inventory.inventory['all']['hosts'][
            'server.school.lan'
        ]
        self.assertEqual(host_data_server['some_var'], 'server')
        host_data_client0 = self.inventory.inventory['all']['children'][
            'classe_1'
        ]['hosts']['as01df00.school.lan']
        self.assertEqual(host_data_client0['some_var'], 'client0')
        host_data_client1 = self.inventory.inventory['all']['children'][
            'classe_1'
        ]['hosts']['as01df01.school.lan']
        self.assertEqual(host_data_client1['some_var'], 'client1')

    def test_add_machine_no_groups(self):
        machine = stores.Machine('52:54:00:5e:76:10', name='server.school.lan')
        self.inventory.add_machine(machine)
        inv = self.inventory.inventory['all']
        self.assertIn('server.school.lan', inv['hosts'])

    def test_add_machine_one_group(self):
        machine = stores.Machine(
            '12:34:56:78:9a:01', name='as01df01.school.lan'
        )
        machine.groups = ['classe_1']
        self.inventory.add_machine(machine)
        inv = self.inventory.inventory['all']
        self.assertIn(
            'as01df01.school.lan', inv['children']['classe_1']['hosts']
        )

    def test_add_machine_multiple_groups(self):
        machine = stores.Machine(
            '12:34:56:78:9a:01', name='as01df01.school.lan'
        )
        machine.groups = ['classe_1', 'laptops']
        self.inventory.add_machine(machine)
        inv = self.inventory.inventory['all']
        self.assertIn(
            'as01df01.school.lan', inv['children']['classe_1']['hosts']
        )
        self.assertIn(
            'as01df01.school.lan', inv['children']['laptops']['hosts']
        )

    def test_add_machine_with_vars(self):
        machine = stores.Machine(
            '52:54:00:5e:76:10',
            name='server.school.lan',
            host_vars={'some_variable': 'some_value'},
        )
        self.inventory.add_machine(machine)
        inv = self.inventory.inventory['all']
        host_entry = inv['hosts']['server.school.lan']
        self.assertEqual(host_entry['macaddress'], '52:54:00:5e:76:10')
        self.assertEqual(host_entry['some_variable'], 'some_value')


class TestRRList(AsyncTestCase):
    @gen_test
    def testSize(self):
        rrlist = stores.RRList(max_size=1, max_age=None)
        self.assertEqual(len(rrlist), 0)
        rrlist.append({'timestamp': 1})
        self.assertEqual(len(rrlist), 1)
        rrlist.append({'timestamp': 2})
        self.assertEqual(len(rrlist), 1)
        self.assertEqual(rrlist[0]['timestamp'], 2)
        rrlist.append({'timestamp': 3})
        self.assertEqual(len(rrlist), 1)
        self.assertEqual(rrlist[0]['timestamp'], 3)

    def testAge(self):
        rrlist = stores.RRList(max_age=1)
        day = 24 * 60 * 60
        now = time.time()
        self.assertEqual(len(rrlist), 0)
        rrlist.append({'timestamp': now - day - 15})
        self.assertEqual(len(rrlist), 0)
        rrlist.append({'timestamp': now})
        self.assertEqual(len(rrlist), 1)
        # rrlist expects timestamps to be ordered: if they aren't
        # elements that have an earlier timestamp may remain in the
        # list.
        rrlist.append({'timestamp': now - day - 15})
        self.assertEqual(len(rrlist), 2)
