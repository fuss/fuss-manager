import unittest
from manager.users.db import User, Group


class TestUser(unittest.TestCase):
    def test_empty(self):
        user = User()
        self.assertIsNone(user.name)
        self.assertIsNone(user.id)
        self.assertIsNone(user.display_name)
        self.assertEqual(user.groups, [])

    def test_full(self):
        user = User(name="foo", id=100,
                    groups=[Group("foo", 10), Group("bar", 20)],
                    display_name="name")
        self.assertEqual(user.name, "foo")
        self.assertEqual(user.id, 100)
        self.assertEqual(user.display_name, "name")
        self.assertEqual(user.groups, [Group("foo", 10), Group("bar", 20)])

    def test_jsonable(self):
        user = User(name="foo", id=100,
                    groups=[Group("foo", 10), Group("bar", 20)],
                    display_name="name")
        d = user.to_jsonable()
        self.assertEqual(d, {
            "name": "foo",
            "id": 100,
            "groups": [
                {"name": "foo", "id": 10},
                {"name": "bar", "id": 20},
            ],
            "display_name": "name",
        })
