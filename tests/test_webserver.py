from urllib.parse import urlencode
import json
import tempfile
from unittest import mock, SkipTest

from tornado.testing import AsyncHTTPTestCase, gen_test
from .common import AsyncHTTPClientWithCookies

from manager.web.server import Application
from manager.config import MockConfig


class ApplicationTestBase(AsyncHTTPTestCase):
    def get_app(self, *args, **kw):
        config = MockConfig()
        op_log = tempfile.NamedTemporaryFile()
        config.operation_log_file = op_log.name
        config.auth_forward_secret = "test"
        app = Application(config)
        self.io_loop.run_sync(app.init)
        return app

    def setUp(self):
        super().setUp()
        self.http_client = AsyncHTTPClientWithCookies(self.http_client)


class TestManagerApplication(ApplicationTestBase):
    def test_root(self):
        resp = self.fetch('/')
        self.assertEqual(resp.code, 200)


class TestWebAPI(ApplicationTestBase):
    async def api_get(self, name, expected_code=200, data=None):
        url = "/api/1.0/{}".format(name)
        if data:
            url += "?" + urlencode(data)
        headers = {}
        response = await self.http_client.fetch(
            self.get_url(url), headers=headers, raise_error=False,
        )
        content_type = response.headers.get("Content-Type")
        self.assertIn("application/json", content_type)
        self.assertEqual(response.code, expected_code)
        return json.loads(response.body.decode())

    async def api_post(self, name, expected_code=200, data=None):
        url = "/api/1.0/{}".format(name)
        headers = {}
        if data is None:
            data = {}
        body = json.dumps(data)
        response = await self.http_client.fetch(
            self.get_url(url),
            method="POST",
            headers=headers,
            body=body,
            raise_error=False,
        )
        content_type = response.headers.get("Content-Type")
        if "application/json" not in content_type:
            print("*** Response with unexpected content type", content_type)
            print(response.body.decode())
        self.assertIn("application/json", content_type)
        self.assertEqual(response.code, expected_code)
        return json.loads(response.body.decode())

    async def api_login(self, username):
        # Make a GET request to get XSRF cookie
        await self.http_client.fetch(
            self.get_url("/login"), method="GET", raise_error=True
        )
        self.http_client.enable_xsrf_header()
        if not username:
            return
        response = await self.http_client.fetch(
            self.get_url("/login"),
            method="POST",
            body=urlencode({"username": username, "password": username}),
            raise_error=True,
        )
        return response.code == 200

    @gen_test
    async def test_ping(self):
        for user in ['none']:
            await self.api_login(user)
            with mock.patch("time.time", return_value=100):
                res = await self.api_get("ping", expected_code=403)
            self.assertTrue(res['error'])
        for user in ['admin', 'user']:
            await self.api_login(user)
            with mock.patch("time.time", return_value=100):
                res = await self.api_get("ping")
            self.assertEqual(res, {"pong": True, "time": 100})

    @gen_test
    async def test_async_ping(self):
        for user in ['none']:
            await self.api_login(user)
            with mock.patch("time.time", return_value=100):
                res = await self.api_get("async_ping", expected_code=403)
        for user in ['admin', 'user']:
            await self.api_login(user)
            with mock.patch("time.time", return_value=100):
                res = await self.api_get("async_ping")
            self.assertEqual(res, {"pong": True, "time": 100})

    @gen_test
    async def test_whoami(self):
        sid = "1234567890"
        with mock.patch(
            "manager.web.session.SessionManager._get_random_string",
            return_value=sid,
        ):
            with mock.patch("time.time", return_value=100):
                res = await self.api_get("whoami")
        self.assertEqual(res, {"user": None, "time": 100})

        await self.api_login('root')

        with mock.patch("time.time", return_value=100):
            res = await self.api_get("whoami")
        self.assertEqual(
            res,
            {
                "user": {
                    'name': 'root',
                    'display_name': 'Root',
                    'id': 452,
                    'groups': [
                        {'id': 452, 'name': 'root'},
                        {'id': 521, 'name': 'admin'},
                    ],
                },
                "time": 100,
            },
        )

    @gen_test
    async def test_machines(self):
        for user in ['none']:
            await self.api_login(user)
            with mock.patch("time.time", return_value=100):
                res = await self.api_get("machines", expected_code=403)
        for user in ['admin', 'user']:
            await self.api_login(user)
            with mock.patch("time.time", return_value=100):
                res = await self.api_get("machines")
            self.assertIsInstance(res["machines"], list)
            self.assertEqual(res["time"], 100)

    @gen_test
    async def test_machine(self):
        for user in ['none']:
            await self.api_login(user)
        with mock.patch("time.time", return_value=100):
            res = await self.api_get(
                "machine/11:22:33:44:55:66", expected_code=403
            )
        for user in ['admin', 'user']:
            await self.api_login(user)
        with mock.patch("time.time", return_value=100):
            res = await self.api_get(
                "machine/11:22:33:44:55:66", expected_code=404
            )
        self.assertEqual(
            res,
            {
                "code": 404,
                "error": True,
                "message": "machine not found",
                "time": 100,
            },
        )

    @gen_test
    async def test_operation(self):
        # Make a GET request to get XSRF cookie
        await self.http_client.fetch(
            self.get_url("/login"), method="GET", raise_error=True
        )
        self.http_client.enable_xsrf_header()

        with mock.patch("time.time", return_value=100):
            res = await self.api_post(
                "operation", data={"op": {"name": "Noop"}}
            )
        self.assertEqual(res, {"time": 100})

    @gen_test
    async def test_auth_forward(self):
        from manager import signing

        if not signing.HAVE_SIGNING:
            raise SkipTest("signing module is not available")

        token = signing.dumps(
            {
                "url": "/test",
                "user": {
                    "name": "test",
                    "id": 2,
                    "display_name": "Test User",
                    "groups": [
                        {"name": "test", "id": 1},
                        {"name": "users", "id": 2},
                    ],
                },
            },
            key=self._app.config.auth_forward_secret,
            salt="fuss_manager.auth",
        )

        # Try to log in using this token
        res = await self.http_client.fetch(
            self.get_url("/auth-forward") + "?t=" + token,
            method="GET",
            raise_error=False,
            follow_redirects=False,
        )
        self.assertEqual(res.code, 302)
        self.assertEqual(res.headers["Location"], "/test")

        with mock.patch("time.time", return_value=100):
            res = await self.api_get("whoami")
        self.assertEqual(
            res,
            {
                "user": {
                    'name': 'test',
                    'display_name': 'Test User',
                    'id': 2,
                    'groups': [
                        {'id': 1, 'name': 'test'},
                        {'id': 2, 'name': 'users'},
                    ],
                },
                "time": 100,
            },
        )
